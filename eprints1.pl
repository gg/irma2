#!/usr/bin/perl

=head1 NAME

  eprints1.pl

=head1 DESCRIPTION

Interface for registration of identifiers in the urn:nbn:at namespace,
a.k.a. NBNs, as a proxy of attached applications like "eprints".

=head1 USAGE

=head2 normal work processes

  --rms           ... transfer data from eprints into redmine
  --mab           ... manually check MAB records; specify AC-Numbers as parameters
  --marc          ... manually check MARC records; specify AC-Numbers as parameters
  --mab4ep=count  ... verify MAB records and request up to <count> missing MAB records from agent "aleph_cat"
  --marc4ep=count ... verify MARC records and request up to <count> missing MARC records, also from agent "aleph_cat"

  --nbn4ep=count  ... feed <count> items to the urn:nbn:at request queue and check for already registered nbns.

  --reset <ac_numbers> ... reset error conditions for these ac_numbers
  --reset ALL REALLY   ... reset all error conditions and request fresh MARC records

=head2 debugging

  --debug
  --debug_names    ... export all names
  --debug_classifications  --DC
  --debug_stkz
  --debug_stbez
  --debug_filenames

=head2 databases access

  --ci ... connect to IRMA database (MySQL, Redmine)
  --ce ... connect to eprints database (MySQL)
  --ch ... connect to hopla database (MySQL)
  --cu ... connect to utheses database (MySQL, Webaccount)

=head1 EXAMPLES

=head2 see selected MAB records

  ./eprints1.pl --mab AC10709782 AC10709833 AC10710075

=head2 verify the next 1000 MAB records

  ./eprints --mab4ep=1000 ... verify and optionally fetch

  after a while:
  ./eprints --mab4ep=1000 ... just verify the newly fetched items

=cut

use strict;

# use DBI;
# use DBD::mysql;
use JSON -convert_blessed_universally;
use Redis;

use Util::ts;
use Util::JSON;
use Util::Matrix;
use Redmine::DB::MySQL;

use Phaidra::Utils::iso639;

use Data::Dumper;
$Data::Dumper::Indent= 1;
$Data::Dumper::Sortkeys= 1;

use FileHandle;

use utf8;
binmode( STDIN,  ':utf8' );
binmode( STDOUT, ':utf8' ); autoflush STDOUT 1;
binmode( STDERR, ':utf8' ); autoflush STDERR 1;

use lib 'lib';

use IRMA::NA;
use IRMA::eprints;
use IRMA::Mab;
use IRMA::urn::nbn::at;

use Univie::Utheses::Container;

use Alma::MARC_Extractor;

# TODO: make this configurable too!
my $config_fnm= '/etc/irma/eprints.json';

# my $agent_name= 'irma-urn-othes-test';
my $agent_name= 'irma-urn-othes-prod';

# querying mab records
my $MAX_MAB_AGE= 86400*14;
my $MAX_MAB_REQUESTS= 10_000;
my $MAX_MARC_AGE= 86400*14;
my $MAX_MARC_REQUESTS= 10_000;
my $MAX_URN_REQUESTS= 10_000;
my $MAX_SYNC= 100_000;

my $cnt_mab_requests= 0;
my $cnt_marc_requests= 0;

my $delete_bad_ac_entries= 0; # TODO: should be an option?
my $show_TODOs= 0;

my $die_nbn_already_defined= 0;
my $op_mode= 'unknown';

# ======================================================================
# BEGIN OT2UT: Othes to Utheses 
  my $ot2ut_context= 'ot2ut-entw'; # TODO: parametrize
# my $ot2ut_context= 'ot2ut-test'; # TODO: parametrize
# my $ot2ut_context= 'ot2ut-prod'; # TODO: parametrize
my $oma_sleep_time= 5;
my $activity_period= 12;
my $MAX_BLOCK= 699;

my %map_ot2ut_roles=
(
  'advisers'   => [qw(betreuer                   betreuer_2     betreuer_3   )],
  'coadvisers' => [qw(mitbetreuer                mitbetreuer_2               )],
  'assessors'  => [qw(             beurteiler_1  beurteiler_2   beurteiler_3 )],
);

my %map_ot2ut_thesis_columns=
(
  doi       => 'doi',
  urn       => 'urn',
  ac_nummer => 'ac_number',
  pages     => 'pages_scope',
  date_year => 'publication_date',
  eprintid  => 'eprint_id',
);

my %map_ot2ut_json_columns=
(
  ubw_intern_1 => 'utheses_note_1',
  ubw_intern_2 => 'utheses_note_2',
);

# my $fnm_utheses_faculty_map_old= '/home/gg/work/gitlab.uv/zid-services/utheses-admin-tools/faculty/univie.json';
my $fnm_utheses_departments_map_new= '/home/gg/work/gitlab.uv/zid-services/utheses/uthesesAPI/UthesesAPI/Resources/Departments.json';

my $utheses_faculty_map;
my $utheses_faculty_map_loaded= 0;
# NOTE: alternative module (currently without persistent department identifier)
# https://gitlab.uv.univie.ac.at/zid-services/hopla/blob/dev-utheses/lib/utheses_data.pm

# https://gitlab.uv.univie.ac.at/zid-services/utheses/blob/entw/uthesesAPI/UthesesAPI/Helpers/JsonTranslator.pm#L922
# sub _getThesisTypePhaidraId()
my %map_ot2ut_thesis_type=
(
  dipl        => 'https://pid.phaidra.org/vocabulary/9E94-E3F8', # 'Diplomarbeit'
  habil       => 'https://pid.phaidra.org/vocabulary/9ZSV-CVJH', # 'Habilitationsschrift', 'Habilitation'
  magister    => 'https://pid.phaidra.org/vocabulary/ST05-F6SP', # 'Magisterarbeit'
  master      => 'https://pid.phaidra.org/vocabulary/P2YP-BMND', # 'Masterarbeit'
  master_ulg  => 'https://pid.phaidra.org/vocabulary/H1TF-SDX1', # 'Master-Thesis (ULG)'
  phd         => 'https://pid.phaidra.org/vocabulary/1PHE-7VMS', # 'Dissertation'
);

my @ot2ut_synced_columns= qw( eprint_id eprint_status lastmod context ts_upload td_total error_code error_cnt utheses_id uploaded_fnm );
# Fields currently not available:
#  container_pid container_result document_pid document_result
#  activate_result import_code response_msg import_note

my $ot2ut_eprint_status= 'archive';
my $silent_upload_success= 0;
my $do_upload= 0;
my $ignore_errors= 0;

if ($0 eq './ot2ut.pl') { $op_mode= 'ot2ut'; $do_upload= 1; $MAX_SYNC= 1; }
if ($0 eq './oma.pl')   { $op_mode= 'oma';   $do_upload= 1; }

my $flag_add_utheses_policies= 1;
my $flag_add_identifiers= 1;

my %bucketlist_column_descriptions=
(
  bucket => 'Kategorie bzw. Klassifizierung - wird dynamisch erstellt - somit keine ID',
  eprint_status => 'Status [archive/buffer/etc]',
  einverstaendnis => 'Werknutzung [TRUE/FALSE/NULL=nicht vorhanden]',
  sperre => 'Sperre nach §86 gesetzt [TRUE/FALSE/NULL=nicht vorhanden]',
  hds => 'Has Date Sperre [future/past/no]',
  fts => 'Full-Text-Status [public/restricted]',
  docs => 'Verfügbare Dokumente [1 = Thesis]',
  pub => 'Zahl der zugaenglichen Dokumente',
  restr => 'Zahl der gesperrten Dokumente',
  embargo =>  'Zahl der Dokumente mit Embargo',
  errors => 'z.B. Datensatz entspricht nicht den Erwartungen',

  # Abbildung bzw. Status in utheses
  lrq => 'utheses policy flag: Lock-Request (wird ergaenzt, wenn Sperrdatum vorhanden: wird in Folge für den Import aus Hopla verwendet.)',
  lst => 'utheses policy flag: Lock-Status nach §86',
  kwl => 'utheses policy flag: Keywords-Locked',
  absl => 'utheses policy flag: Abstract-Locked',
  ftl => 'utheses policy flag: Full-Text-Locked',
  urn => '[ID present/NULL]',
  doi => '[ID present/NULL]',

  # Anzahl auf Instanz
  othes => 'Zahl der Objekte in Eprints DB (othes)',
  entw => 'Zahl der Objekte in utheses Entw',
  test => 'Zahl der Objekte in utheses Test',
  prod => 'Zahl der Objekte in utheses Prod',
);

my %ot2ut_sync_anyway= map { $_ => 1 } qw(); # these eprint_ids should be synced, even if they were already marked as ok

# fixup names
my $fn_init= 0;
local *FN_lst;
my @fn_lst= qw(ac_number eprint_id column_name name notes1 fixed1 nn vn);
my $fn_cnt= 0;

# This should be factored out!!
my @first_names= qw(
  Adam Adelheid Alejandro Alexa Alexander Alexandra Alfred Alice Alina
  Alois Alojz Amadou-Lamine Ana András Andre Andrea Andreas Angela Angeles
  Ania Anika Anjte Anke Anna Annegret Annemarie Anton Antonio Aprile
  Armando Armin Arnd Arno Arnold Arthur Aslan Astrid Awad Axel Barbara
  Barbara-Amina Beatrice Berhard Bernd Bernd-Christian Bernhard Berta
  Bertrand Bettecke Bettina Birgit Birgitta Björn Bodo Bodomo Boris
  Brigitta Brigitte Caecilie Cengiz Cesare Chris Christa Christian Christiane
  Christian-Hubert Christine Christof Christoph Christopher Claire
  Claudia Claus Clemens Constanze Dagmar Daiana Daniel Daniela David
  Diamantopoulos Diana Dieter Dietmar Dimitris Dirim Donald Doris Dorothea
  Dorothée Eberhard Eckhard Edit Eduard Elisabeth Elke Elmar Erhard
  Erich Erik Ernst Erwin Estella Esther Eugen Eva Eva-Maria Eveline Evrim Ewald
  Federico Fedor Ferdinand Floortje Florian France Frank Franz
  Franz-Markus Franz-Stefan Friederike Friedhelm Friedrich Frigerio
  Fritz Gabriel Gabriela Gabriele Garcia Georg George Georgios Gerald Gerda
  Gerhard Gerit Germain Gernot Gero Gerte Gertraud Gil Gordon Gottfried
  Gualtiero Gunda Gunnar Gunter Günter Günther Gustav Gyongyi
  Gyöngyi Hanna Hannes Hanno Hans Hans-Georg Hans-Jürgen Hanspeter
  Harald Heidemarie Heiner Heinrich Heinz Helen Helga Helmut Helmuth
  Henk Henning Henry Herbert Hermann Hermine Herwig Hilde Hildegard
  Horst Hristov Iacopo Igor Ilona Ilse Ines Ingeborg Ingfrid Ingo Ingrid Irene
  Irmgard Irmtraud Isabella Isabelle Item Jadranka Jan Jan-Heiner
  Jens Jiří Joan Joao Johann Johanna Johannes Joost Jörg Jorinde Josef
  József Judith Julia Julius Jürg Jürgen Jutta Karel Karen
  Karin Karl Karl-Heinz Katharina Katharine Kathrin Katja Katrin Kerstin
  Kim Kirsten Klara Klára Klaus Klaus-Börge Klaus-Dieter Klemens Konrad
  Konstanze Korina Kornelia Kristina Kurt Larisa Leopold Lieselotte Lorenz
  Lothar Ludger Lukas Lydia Maciel Magdalena Majchrzak Manfred Manuela
  Marc Marcello Marco Margareta Margarete Margareth Margaretha Margit
  Maria Marianne Marie-France Marija Mario Marion Marko Markus Martin
  Martina Mathilde Matthias Maximilian Melissa Meta Michael Michaela
  Michaela-Maria Michal Michela Michele Michèle Michelle Milena Mira
  Miranda Mitchell Moga Mona Monika Monje Murray Nadine Nadja Natalia Nicole Nikolaus
  Norbert Nuno Oliver Oskar Oskár Otmar Otto Patricia Patrick Paul
  Peter Petra Philip Philipp Pia Qi Rainer Ralf Ramon Ramón Raphael
  Regina Regine Reingard Reinhard Reinhold Renate René Richard Robert
  Robin Roland Ronald Rosa Rüdiger Rüdiger Rudolf Rupert Ruth Sabina
  Sabine Sami Sandra Sarah Sascha Saskia Savvas Sebastian Segeja Serge
  Sergey Sergio Siegfried Sieglinde Sigmar Sigrid Simon Sonia Sonja
  Sophie Stefan Stefanie Stefan-Michael Steffen Stephan Stephanie Steven Susanne
  Susi Suzanne Sylvia Tamara Taťána Tatjana Tecumseh Theodoros Thierry Thilo
  Thomas Timothy Tobias Tom Udo Ulf Ulrich Ulrike Urs Ursula Ute Verena
  Veronika Viera Viktor Vincente Violetta Vittorio Vlastimil Waldemar Walter
  Werner Wieland Wilfried Wilhelm Wolfgang Wolfram Wynfrid Yan Yulia
);

my %first_names= map { $_ => 1 } @first_names;

my %name_fixup_by_name=
(
  'THEIS Lioba' => { fn => 'Theis, Lioba' },
  'Univ. Prof in. Dr. in med.univ. Margarethe Geiger' => { fn => 'Geiger, Margarethe' },
  'Mag. Dr, Manfred Glauninger, Privatdoz.' => { fn => 'Glauninger, Manfred' },
  'Dipl.-Ing. Dr. -Ing. Henry Jäger' => { fn => 'Jäger, Henry' },
  'Frau Assoz. Profin. MMaga. DDrin. Esther Ramharter' => { fn => 'Ramharter, Esther' },
  'ao. Univ.-Prof. tit. Univ.-Prof. Dipl.-Ing. Dr. Erich Schikuta' => { fn => 'Schikuta, Erich' },
  'Pr. Dr. Arthur Rachwald' => { fn => 'Rachwald, Arthur' },
  'Dr. Manuel.D Montaño' => { fn => 'Montaño, Manuel D.' },
  'V.-Prof. Doz. Dr. Marie- France Chevron' => { fn => 'Chevron, Marie-France' },
  'Mgr. Michal Dovrecký. PhD' => { fn => 'Dovrecký, Michal' },
  'ao. Univ.-Prof. Dipl.-Geol. Dr. Christa-Ch. Hofmann' => { fn => 'Hofmann, Christa-Ch.' },
  'Bago- Pilátová Martina' => { fn => 'Bago-Pilátová, Martina' },
  'Ania-Martinez Ana Begona' => { fn => 'Ania-Martinez, Ana Begona' },
  'DR. DR. AMADOU-LAMINE SARR' => { fn => 'Sarr, Amadou-Lamine' },
  'em. Univ. Prof .DDr. Paul Michael Zulehner' => { fn => 'Zulehner, Paul Michael' },
  'Hennig Schluß' => { fn => 'Schluß, Henning' },
  'Univ. Lektor Mag. Dr. Christian Sitte' => { fn => 'Schluß, Henning' },
  'RACHWALD ARTHUR' => { fn => 'Rachwald, Arthur R.' },
  'Professor Dr. Arthur R. Rachwald' => { fn => 'Rachwald, Arthur R.' },
  'Univ. Lektor Mag. Dr. Christian Sitte' => { fn => 'Sitte, Christian' },
  'Univ.-Prof. Dr. Mathes, Klaus-Dieter, Privatdoz. M.A.' => { fn => 'Mathes, Klaus-Dieter' },
  'Univ.-Prof.MMag DDr. Rupert Klieber' => { fn => 'Klieber, Rupert' },
  'Em.Univ.-Prof.Dipl.Soz.Dr. Paul Kellermann' => { fn => 'Kellermann, Paul' },
  'Univ.-Ass. Mag. Dr. Matthias Flatscher' => { fn => 'Flatscher, Matthias' },
  'Univ.-Prof. Dipl-Ing. Dr. Arnold Baca' => { fn => 'Baca, Arnold' },
  'PhD Savvas Stafylidis' => { fn => 'Stafylidis, Savvas' },
  'Dipl.-Ernährungswiss. Dr. Barbara Lieder' => { fn => 'Lieder, Barbara' },
  'Weber, Gerhard, W.' => { fn => 'Weber, Gerhard W.' },
  'Dr. Martin Melzer, LL.M., TEP' => { fn => 'Melzer, Martin' },
  'Monje Quiroga Francisco' => { fn => 'Francisco, Monje Quiroga' },
  'Univ.-Prof. MMMMag. Dr. Pokorny, Lukas, M.A.' => { fn => 'Pokorny, Lukas' },
  'e.m. a.o. Unif.-Prof. Dr. Olaf Bockhorn' => { fn => 'Bockhorn, Olaf' },
  'o. Univ.-Prof. DDr. Ludger Müller M.A.' => { fn => 'Müller, Ludger' },
  'WEISS Friedl' => { fn => 'Weiss, Friedl' },
  'Priv. Uni. Doz. Mag. DDr. Julia Wippersberg' => { fn => 'Wippersberg, Julia' },
  'Univ.-Prof Mag. Dr. Gerhard Budin' => { fn => 'Budin, Gerhard' },
  'Uni.-Prof. Dr. Ulrich Teichler' => { fn => 'Teichler, Ulrich' },
  'Ao. Univ.-Porf. Dr. REINPRECHT Christoph' => { fn => 'Reinprecht, Christoph' },
  'ao. Univ.-Prof. Dr. Fritz (Friedrich) Hausjell' => { fn => 'Hausjell, Friedrich' }, # laut ufind ist der Vorname "Friedrich", siehe https://ufind.univie.ac.at/en/person.html?id=1872
  'Frank Christa' => { fn => 'Frank, Christa' },
  'Uni. Prof. Dr. Dr. hc. Peter Fischer' => { fn => 'Fischer, Peter' },
  'MR Dr. Andreas SOMMER' => { fn => 'Sommer, Andreas' },
  'Univ.-Prof. DI Dr. Wolfgang Kainz' => { fn => 'Kainz, Wolfgang' },
  'Mag.a art. Dr.in.phil Sabine Prokop' => { fn => 'Prokop, Sabine' },
  'Univ.-Prov. Dr. Biewer Gottfried' => { fn => 'Biewer, Gottfired' },
  'Kosman Admial ' => { fn => 'Admial, Kosman' }, # that's a guess because: eprint_id 62899 has mitbetreuer='Pokorny Lukas', betreuer='Langer Gerhard', beurteiler_1='Kosman Admial', beurteiler_2='Ruzer Serge'
  'Privatdozent Mag. Dr. Ulrich Tran' => { fn => 'Tran, Ulrick' },
  'Lueger - Schuster Brigitte' => { fn => 'Lueger-Schuster, Brigitte' },

# 'PD DDr Wippersberg Julia' => { fn => 'Wippersberg, Julia' },
);

my %name_fixup_by_eprint_id=
(
  '23255' => { betreuer => 'Frank, Christa' },
  '46724' => { betreuer => 'Leitner, Manuela', beurteiler_1 => 'Leitner, Manuela' }, # othes only shows "Manuela", see https://ubdata.univie.ac.at/AC14545271
  '47697' => { betreuer => 'Fischer, Peter' },
  '48949' => { beurteiler_1 => 'Rüffler, Friedrich' },
  '49466' => { betreuer => 'Frank, Rüdiger' },
  '58166' => { betreuer => 'Marko, Doris' }, # othes says: "Marko Doris" which results in a wrong pick; there are also objects that say "Doris Marko" which lead to a correct pick
  '55849' => { betreuer => 'Kirchmayr-Schliesselberger, Sabine' },
  '60229' => { betreuer => 'Slunecko, Thomas' },
  '60326' => { mitbetreuer => 'Liebhart, Karin' },
  '60552' => { betreuer => 'Hashi, Hisaki' }, # https://ufind.univie.ac.at/en/person.html?id=13985
  '60679' => { betreuer => 'Reichertz, Jo' },
  '60745' => { betreuer => 'Leoonida, Fusani' }, # see: https://ufind.univie.ac.at/en/person.html?id=56183
  '60808' => { beurteiler_1 => 'Weigelin-Schwiedrzik, Susanne', betreuer => 'Weigelin-Schwiedrzik, Susanne' },
  '60977' => { betreuer => 'Hofmann, Thilo' },
  '61076' => { betreuer => 'Heinz, Sarah' },
  '61589' => { betreuer => 'Hofmann, Christa-Ch.' },
  '61603' => { beurteiler_2 => 'Sejdini, Zekirija' },
  '61899' => { betreuer => 'Heinz, Sarah' },
  '62393' => { betreuer => 'Rudolf, Claudia' },
  '62397' => { betreuer => 'Rudolf, Claudia' },
  '62568' => { mitbetreuer => 'Montaño, Manuel D.', betreuer => 'Hofmann, Thilo' },
  '62705' => { betreuer => 'Mathes, Klaus-Dieter' },
  '62734' => { betreuer => 'Kirchmayr-Schliesselberger, Sabine' },
  '63391' => { mitbetreuer => 'Garcia, Daniel', beurteiler_2 => 'Garcia, Daniel', },
  '63404' => { betreuer => 'Rachwald, Arthur R.' },
  '63461' => { betreuer => 'Marko, Doris' }, # othes says: "Marko Doris" which results in a wrong pick; there are also objects that say "Doris Marko" which lead to a correct pick
  '63699' => { betreuer => 'Wessner, Barbara' },
);

my $refresh_oldest_policies;

my %doc_embargo_dates;
my $base_path= '/var/www/ot2ut'; # TODO(maybe): get this from the config...
# END OT2UT: Othes to Utheses 
# ======================================================================

my @db_tables= ();
my @PARS;
my $debug_level= 0;
my $force= 0;
my $db_name;

my $arg;
while (defined ($arg= shift (@ARGV)))
{
  if ($arg eq '--') { push (@PARS, @ARGV); @ARGV= (); }
  elsif ($arg =~ /^--(.+)/)
  {
    my ($opt, $val)= split ('=', $1, 2);

       if ($opt eq 'help') { usage(); }
    elsif ($opt eq 'connect')
    {
      $op_mode= $opt;
      $db_name= $val || shift(@ARGV);

         if ($db_name eq 'e') { $db_name= 'eprints_database'; }
      elsif ($db_name eq 'i') { $db_name= 'irma_database'; }
    }
    elsif ($opt eq 'ci')      { $op_mode= 'connect'; $db_name= 'irma_database'; }
    elsif ($opt eq 'ce')      { $op_mode= 'connect'; $db_name= 'eprints_database'; }
    elsif ($opt eq 'ch')      { $op_mode= 'connect'; $db_name= 'hopla_database'; }
    elsif ($opt eq 'cu')      { $op_mode= 'connect'; $db_name= 'utheses_database'; }
    elsif ($opt eq 'bup1')    { $op_mode= 'backup';  $db_name= 'eprints_database'; @db_tables= qw(eprint); }
    elsif ($opt eq 'mab')     { $op_mode= $opt; } # manual checking
    elsif ($opt eq 'marc')    { $op_mode= $opt; } # manual checking
    elsif ($opt eq 'rms')     { $op_mode= $opt; $MAX_URN_REQUESTS=  $val if (defined ($val)); } # transfer data from eprints into redmine
    elsif ($opt eq 'mab4ep')  { $op_mode= $opt; $MAX_MAB_REQUESTS=  $val if (defined ($val)); }
    elsif ($opt eq 'marc4ep') { $op_mode= $opt; $MAX_MARC_REQUESTS= $val if (defined ($val)); }
    elsif ($opt eq 'nbn4ep')  { $op_mode= 'nbn4ep'; $MAX_URN_REQUESTS= $val if (defined ($val)); } # request urns for eprints
    elsif ($opt eq 'urn4xx')  { $op_mode= $opt; $MAX_SYNC= $val if (defined ($val)); } # request urns for ???
    elsif ($opt eq 'doi4ep')  { $op_mode= $opt; $MAX_SYNC= $val if (defined ($val)); } # new 2019-06-18
    elsif ($opt eq 'ot2ut')   { $op_mode= $opt; $MAX_SYNC= $val if (defined ($val)); } # new 2019-11-28
    elsif ($opt eq 'ot2ut-context') { $ot2ut_context= $val || shift(@ARGV); }
    elsif ($opt eq 'rqm')     { $op_mode= $opt; } # Request Monitor
    elsif ($opt eq 'auto')    { $op_mode= $opt; } # perform a complete run including resetting all errors
    elsif ($opt eq 'debug')   { $debug_level= $val || 1; }
    elsif ($opt eq 'debug_names')   { $op_mode= 'debug_names'; }
    elsif ($opt eq 'debug_classifications' || $opt eq 'DC')   { $op_mode= 'debug_classifications'; }
    elsif ($opt eq 'debug_keywords')  { $op_mode= 'debug_keywords'; }
    elsif ($opt eq 'debug_abstracts') { $op_mode= 'debug_abstracts'; }
    elsif ($opt eq 'debug_stkz')      { $op_mode= 'debug_stkz'; }
    elsif ($opt eq 'debug_stbez')     { $op_mode= 'debug_stbez'; }
    elsif ($opt eq 'debug_filenames') { $op_mode= 'debug_filenames'; }
    elsif ($opt eq 'update-policies') { $op_mode= 'update-policies'; $refresh_oldest_policies= $val if ($val); }
    elsif ($opt eq 'policies-stats')  { $op_mode= 'policies-stats'; }
    elsif ($opt eq 'export-redirect' || $opt eq 'export-redis') { $op_mode= 'export-redirect'; }
    elsif ($opt eq 'max')     { $MAX_SYNC= $val || shift (@ARGV); }
    elsif ($opt eq 'mab-age') { $MAX_MAB_AGE= $val || shift (@ARGV); } # in seconds
    elsif ($opt eq 'marc-age'){ $MAX_MARC_AGE= $val || shift (@ARGV); } # in seconds
    elsif ($opt eq 'export-eprints')  { $op_mode= 'export-eprints'; }
    elsif ($opt eq 'export-migration') { $op_mode= 'export-migration'; }
    elsif ($opt eq 'reset')   { $op_mode= $opt;  }
    elsif ($opt eq 'force')   { $force= defined($val) ? $val : 1; }
    elsif ($opt eq 'upload')  { $do_upload= defined($val) ? $val : 1; }
    elsif ($opt eq 'buffer')  { $ot2ut_eprint_status= 'buffer'; }
    elsif ($opt eq 'ignore-errors') { $ignore_errors= defined($val) ? $val : 1; }
    elsif ($opt eq 'oma' || $opt eq 'PAF') { $op_mode= 'oma'; $do_upload= 1; }
    else { usage("unknown option $arg"); }
  }
  elsif ($arg =~ /^-(.+)/)
  {
    foreach my $opt (split ('', $1))
    {
         if ($opt eq 'h') { usage(); exit (0); }
      # elsif ($opt eq 'x') { $x_flag= 1; }
      else { usage("unknown option $arg"); }
    }
  }
  else
  {
    push (@PARS, $arg);
  }
}

# print join (' ', __FILE__, __LINE__, 'caller=['. caller() . ']'), "\n";

my $sleep_urn_request= 3;
my $running= 1;
$SIG{INT}= sub { $running= 0; };
$SIG{USR1}= sub { $running= 0; };

my $serving_requests= 1;
$SIG{USR2}= sub { $serving_requests= 0; };

# Agent mode
my $db_ot2ut;
my $col_msg;
my $col_activity;
my $last_activity= 0;

my $cnf= Util::JSON::read_json_file ($config_fnm);

if ($op_mode eq 'connect')
{
  connect_db_interactive ($cnf, $db_name, 'connect');
}
elsif ($op_mode eq 'backup')
{
  connect_db_interactive ($cnf, $db_name, 'backup', @db_tables);
}
elsif ($op_mode eq 'rms') # redmine sync: transfer info from eprints application (table eprint) into redmine (table eprints)
{
  rms();
}
elsif ($op_mode eq 'mab') # single ac_number check in Aleph MAB collection
{
  my $mab_db= get_mab_db ($cnf);
  check_mab ($mab_db, @PARS);
}
elsif ($op_mode eq 'mab4ep') # redmine sync: transfer info from MAB (AC-INFO) database into redmine (table ac_numbers)
{
  my $irma_na= get_irma_na_db($cnf);
  my $irma_ac_data= $irma_na->fetch_ac_data();

  my $mab_db= get_mab_db ($cnf);
  my $updates= merge_mab ($irma_na, $mab_db, $irma_ac_data);
  print __LINE__, " updates: ", main::Dumper ($updates);
}
elsif ($op_mode eq 'marc') # single ac_number check in Alma MARC collection
{
  my $marc_db= get_marc_db ($cnf);
  check_marc ($marc_db, @PARS);
}
elsif ($op_mode eq 'marc4ep') # redmine sync: transfer info from MAB (AC-INFO) database into redmine (table ac_numbers)
{
  marc4ep();
}
elsif ($op_mode eq 'nbn4ep') # update urn data in redmine: ac_numbers ==> urn-registration (via PAF) ==> ac_number ==> eprints
{
  nbn4ep();
}
elsif ($op_mode eq 'rqm') # request monitor
{
  my $marc_db= get_marc_db ($cnf);
  check_requests ($marc_db, @PARS);
}
elsif ($op_mode eq 'auto') # request monitor
{
  my $marc_db= get_marc_db ($cnf);

  # normal run
  rms();
  marc4ep();
  check_requests ($marc_db, 10, 0);
  marc4ep();
  nbn4ep();
  check_requests ($marc_db, 10, 0);
  nbn4ep();
  check_requests ($marc_db, 10, 0);
  rms();

=begin comment

somehow, extra requests for alma_cat are generated, when errors are reset this way
  # error correction run
  reset_errors('ALL', 'REALLY');
  # do not marc4ep() here, this is already handled by reset_errors() !
  check_requests ($marc_db, 10, 1);
  marc4ep();
  nbn4ep();
  check_requests ($marc_db, 10, 1);
  nbn4ep();
  check_requests ($marc_db, 10, 1);
  rms();
=end comment
=cut

}
elsif ($op_mode eq 'urn4xx') # WIP
{
  my $irma_na= get_irma_na_db($cnf);

  verify1_urn_registration1 ($irma_na, undef, 'eprints');
}
elsif ($op_mode eq 'export-eprints') # WIP
{
  my $irma_na= get_irma_na_db($cnf);

  export_csv ($irma_na, 'eprints');
}
elsif ($op_mode eq 'export-migration') # WIP
{
  export_migration_data();
}
elsif ($op_mode eq 'doi4ep') # WIP, started 2019-06-18
{
  doi4ep();
}
elsif ($op_mode eq 'ot2ut') # WIP, started 2019-11-28
{
  ot2ut(@PARS);
}
elsif ($op_mode eq 'oma')
{
  oma();
}
elsif ($op_mode eq 'debug_names')
{
  debug_names();
}
elsif ($op_mode eq 'debug_keywords')
{
  debug_keywords();
}
elsif ($op_mode eq 'debug_abstracts')
{
  debug_abstracts();
}
elsif ($op_mode eq 'debug_classifications')
{
  # print "cnf: ", Dumper ($cnf);

  debug_classifications();
}
elsif ($op_mode eq 'debug_stkz')
{
  debug_stkz();
}
elsif ($op_mode eq 'debug_stbez')
{
  debug_stbez();
}
elsif ($op_mode eq 'debug_filenames')
{
  debug_filenames();
}
elsif ($op_mode eq 'update-policies')
{
  update_policies(@PARS);
  policies_stats('policies updated');
}
elsif ($op_mode eq 'policies-stats')
{
  policies_stats(@PARS);
}
elsif ($op_mode eq 'export-redirect')
{
  export_redirect();
}
elsif ($op_mode eq 'reset') # reset error conditions for given ac_numbers
{
  reset_errors(@PARS);
}
else { usage(); }

if ($die_nbn_already_defined)
{
  print "ATTN: die_nbn_already_defined=[$die_nbn_already_defined]\n";
}

activity({ activity => 'exiting' });

exit(0);

=head1 CONVENIENCE FUNCTIONS

=head2 usage()

=cut

sub usage
{
  if (@_)
  {
    print join ("\n", @_, '');
    sleep 2;
  }
  system ('perldoc', $0);
  exit;
}

sub rms
{
  print "starting rms\n";
  my $irma_na= get_irma_na_db($cnf);
  my $epr= get_eprints_db($cnf);

  eprint_full_sync ($irma_na, $epr);
  print "finished rms\n";
}

sub marc4ep
{
  print "starting marc4ep\n";
  my $irma_na= get_irma_na_db($cnf);
  my $irma_ac_data= $irma_na->fetch_ac_data();

  my $marc_db= get_marc_db ($cnf);
  my $updates= merge_marc ($irma_na, $marc_db, $irma_ac_data);
  print __LINE__, " updates: ", main::Dumper ($updates);
  print "finished marc4ep\n";
}

sub nbn4ep
{
  print "starting nbn4ep\n";
  my $irma_na= get_irma_na_db($cnf);
  # TODO: maybe this should be a separate subroutine
  {
    my $irma_jq= new IRMA::urn::nbn::at ('config_data' => $cnf, 'agent_name' => $agent_name);
    # print "irma_jq: ", Dumper ($irma_jq);

    my ($cnt_req, $upd_ids1)= request_urn_registration ($irma_na, $irma_jq, 'eprints');
    print "urns requested=[$cnt_req]\n";

    my ($cnt_check, $upd_ids2)= check_urn_registration ($irma_na, $irma_jq, 'eprints');
    print "urns checked=[$cnt_check]\n";
  }

  my ($cnt_u2e, $upd_ids2)= urn_to_eprints ($irma_na);
  print "urns transcribed=[$cnt_u2e]\n";
  print "finished nbn4ep\n";
}

sub reset_errors
{
  my @pars= @_;

  print "started reset_errors\n";

  my $irma_na= get_irma_na_db($cnf);

  if ($pars[0] eq 'ALL')
  {
    my $doit= ($pars[1] eq 'REALLY');

    my $error_hash= get_ac_errors($irma_na);
    @pars= map { $error_hash->{$_}->{ac_number} } keys %$error_hash;
    print "found ", scalar (@pars), " items\n";
    print join (' ', @pars), "\n";

    exit unless ($doit);
  }

  reset_ac_errors ($irma_na, @pars);

=begin comment

  $MAX_MAB_AGE= 1;
  # from CLI switch "--mab"
  my $mab_db= get_mab_db ($cnf);
  check_mab ($mab_db, @pars);

=end comment
=cut

  $MAX_MARC_AGE= 1;
  my $marc_db= get_marc_db ($cnf);
  check_marc ($marc_db, @pars);

  print "finished reset_errors\n";
}

=head2 connect_db_interactive($cnf, $db_name)

Connect to the database which logic name is searched in $cnf, the hash
containing the configuration data.

=cut

sub connect_db_interactive
{
  my $cnf= shift;
  my $db_name= shift || 'database';
  my $action= shift || 'connect';
  my @tables= @_;

  my ($driver, $dbcc)= IRMA::db::get_db_config ($cnf, $db_name);

  if ($driver eq 'mysql')
  {
    my ($h, $u, $p, $db)= map { $dbcc->{$_} or die "db config without $_" } qw(host username password database);
    $ENV{MYSQL_PWD}= $p;

    my @cmd;
    if ($action eq 'connect')
    {
      @cmd= ('mysql', '--default-character-set=utf8', '-A', '-h', $h, '-u', $u, $db);
    }
    elsif ($action eq 'backup')
    {
      my $dump_file= join ('_', $db_name, @db_tables, Util::ts::ts_ISO_gmt(time())) . '.dump';
      @cmd= ('mysqldump', '--default-character-set=utf8', '-h', $h, '-u', $u, '-r', $dump_file, $db);
      push (@cmd, @db_tables) if (@db_tables);
    }
    print "cmd=[",join (' ', @cmd), "]\n";
    system (@cmd);
  }
}

=head1 MAB DATABASE FUNCTIONS

=head2 get_mab_db($cnf)

access MAB database from config data found in $cnf

=cut

sub get_mab_db
{
  my $cnf= shift;

  my ($driver, $dbcc)= IRMA::db::get_db_config ($cnf, 'mab_database');
  # print __LINE__, " mab: driver=[$driver] dbcc=", Dumper ($dbcc);

  my $mab= new IRMA::Mab ('driver' => $driver, 'dbcc' => $dbcc);
  # print __LINE__, " mab: ", Dumper ($mab);
  $mab;
}

=head2 check_mab ($mab, @ac_numbers);

check ac_numbers from list in MAB database (usually interactively)

=cut

sub check_mab
{
  my $mab= shift;
  my @ac_numbers= @_;

  AC_NUMBER: foreach my $ac_number (@ac_numbers)
  {
    print __LINE__, " ac_number=[$ac_number]\n";
    my ($ac_status, $ts, $mab_data, $error_info)= check_mab_ac_number ($mab, $ac_number, 1);
    print __LINE__, " ac_number=[$ac_number] ac_status=[$ac_status] ts=[$ts] mab_data: ", Dumper ($mab_data);
    print __LINE__, " error_info=[$error_info]\n" if ($ac_status eq 'error');
  }
}

=head2 my ($ac_status, $item_epoch, $mab_data, $error_info)= check_mab_ac_number ($mab, $ac_number, $make_request)

retrieve $mab_data from $mab (usually in MongoDB as aleph.mab collection);
if the item is not there or too old (older than $MAX_MAB_AGE) and $make_request is true, it will be fetched.

=head3 return values

 * ac_status:
   * unknown   => no information avaliable
   * avaliable => mab item was found and retrieved; item_epoch indicates when the item was fetch from Aleph
   * requested => mab item was requested from upstream; only when object $make_request is true
 * item_epoch: (epoch) when applicable
 * mab_data: retrieved mab_data from MongoDB
 * error_info: indicates whatever went wrong

=cut

sub check_mab_ac_number
{
  my $mab= shift;
  my $ac_number= shift;
  my $make_request= shift;

  my $col= $mab->get_collection ('aleph.mab'); # TODO: parameter or config
  # print __LINE__, " col: ", Dumper ($col);

  my $sel= { 'ac_number' => $ac_number };
  print __LINE__, " sel: ", Dumper ($sel);

  my @res= $col->find($sel)->all();

  my $mk_request= 0;
  my $mapped_mab_record;
  my $ac_status= 'unknown';
  my $fetched;
  my $error_info;

     if (@res == 0) { $mk_request= 1; $ac_status= 'requested'; }
  elsif (@res == 1)
  {
    my $found= $res[0];

    $fetched= $found->{fetched};
    # TODO: if too old, insert request anyway

    if ($fetched + $MAX_MAB_AGE < time () && $make_request)
    {
      $mk_request= 1;
      $ac_status= 'requested';
    }
    print "found: [", scalar localtime($fetched), "] mk_request=[$mk_request]\n";

    unless ($mk_request)
    {
      # print __LINE__, " found: ", Dumper ($found);

      my $mab_record= $found->{'xmlref2'}->{'record'}->[0]->{metadata}->[0]->{oai_marc}->[0];

      if (defined ($mab_record))
      {
        # print __LINE__, " mab_record: ", Dumper ($mab_record);
        $mapped_mab_record= map_mab ($ac_number, $mab_record);
        # print __LINE__, " mapped_mab_record: ", Dumper ($mapped_mab_record);
        $ac_status= 'available';
      }
      else
      {
        $error_info= "mab record not found in fetched data ac_number=[$ac_number]";
        $ac_status= 'error';
      }
    }
  }
  else
  {
    print __LINE__, " res: ", Dumper (\@res);
    $ac_status= 'error';
    $error_info= 'too many records in aleph.mab collection';
  }

  print __LINE__, " ATTN: ", $error_info, "\n" if (defined ($error_info));

  if ($mk_request)
  {
    if ($make_request)
    {
      my $request=
      {
        'agent'     => 'aleph_cat',
        'status'    => 'new',
        'action'    => 'update_aleph_2xml',
        'ac_number' => $ac_number,
      };

      my $req= $mab->get_collection ('requests');
      $req->insert ($request);

      # TODO/NOTE: for now, also request the Alma MARC record
      $request->{agent}= 'alma_cat';
      $request->{action}= 'update_alma_2xml';
      $req->insert ($request);
    }
    else
    {
      $ac_status= 'unknown';
    }
  }

  ($ac_status, $fetched, $mapped_mab_record, $error_info);
}

sub map_mab
{
  my $ac_number= shift;
  my $mab_record= shift;

  my %res;
  if (exists ($mab_record->{varfield}))
  {
    my $vfa= $mab_record->{varfield};
    unless (ref ($vfa) eq 'ARRAY')
    {
      print "ac_number=[$ac_number]\n";
      print "mab_record: ", Dumper ($mab_record);
      die "varfield not an array reference! ac_number=[$ac_number]";
    }

    foreach my $vf (@$vfa)
    {
      my $tag1= join (':', map { $vf->{$_} } qw(id i1 i2));
      SF: foreach my $sf (@{$vf->{subfield}})
      {
        my $tag= join (':', $tag1, $sf->{label});

=begin comment

... this happens all the time
        if (exists ($res{$tag}))
        {
          print "ATTN: ignored duplicate tag=[$tag] in ac_number=[$ac_number]\n";
          next SF;
        }
        $res{$tag}= $sf->{content};

=end comment
=cut

        push(@{$res{$tag}}, $sf->{content});
      }
    }
  }

  (wantarray) ? %res : \%res;
}

=head2 $xx= merge_mab ($irma, $mab_db, $d_na);

  $irma: handle for IRMA::NA database (MySQL)
  $mab_db: handle for MAB Database (MongoDB)
  $d_na: data from fetch_ac_data();

merge mab data into redmine ac_numbers table

=cut

sub merge_mab
{
  my $irma= shift;
  my $mab_db= shift;
  my $d_na= shift;

  my $irma_db= $irma->connect();
  # print __LINE__, " d_na: ", Dumper ($d_na);
  # $irma_db->verbose(1);

  my %updates;
  foreach my $id (sort keys %$d_na)
  {
    my $row= $d_na->{$id};
# print __LINE__, " merge_mab: id=[$id] row=", main::Dumper($row);

    my ($ac_status, $context)= map { $row->{$_} } qw(ac_status context);

    next if ($ac_status eq 'done'); # TODO: allow revalidation, possibly triggered by attributes like ac_number, age, whatever...

    if ($context ne 'eprints')
    {
      print "unknown context=[$context]; ignored\n"; # not our business
      return undef;
    }

    my ($st, $upd)= merge_mab_item_eprints ($irma_db, $mab_db, $row);
# print __LINE__, " st=[$st] upd=[$upd]\n";

    $updates{$id}= $upd if (defined ($upd));
  }

  (wantarray) ? %updates : \%updates;
}

sub merge_mab_item_eprints
{
  my $irma_db= shift;
  my $mab_db= shift;
  my $row= shift;

# print __LINE__, " merge_mab_item_eprints: row=", main::Dumper($row);
  my ($id, $ac_number, $ac_status, $context_id)= map { $row->{$_} } qw(id ac_number ac_status context_id);

    if ($ac_status eq 'error')
    {
      print "NOTE: ac_status=[error]; ignored\n" if ($show_TODOs); # TODO: re-check or something else ...
      return undef;
    }

    if ($ac_status eq 'verified')
    {
      print "TODO: go ahead checking ac_number=[$ac_number], ac_status=[$ac_status]\n" if ($show_TODOs);

=begin comment

no mab data present here!

      my $ac_urn= $mab_data->{'552:b:1:a'}->[0]; # TODO: parametrize?

if (defined ($ac_urn))
{ # TODO: this is not expected yet ...
  print __LINE__, " row: ", Dumper($row);
  die "ac_urn=[$ac_urn] already defined!";
}

=end comment
=cut

      return undef;
    }

    unless ($ac_status eq 'new' || $ac_status eq 'requested')
    {
      print "ATTN: unknown ac_status=[$ac_status]; ignored\n";
      return undef;
    }

# BEGIN validation code

    my $update;
    my %upd= ();

    my ($mab_item_status, $mab_item_epoch, $mab_data, $mab_error_info)=
       check_mab_ac_number ($mab_db, $ac_number,
         ($cnt_mab_requests >= $MAX_MAB_REQUESTS || $ac_status eq 'requested') ? 0 : 1 # too many open requests ...
         );
    print __LINE__, " mab_item_status=[$mab_item_status] checking row: ", Dumper ($row) if ($ac_number eq 'AC06678536');

    if ($mab_item_status eq 'available')
    {

# NOTE/TODO: maybe we should refactor out this block as "mab_verify()" or something like that which can then be used to re-validate already validated items

      my $ac_url= $mab_data->{'655:e:1:u'}->[0]; # TODO: parametrize?
      my $ac_urn= $mab_data->{'552:b:1:a'}->[0]; # TODO: parametrize?

if (defined ($ac_urn))
{ # TODO: this is not expected yet ...
  print __LINE__, " row: ", Dumper($row);
  die "ac_urn=[$ac_urn] already defined!";
}

    if ($mab_item_epoch + $MAX_MAB_AGE < time ())
    {
      print "ATTN: mab entry for $ac_number is too old, ignored! (maybe it needs to be requested again)\n";
      print __LINE__, " ac_number=[$ac_number] mab_item_epoch=[", scalar localtime($mab_item_epoch),
            " ] MAX_MAB_AGE=[$MAX_MAB_AGE]\n";
      return undef;
    }

      # NOTE/TODO: xml_url and so on are proably not good choices as attribute names in the database, maybe ac_url and so on would be better...
      $upd{xml_ts}= $mab_item_epoch;
      $upd{xml_url}= $ac_url;
      $upd{xml_urn}= $ac_urn;

      # 1. check if the URL is valid
      # TODO: there must be a better way to retrieve possible URL variants ...
      my @ctx_urls= IRMA::eprints::get_eprint_urls($context_id);

      my $ctx_url_ok;
      URL_CHECK: foreach my $ctx_url (@ctx_urls)
      {
        if ($ac_url eq $ctx_url)
        {
          $ctx_url_ok= $ctx_url; # TODO: since this is a preference-ordered list, this info could be used for something ...
          print "matching ac_url=[$ac_url] ctx_url=[$ctx_url]\n";
          last URL_CHECK;
        }
      }

      if (!defined ($ctx_url_ok))
      {
        print __LINE__, " ATTN: url_mismatch!\n";
        print " ac_url=[$ac_url]\n";
        print "ctx_urls=[", join ('|', @ctx_urls),"]\n";

        $update= 'error';
        $upd{error_source}= 'link_validation';
        $upd{error_text}= "link in ac_record=[$ac_url]";
      }
      else
      {
        $update= 'verified';
      }

# END validation code (block mismatch!)

    }
    elsif ($mab_item_status eq 'requested')
    {
      print __LINE__, " requested new item: ac_number=[$ac_number]\n";
      $update= 'requested';
      $cnt_mab_requests++;
    }
    elsif ($mab_item_status eq 'unknown') {} # NOP!
    elsif ($mab_item_status eq 'error')
    {
      print __LINE__, "mab_item_status='error'\n";
      $update= 'error';
      $upd{error_source}= 'mab_fetch';
      $upd{error_text}= $mab_error_info;
    }
    else
    {
      $update= 'error';
      $upd{error_source}= 'mab_fetch';
      $upd{error_text}= "unknown mab_item_status=[$mab_item_status] for ac_number=[$ac_number]";

      print __LINE__, " ATTN: ", $upd{error_text}, "\n";
    }

DONE:
    if (defined ($update))
    {
      $upd{ac_status}= $update;
      $irma_db->update('ac_numbers', $id, \%upd);
    }

  ($update, \%upd);
}

=head1 MARC DATABASE FUNCTIONS

=head2 get_marc_db($cnf)

access MARC database from config data found in $cnf

=cut

=begin comment

sub get_marc_db
{
  my $cnf= shift;

  my ($driver, $dbcc)= IRMA::db::get_db_config ($cnf, 'marc_database');
  # print __LINE__, " marc: driver=[$driver] dbcc=", Dumper ($dbcc);

  my $marc= new IRMA::db ('driver' => $driver, 'dbcc' => $dbcc);
  # print __LINE__, " marc: ", Dumper ($marc);
  $marc;
}

=end comment
=cut

sub get_marc_db
{
  my $cnf= shift;
  IRMA::db::get_any_db ($cnf, 'marc_database');
}

=head2 check_marc ($marc, @ac_numbers);

check ac_numbers from list in MARC database (usually interactively)

=cut

sub check_marc
{
  my $marc= shift;
  my @ac_numbers= @_;

  AC_NUMBER: foreach my $ac_number (@ac_numbers)
  {
    print __LINE__, " ac_number=[$ac_number]\n";
    my ($ac_status, $ts, $marc_data, $error_info)= check_marc_ac_number ($marc, $ac_number, 1);
    print __LINE__, " ac_number=[$ac_number] ac_status=[$ac_status] ts=[$ts] marc_data: ", Dumper ($marc_data);
    print __LINE__, " error_info=[$error_info]\n" if ($ac_status eq 'error');
    my ($ac_url, $ac_urn)= marc_extract_url_urn ($marc_data);
    print "ac_url=[$ac_url] ac_urn=[$ac_urn]\n";
  }
}

=head2 my ($ac_status, $item_epoch, $mab_data, $error_info)= check_marc_ac_number ($mab, $ac_number, $make_request)

retrieve $mab_data from $mab (usually in MongoDB as alma.marc collection);
if the item is not there or too old (older than $MAX_MARC_AGE) and $make_request is true, it will be fetched.

=head3 return values

 * ac_status:
   * unknown   => no information avaliable
   * avaliable => mab item was found and retrieved; item_epoch indicates when the item was fetch from Aleph
   * requested => mab item was requested from upstream; only when object $make_request is true
 * item_epoch: (epoch) when applicable
 * mab_data: retrieved mab_data from MongoDB
 * error_info: indicates whatever went wrong

=cut

sub check_marc_ac_number
{
  my $marc= shift;
  my $ac_number= shift;
  my $make_request= shift;

  my $col= $marc->get_collection ('alma.marc'); # TODO: parameter or config
  # print __LINE__, " col: ", Dumper ($col);

  my $sel= { 'ac_number' => $ac_number };
  print __LINE__, " sel: ", Dumper ($sel);

  my @res= $col->find($sel)->all();

  my $mk_request= 0;
  my $mapped_marc_record;
  my $ac_status= 'unknown';
  my $fetched;
  my $error_info;

     if (@res == 0) { $mk_request= 1; $ac_status= 'requested'; }
  elsif (@res == 1)
  {
    my $found= $res[0];

    $fetched= $found->{fetched};
    # TODO: if too old, insert request anyway

    print __LINE__, " check_marc_ac_number: fetched=[$fetched] (", scalar localtime($fetched), "), MAX_MARC_AGE=[$MAX_MARC_AGE], make_request=[$make_request]\n";
    if ($fetched + $MAX_MARC_AGE < time () && $make_request)
    {
      $mk_request= 1;
      $ac_status= 'requested';
    }
    print "found: [", scalar localtime($fetched), "] mk_request=[$mk_request]\n";

    unless ($mk_request)
    {
      # print __LINE__, " found: ", Dumper ($found);

      my $marc_record= $found->{'xmlref'}->{'records'}->[0]->{record}->[0]->{recordData}->[0]->{record}->[0];

      print __LINE__, " marc_record: ", Dumper ($marc_record);

      if (defined ($marc_record))
      {
        # print __LINE__, " marc_record: ", Dumper ($marc_record);
        $mapped_marc_record= map_marc ($ac_number, $marc_record);
        # print __LINE__, " mapped_marc_record: ", Dumper ($mapped_marc_record);
        $ac_status= 'available';
      }
      else
      {
        $error_info= "marc record not found in fetched data ac_number=[$ac_number]";
        $ac_status= 'error';
      }
    }
  }
  else
  {
    print __LINE__, " res: ", Dumper (\@res);
    $ac_status= 'error';
    $error_info= 'too many records in alma.marc collection';
  }

  print __LINE__, " ATTN: ", $error_info, "\n" if (defined ($error_info));

  if ($mk_request)
  {
    if ($make_request)
    {
      my $request=
      {
        'agent'     => 'alma_cat',
        'status'    => 'new',
        'action'    => 'update_alma_2xml',
        'ac_number' => $ac_number,
      };

      my $req= $marc->get_collection ('requests');
      $req->insert ($request);
      print __LINE__, " check_marc_ac_number: insert requests: ", Dumper($request);
    }
    else
    {
      $ac_status= 'unknown';
    }
  }

  ($ac_status, $fetched, $mapped_marc_record, $error_info);
}

sub map_marc
{
  my $ac_number= shift;
  my $marc_record= shift;

  my %res;
  if (exists ($marc_record->{datafield}))
  {
    my $dfa= $marc_record->{datafield};
    unless (ref ($dfa) eq 'ARRAY')
    {
      print "ac_number=[$ac_number]\n";
      print "marc_record: ", Dumper ($marc_record);
      die "datafield not an array reference! ac_number=[$ac_number]";
    }

    foreach my $df (@$dfa)
    {
      my $tag1= join (':', map { $df->{$_} } qw(tag ind1 ind2));
      SF: foreach my $sf (@{$df->{subfield}})
      {
        my $tag= join (':', $tag1, $sf->{code});

=begin comment

... this happens all the time
        if (exists ($res{$tag}))
        {
          print "ATTN: ignored duplicate tag=[$tag] in ac_number=[$ac_number]\n";
          next SF;
        }
        $res{$tag}= $sf->{content};

=end comment
=cut

        push(@{$res{$tag}}, $sf->{content});
      }
    }
  }

  (wantarray) ? %res : \%res;
}

sub merge_marc
{
  my $irma= shift;
  my $marc_db= shift;
  my $d_na= shift;

  my $irma_db= $irma->connect();
  # print __LINE__, " d_na: ", Dumper ($d_na);
  # $irma_db->verbose(1);

  my %updates;
  foreach my $id (sort keys %$d_na)
  {
    my $row= $d_na->{$id};
# print __LINE__, " merge_mab: id=[$id] row=", main::Dumper($row);

    my ($ac_status, $context)= map { $row->{$_} } qw(ac_status context);

    next if ($ac_status eq 'done'); # TODO: allow revalidation, possibly triggered by attributes like ac_number, age, whatever...

    if ($context ne 'eprints')
    {
      print "unknown context=[$context]; ignored\n"; # not our business
      return undef;
    }

    my ($st, $upd)= merge_marc_item_eprints ($irma_db, $marc_db, $row);
# print __LINE__, " st=[$st] upd=[$upd]\n";

    $updates{$id}= $upd if (defined ($upd));
  }

  (wantarray) ? %updates : \%updates;
}

sub marc_extract_url_urn
{
  my $marc_data= shift;

  # TODO: parametrize?
  # Comments:
  # * part 1 .. according to marc_bd.pdf
  # * part 2 .. value of subfield 3
  my $ac_url= $marc_data->{'856:4: :u'}->[0];                            # "Keine Information vorhanden"; Volltext am Hochschulschriftenserver der UB Wien
     $ac_url= $marc_data->{'856:4:0:u'}->[0] unless (defined ($ac_url)); # "Ressource"; Volltext am Hochschulschriftenserver der UB Wien; e.g. AC08823281
     $ac_url= $marc_data->{'856:4:1:u'}->[0] unless (defined ($ac_url)); # "Version der Ressource"; Volltext am Hochschulschriftenserver der UB Wien
     $ac_url= $marc_data->{'856:4:2:u'}->[0] unless (defined ($ac_url)); # "Verknüpfte Ressource"; Abstract am Hochschulschriftenserver der UB Wien

  my $ac_urn= $marc_data->{'024:7: :a'}->[0];

  return ($ac_url, $ac_urn);
}

sub merge_marc_item_eprints
{
  my $irma_db= shift;
  my $marc_db= shift;
  my $row= shift;

# print __LINE__, " merge_mab_item_eprints: row=", main::Dumper($row);
  my ($id, $ac_number, $ac_status, $context_id)= map { $row->{$_} } qw(id ac_number ac_status context_id);

    if ($ac_status eq 'error')
    {
      print "NOTE: ac_status=[error]; ignored\n" if ($show_TODOs); # TODO: re-check or something else ...
      return undef;
    }

    if ($ac_status eq 'verified')
    {
      print "TODO: go ahead checking ac_number=[$ac_number], ac_status=[$ac_status]\n" if ($show_TODOs);

=begin comment

no mab data present here!

      my $ac_urn= $marc_data->{'024:7: :a'}->[0]; # TODO: parametrize?

if (defined ($ac_urn))
{ # TODO: this is not expected yet ...
  print __LINE__, " row: ", Dumper($row);
  die "ac_urn=[$ac_urn] already defined!";
}

=end comment
=cut

      return undef;
    }

    unless ($ac_status eq 'new' || $ac_status eq 'requested')
    {
      print "ATTN: unknown ac_status=[$ac_status]; ignored\n";
      return undef;
    }

# BEGIN validation code

    my $update;
    my %upd= ();

    my ($marc_item_status, $marc_item_epoch, $marc_data, $marc_error_info)=
       check_marc_ac_number ($marc_db, $ac_number,
         ($cnt_marc_requests >= $MAX_MARC_REQUESTS || $ac_status eq 'requested') ? 0 : 1 # too many open requests ...
         );
    print __LINE__, " marc_item_status=[$marc_item_status] checking row: ", Dumper ($row) if ($ac_number eq 'AC06678536');

    if ($marc_item_status eq 'available')
    {

# NOTE/TODO: maybe we should refactor out this block as "marc_verify()" or something like that which can then be used to re-validate already validated items

      my ($ac_url, $ac_urn)= marc_extract_url_urn ($marc_data);

if (defined ($ac_urn))
{ # TODO: this is not expected yet ...
  print __LINE__, " row: ", Dumper($row);
  $die_nbn_already_defined++;
  # die "ac_urn=[$ac_urn] already defined!";
  print __LINE__, " ac_urn=[$ac_urn] already defined! skipped";
  return undef;
}

    if ($marc_item_epoch + $MAX_MARC_AGE < time ())
    {
      print "ATTN: marc entry for $ac_number is too old, ignored! (maybe it needs to be requested again)\n";
      print __LINE__, " ac_number=[$ac_number] marc_item_epoch=[", scalar localtime($marc_item_epoch),
            " ] MAX_MARC_AGE=[$MAX_MARC_AGE]\n";
      return undef;
    }

      # NOTE/TODO: xml_url and so on are proably not good choices as attribute names in the database, maybe ac_url and so on would be better...
      $upd{xml_ts}= $marc_item_epoch;
      $upd{xml_url}= $ac_url;
      $upd{xml_urn}= $ac_urn;

      # 1. check if the URL is valid
      # TODO: there must be a better way to retrieve possible URL variants ...
      my @ctx_urls= IRMA::eprints::get_eprint_urls($context_id);

      my $ctx_url_ok;
      URL_CHECK: foreach my $ctx_url (@ctx_urls)
      {
        if ($ac_url eq $ctx_url)
        {
          $ctx_url_ok= $ctx_url; # TODO: since this is a preference-ordered list, this info could be used for something ...
          print "matching ac_url=[$ac_url] ctx_url=[$ctx_url]\n";
          last URL_CHECK;
        }
      }

      if (!defined ($ctx_url_ok))
      {
        print __LINE__, " ATTN: url_mismatch!\n";
        print " ac_url=[$ac_url]\n";
        print "ctx_urls=[", join ('|', @ctx_urls),"]\n";

        $update= 'error';
        $upd{error_source}= 'link_validation';
        $upd{error_text}= "link in ac_record=[$ac_url]";
      }
      else
      {
        $update= 'verified';
      }

# END validation code (block mismatch!)

    }
    elsif ($marc_item_status eq 'requested')
    {
      print __LINE__, " requested new item: ac_number=[$ac_number]\n";
      $update= 'requested';
      $cnt_marc_requests++;
    }
    elsif ($marc_item_status eq 'unknown') {} # NOP!
    elsif ($marc_item_status eq 'error')
    {
      print __LINE__, "marc_item_status='error'\n";
      $update= 'error';
      $upd{error_source}= 'marc_fetch';
      $upd{error_text}= $marc_error_info;
    }
    else
    {
      $update= 'error';
      $upd{error_source}= 'marc_fetch';
      $upd{error_text}= "unknown marc_item_status=[$marc_item_status] for ac_number=[$ac_number]";

      print __LINE__, " ATTN: ", $upd{error_text}, "\n";
    }

DONE:
    if (defined ($update))
    {
      $upd{ac_status}= $update;
$irma_db->show_updates(1);
      $irma_db->update('ac_numbers', $id, \%upd);
    }

  ($update, \%upd);
}

=head2 check_requests ($marc, @ac_numbers);

check open requests

count how many open tasks are in the requests queues of our downstream agents

=cut

sub check_requests
{
  my $marc= shift;
  my $delay= shift;
  my $silent= shift;

  my $req_col= $marc->get_collection ('requests'); # TODO: parameter or config

  my @agents= qw(aleph_cat alma_cat irma-urn-othes-prod);

  my $open;
  while ($running)
  {
    $open= 0;

    foreach my $agent (@agents)
    {
      my $count_new= $req_col->find( { agent => $agent, status => 'new' } )->count();
      my $count_wip= $req_col->find( { agent => $agent, status => 'in_progress' } )->count();
      $open += $count_new + $count_wip;
      printf ("%5d %5d %5d %s\n", $open, $count_new, $count_wip, $agent) unless ($silent);
    }

    last unless (defined ($delay));
    last if ($silent && $open == 0);
    sleep ($delay);
  }

  $open;
}

=head1 EPRINTS APPLICATION DATABSE FUNCTIONS

=head2 get_eprints_db($cnf)

access eprints database from config data found in $cnf

This is the MySQL database where the eprints instance is storing it's
data.  Of interest there is mainly the table "eprint".  Do not confuse
that with table "eprints" in the IRMA Naming Authority database.

=cut

sub get_eprints_db
{
  my $cnf= shift;

  my ($driver, $dbcc)= IRMA::db::get_db_config ($cnf, 'eprints_database');

  my $epr= new IRMA::eprints
  (
    'driver' => $driver,
    'dbcc' => $dbcc,
    'table_filter' => \&IRMA::eprints::table_filter, # TODO: there should be a method to do that ...
    'do_desc_all' => 1, # eprints does not use column "id" as primary key as expected by Redmine::DB::MySQL
  );

  $epr;
}

sub print_error_report
{
  my $res= shift;

  my $errors= $res->{'errors'};
  my @errors= sort { $a <=> $b } keys %$errors;

  if (@errors == 0)
  {
    print "no errors found\n";
    return;
  }

  my $error_report_tsv= 'error_report.tsv'; # TODO: make it configurable
  open (FO, '>:utf8', $error_report_tsv);
  print scalar (@errors), " errors found, saved to $error_report_tsv\n";
  print FO join ("\t", qw(epr_id num errors)), "\n";
  foreach my $epr_id (@errors)
  {
    my $r= $errors->{$epr_id};
    print FO join ("\t", $epr_id, scalar (@$r), join (';', @$r)), "\n";
  }
  close (FO);
}

sub sync_irma_to_eprints_app
{
  my $irma= shift;
  my $epr= shift;

  my $irma_db= $irma->connect();
  my $query= 'item_status="new" and urn_status="registered" and urn is not null';
  my $res= $irma_db->get_all_x ('eprints', [$query]);

  my @res= keys %$res;
  print "query=[$query]\n";
  print "found ", scalar (@res), " items\n";
  # print "x: ", join (' ', @x), "\n";

  unless (@res)
  {
    print "nothing to do\n";
    return undef;
  }

  my $epr_db= $epr->connect();
  # $epr_db->verbose(1);

  my $cnt_u2e2= 0;

  EP_ROW: foreach my $id (@res)
  {
    last if ($cnt_u2e2 >= $MAX_URN_REQUESTS); # TODO: different limits?

    my $row_na= $res->{$id};
    my ($eprint_id)= map { $row_na->{$_} } qw(eprint_id);
    # eprint_id is the primary key in the other database, there called "eprintid"!

    print __LINE__, " id=[$id] eprint_id=[$eprint_id] row_na: ", main::Dumper ($row_na);

    $cnt_u2e2++;
    my $error= 0;

    my $rows_epr= $epr_db->get_all_x ('eprint', ['eprintid=?', $eprint_id], 'eprintid,urn,eprint_status');
    # NOTE: we need to fetch eprintid since it is the primary key!

    my $upd_ep= 1;
    my $row_epr;
    if (!defined ($rows_epr)
        || !defined ($row_epr= $rows_epr->{$eprint_id})
       )
    {
      print "ATTN: no matching row found ein eprints database, table eprint\n";
      $error++;
    }
    else
    {
      print __LINE__, " row_epr: ", main::Dumper ($row_epr);

      # TODO: check if the eprint record is still valid
      if (defined ($row_epr->{urn}))
      {
        if ($row_epr->{urn} eq $row_na->{urn})
        {
          # NOTE: this is done twice at this, so the complaint below is understandable...
          $upd_ep= 0;
        }
        else
        {
          print __LINE__, " ATTN: urn changed in eprints database, table eprint\n";
          $error++;
        }
      }

      unless ($row_epr->{eprint_status} eq $row_na->{eprint_status})
      {
        print __LINE__, " ATTN: eprint_status changed in eprints database, table eprint\n";
        $error++;
      }
    }

    if ($error)
    {
      print __LINE__, " ERROR: row_na: ", main::Dumper ($row_na);
      print __LINE__, " ERROR: row_epr: ", main::Dumper ($row_epr);
    }
    else
    {
      if ($upd_ep)
      {
        my %upd_ep=
        (
          'urn' => $row_na->{urn},
        );

        # fixup_eprint_item (\%upd_ep);
        my $res_ep= $epr_db->update('eprint', $eprint_id, \%upd_ep);
        print "upd_ep (eprintid=$eprint_id) res_ep=[$res_ep]: ", Dumper(\%upd_ep);

        # TODO: if something goes wrong, flag it out or whatever
      }

      my %upd_na=
      (
        'item_status' => 'urn_registered',
      );

      my $res_na= $irma_db->update('eprints', $id, \%upd_na);
      print "upd_na (id=$id) res_ns=[$res_na]: ", Dumper(\%upd_na);
    }

    last unless ($running);
  }
}

=head1 IRMA::NA DATABASE FUNCTIONS

=head2 get_irma_na_db($cnf)

access IRMA::NA (naming authority) database from config data found in $cnf

This is the MySQL database where IRMA naming authority is storing it's
data.  Of interest there is mainly the tables related to the connected
applications:

  * table "ac_numbers": contains a reference of all catalog numbers
                        and the context they appeared in


Context specific tables:

  * table "eprints": contains references to the application where the
                     identifiers are used and reference via the catalog
                     numbers (ac_number).

    Do not confuse that with table "eprint" in the eprints application!

TODO: other contexts such as EoD, Goobi

=cut

sub get_irma_na_db
{
  my $cnf= shift;

  my ($driver2, $dbcc2)= IRMA::db::get_db_config ($cnf, 'irma_database');
  # print "dbcc2: ", main::Dumper($dbcc2);
  my $irma_na= new IRMA::NA ('driver' => $driver2, 'dbcc' => $dbcc2);

  $irma_na;
}

=head1 EPRINTS/OTHES FUNCTIONS

=head2 eprint_full_sync ($irma_na_db, $epr_db)

Transfer data between the MySQL database of the EPrint application and
the MySQL database mirroring the relevant data in the IRMA database.

=cut

sub eprint_full_sync
{
  my $irma_na_db= shift;
  my $epr_db= shift;

  # my $irma_na_data= $irma_na_db->fetch_eprints_data();

  print __LINE__, " NOTE: eprint_full_sync started: ", ts_ISO(), "\n";

  eprint_merge ($irma_na_db, $epr_db);

  my @res= sync_irma_to_eprints_app ($irma_na_db, $epr_db);

  print __LINE__, " NOTE: eprint_full_sync finished: ", ts_ISO(), "\n";
}

=head2 eprint_merge ($irma, $d_na, $d_epr)

merge data from the eprint table ($d_epr) into the naming authority table ($d_na)

  $irma: handle to IRMA::NA Database
  $d_na: data from irma_na_db
  $d_epr: data from eprints_db

... specific for othes

=cut

sub eprint_merge
{
  my ($irma, $epr)= @_;

  my $epr_data= $epr->fetch_eprint_app_data();
  print_error_report ($epr_data);

  # print "irma: ", Dumper($irma);
  # $irma->update_from_eprints($epr_res);

  # print "d_na: ", Dumper ($d_na);
  # my %na_epr_id= map { $d_na->{$_}->{eprint_id} => [ $d_na->{$_}->{id}, 1 ] } keys %$d_na;

  my $irma_db= $irma->connect();
  # $irma_db->verbose(1);
  $irma_db->show_updates(1);

  my $epr_db= $epr->connect();
  $epr_db->show_updates(1);

  my $ok= $epr_data->{ok};
  # print __LINE__, " epr_data: ok hash: ", Dumper ($ok);

  my @ok= sort keys %$ok;
  # print __LINE__, " epr_data: ok list: ", Dumper (\@ok);
  # TODO: check if @ok contains data...
  my @added= ();
  my $cnt_synced= 0;
  print __LINE__, " MAX_SYNC=$MAX_SYNC\n";
  foreach my $ac_number (@ok)
  {
    # print __LINE__, " ac_number=[$ac_number]\n";

    last if (defined ($MAX_SYNC) && $cnt_synced >= $MAX_SYNC);

    my $row_app= $ok->{$ac_number};

    my $res= eprint_check_row ($irma_db, $epr_db, $ac_number, $row_app);

    $cnt_synced++;

    last unless ($running);
  }

  # TODO: identify objects that are no longer present in the EPrint application's database
}

=head2 fetch_eprints_row_ref($irma_db, $eprint_id)

=cut

sub fetch_eprints_row_ref
{
  my $irma_db= shift;
  my $eprint_id= shift;

  my $res= $irma_db->get_all_x ('eprints', ['eprint_id=?', $eprint_id]);
  return undef unless (defined ($res));

  my @ids= keys %$res;
  if (@ids > 1)
  {
    print __LINE__, " ATTN: duplicated eprint_id=[$eprint_id]: res: ", Dumper ($res);
    # TODO: error reporting ...
  }

  $res->{$ids[0]};
}

=head2 eprint_check_row($irma_db, $ac_number, $row_app)

=head3 return values

Negative values represent error codes, positive values are a bitfield
describing where changes were made.

 -1 .. error
  0 .. unchanged
  1 .. updated app data
  2 .. updated ref data
  4 .. added ref data

=cut

sub eprint_check_row
{
  my $irma_db= shift;
  my $epr_db= shift;
  my $ac_number= shift;
  my $row_app= shift;   # from EPrints applicaton

  # print __LINE__, " row_app: ", Dumper ($row_app);

  my $eprint_id= $row_app->{eprintid};
  my $eprint_url= "https://othes.univie.ac.at/$eprint_id/"; # NOTE: specific for othes

  my ($ac_check, $ac_record)= IRMA::ac_number::check_ac_number ($irma_db, $ac_number, 'eprints', $eprint_id, $eprint_url);
  if ($ac_check < 0)
  {
    if ($ac_check == -2)
    {
      print "ATTN/TODO: report context violation for ac_number=[$ac_number], eprint_id=[$eprint_id]\n";
    }
    else
    {
      print "ATTN: other error\n";
    }
    return -1;
  }
  # TODO: if ac_number was new, record the ac_number or at least a counter somewhere for a subsequend MAB update run

  my $row_ref= fetch_eprints_row_ref($irma_db, $eprint_id); # from IRMA, eprints mirror
  # print __LINE__, " row_ref=", main::Dumper ($row_ref);

  my $rc= 0;
  unless (defined ($row_ref))
  {
    print __LINE__, "\n";
    my $rec= copy_eprint_item ($row_app);

    my $id= $irma_db->insert ('eprints', $rec);

    print "inserted eprints; id=[$id]\n";

    $row_ref= fetch_eprints_row_ref($irma_db, $eprint_id); # re-fetch for check and possible updates below!

    unless (defined ($row_ref))
    {
      print __LINE__, " could not insert row into eprints: ", Dumper($rec);
      return -1;
    }

    $rc |= 4;
  }

  my ($note, $upd1, $upd2);
  ($rc, $note, $upd1, $upd2)= check_eprint_item ($row_app, $row_ref, $ac_record);
  print __LINE__, " rc=[$rc] note=[$note]\n" unless ($rc == 0);

  if ($rc < 0)
  {
    print "ERROR rc=[$rc]\n";
  }
  else # if $rc == 0: NOP
  {
    if ($rc & 1)
    {
      # fixup_eprint_item ($upd1);
      $epr_db->update ('eprint', [$eprint_id], $upd1);
    }

    if ($rc & 2)
    {
      $irma_db->update ('eprints', $row_ref->{id}, $upd2);
    }
  }

  # TODO: return info if added or
  $rc;
}

=head2 check_eprint_item ($epr_app_row, $epr_na_row, $ac_record)

Return values: ($code, $note, $upd1, $upd2);

 $code:
  0: nothing to do
  1: updated application data
  2: updated reference data
  3: updated both
 -1: error of some kind; # TODO: add some reporting

=cut

sub check_eprint_item
{
  my $row_app= shift;
  my $row_ref= shift;
  my $ac_record= shift;

  # print __LINE__, " row_app: ", Dumper ($row_app);
  # print __LINE__, " row_ref: ", Dumper ($row_ref);
  # print __LINE__, " ac_record: ", Dumper ($ac_record);

  my ($urn1, $eps1)= map { $row_app->{$_} } qw(urn eprint_status);
  my ($urn2, $urn2_st, $eps2, $item_status)= map { $row_ref->{$_} } qw(urn urn_status eprint_status item_status);

  my ($ac_status, $ac_urn_st, $ac_urn_val)= map { $ac_record->{$_} } qw(ac_status urn_status urn_value);

  my $note;
  if ($eps1 ne $eps2)
  {
    print __LINE__, " ATTN/TODO: ", $note= "eprint_status changed: now=[$eps1], was=[$eps2]", "\n";
    # TODO: find out which changes are allowed
    return (-1, $note);
  }

  my $ret= 0;
  my (%upd1, %upd2);

  if ($ac_status eq 'error')
  {
    if ($item_status ne 'ac_error')
    {
      $upd2{item_status}= 'ac_error';
      $ret |= 2;
    }
  }
  elsif (!defined ($urn1) && !defined ($urn2))
  {
    # TODO: urn request job
    if ($ac_urn_st eq 'registered' && defined ($ac_urn_val))
    {
      $upd2{urn_status}= 'registered';
      $upd2{urn}= $upd1{urn}= $ac_urn_val;
      $ret |= 3;
    }
    elsif (($ac_status eq 'verified' || $ac_status eq 'done') && !defined ($urn2_st))
    {
      $upd2{urn_status}= 'torequest';
      $ret |= 2;
    }
  }
  elsif (!defined ($urn1) && defined ($urn2))
  {
    $upd1{urn}= $urn2;
    $note= "sync-back urn=[$urn2]";
    $ret |= 1;
  }
  elsif (defined ($urn1) && !defined ($urn2))
  {
    # TODO: urn inserted on the reference side!
    print __LINE__, " NOTE: ", $note= "urn=[$urn1] inserted on app side, needs verification", "\n";
    $upd2{urn}= $urn1;
    $upd2{urn_status}= 'upstream';
    $note= "verify urn=[$urn1]";
    $ret |= 2;
  }
  elsif ($urn1 ne $urn2)
  {
    print __LINE__, " row_app: ", Dumper ($row_app);
    print __LINE__, " row_ref: ", Dumper ($row_ref);
    print __LINE__, " ac_record: ", Dumper ($ac_record);
    print __LINE__, " ATTN/TODO: ", $note= "urn changed: now=[$urn1], was=[$urn2]", "\n";
    # TODO: this should not be allowed!
    return (-1, $note);
  }
  # elsif ($urn eq $urn2) {} nop; that's ok;

  ($ret, $note, \%upd1, \%upd2);
}

# NOTE: this is not necessarily needed
sub fixup_eprint_item
{
  my $row= shift;
  print __LINE__, " fixup_eprint_item: row=", main::Dumper($row);
  my @t= localtime (time ());

  $row->{lastmod_year}=   $t[5]+1900;
  $row->{lastmod_month}=  $t[4]+1;
  $row->{lastmod_day}=    $t[3];
  $row->{lastmod_hour}=   $t[2];
  $row->{lastmod_minute}= $t[1];
  $row->{lastmod_second}= $t[0];
}

sub copy_eprint_item
{
  my $row= shift;

print __LINE__, " row: ", main::Dumper ($row);

    my $rec=
    {
      eprint_id     => $row->{eprintid},
      eprint_status => $row->{eprint_status},
      eprint_type   => $row->{type},
      eprint_sperre => $row->{sperre},
      eprint_rev_number      => $row->{rev_number},
      eprint_einverstaendnis => $row->{einverstaendnis},
      matr          => $row->{matr},      # TODO: do we need this here? Note: I guess so, because we check it and store the result in matr_status
      urn           => $row->{urn},       # this is the urn-value that eprint stores
      urn_status    => (defined ($row->{urn})) ? 'upstream' : undef,
      ac_number     => $row->{ac_nummer}, # column is named "ac_nummer" in othes/eprints
      item_status   => 'new'
    };

  $rec;
}

=head2 urn_to_eprints ($irma_na)

  irma_na: IRMA Naming Authority Database (MySQL)

Specifically for eprints: transcribe newly generated urns from the ac_numbers table to the eprints table.

This uses a view which should be prepared as follows:

  --- 8< ---
  CREATE OR REPLACE VIEW eprints_acnumbers (id, ep_urn, acnumbers_id, ac_number, ac_urn) AS
   SELECT ep.id, ep.urn, ac.id, ac.ac_number, ac.urn_value
     FROM eprints ep, ac_numbers ac
    WHERE ac.ac_number=ep.ac_number AND context="eprints";
  --- >8 ---

Query:

  --- 8< ---
  SELECT * FROM eprints_acnumbers WHERE ep_urn is null AND ac_urn is not null LIMIT 10;
  --- >8 ---

=cut

sub urn_to_eprints
{
  my $irma_na= shift;

  my $irma_db= $irma_na->connect();
  # $irma_db->verbose(1);
  my $query= ['ep_urn is null and ac_urn is not null'];

  my $x= $irma_db->get_all_x ('eprints_acnumbers', $query);

  my @x= keys %$x;
  # print "x: ", join (' ', @x), "\n";
  my @updated_ids= ();
  my $cnt_u2e= 0;
  foreach my $id (@x)
  {
    last if ($cnt_u2e >= $MAX_URN_REQUESTS); # TODO: different limits?

    my $row= $x->{$id};
    my $urn= $row->{ac_urn};
    print "id=[$id] urn=[$urn]\n";
    # print " row: ", main::Dumper ($row);

    my %upd=
    (
      'urn_status' => 'registered',
      'urn' => $row->{ac_urn},
      # TODO: should we see item_status here too?
    );

    # print "upd: ", Dumper (\%upd);
    $irma_db->update ('eprints', $row->{id}, \%upd);

    push (@updated_ids, $row->{id});
    $cnt_u2e++;

    last unless ($running);
  }

  ($cnt_u2e, \@updated_ids);
}

=head2 request_urn_registration ($irma_na, $irma_jq, $context)

  irma_na: IRMA Naming Authority Database (MySQL)
  irma_jq: IRMA Job Queue (MongoDB)
  context: name of the context (e.g. "eprints")

KW: nbn4ep

=cut

sub request_urn_registration
{
  my $irma_na= shift;
  my $irma_jq= shift;
  my $context= shift;

  my $irma_db= $irma_na->connect();
  # $irma_db->verbose(1);
  my $query= ['context=? and ac_status="verified" and xml_url is not null and xml_urn is null and urn_status is null', $context];

  my $x= $irma_db->get_all_x ('ac_numbers', $query);
  # print __LINE__, " request_urn_registration x: ", main::Dumper($x);

  my $cnt_urn_req_jobs= 0;

  my @x= keys %$x;
  # print "x: ", join (' ', @x), "\n";
  my @updated_ids= ();
  foreach my $id (@x)
  {
    last if ($cnt_urn_req_jobs >= $MAX_URN_REQUESTS);

    my $row= $x->{$id};
    # print "id=[$id] row: ", main::Dumper ($row);
    my $req_data=
    {
      'ac_number' => $row->{ac_number}, # that's for us
      'resources' => # stuff that the Metaresolver wants
      [
        {
          frontpage => 1,
          primary => 1,
          origin => 1,
          url => $row->{context_url},
        }
      ],
    };

    print "req_data: ", main::Dumper ($req_data);
    my $res= $irma_jq->insert_job ($req_data);
    print "res: ", Dumper ($res);
    my %upd=
    (
      'urn_status' => 'requested',
      'urn_req_id' => $res->{value}, # TODO: depends on MongoDB version
    );
    print "upd: ", Dumper (\%upd);
    $irma_db->update ('ac_numbers', $row->{id}, \%upd);

    push (@updated_ids, $row->{id});
    $cnt_urn_req_jobs++;

    # just for debug... sleep($sleep_urn_request);

    last unless ($running);
  }

  ($cnt_urn_req_jobs, \@updated_ids);
}

=head2 check_urn_registration ($irma_na, $irma_jq, $context)

check if the urn registration was completed by now

  irma_na: IRMA Naming Authority Database (MySQL)
  irma_jq: IRMA Job Queue (MongoDB)
  context: name of the context (e.g. "eprints")

  * only applicable in the context "eprints" (nbn4ep)??? not necessarily ...  Let's keep it parametrized for now

=cut

sub check_urn_registration
{
  my $irma_na= shift;
  my $irma_jq= shift;
  my $context= shift;

print __LINE__, " check_urn_registration: context=[$context]\n";

  my $irma_db= $irma_na->connect();
  # $irma_db->verbose(1);
  my $query= ['context=? and urn_status="requested"', $context];
  # NOTE: this is redundant here: and ac_status="verified"

  my $x= $irma_db->get_all_x ('ac_numbers', $query);
  # print __LINE__, " check_urn_registration x: ", main::Dumper($x);

  my @x= keys %$x;
  my $cnt_urn_check_jobs= 0;

  print __LINE__, " items to check: ", scalar (@x), "\n";
  my @updated_ids= ();
  if (@x)
  {
    # print "x: ", join (' ', @x), "\n";
    # print "irma_jq: ", main::Dumper ($irma_jq);
    # my $jq_col= $irma_jq->{_jq}->connect();
    # print "jq_col: ", main::Dumper ($jq_col);

    foreach my $id (@x)
    {
      last if ($cnt_urn_check_jobs >= $MAX_URN_REQUESTS); # TODO: different limits?

      my $row= $x->{$id};
      print __LINE__, " id=[$id] row: ", main::Dumper ($row);

      my $req_id= $row->{urn_req_id};
      my $job= $irma_jq->get_job_by_id ($req_id);

      if (defined ($job)
          && $job->{_id}->{value} == $req_id # TODO: depends on MongoDB version
         )
      {
        my $urn_value= $job->{urn_data}->{urn};

        if (defined ($urn_value)) # otherwise the job is still pending
        {
          # print "job: req_id=[$req_id] urn_value=[$urn_value] ", main::Dumper ($job);
          print __LINE__, " job: req_id=[$req_id] urn_value=[$urn_value]\n";

          my %upd=
          (
            'urn_status' => 'registered',
            'urn_value'  => $urn_value,
          );
          # print "upd: ", Dumper (\%upd);
          $irma_db->update ('ac_numbers', $row->{id}, \%upd);

          $cnt_urn_check_jobs++;
          push (@updated_ids, $row->{id});
        }
      }

      # ATTN/BUG/FEATURE?: at least one item gets processed

      # sleep($sleep_urn_request); # TODO: do we need a sleep timer here?
      last unless ($running);
    }
  }

  ($cnt_urn_check_jobs, \@updated_ids);
}

=head2 verify1_urn_registration

If the upstream context already has an urn registration, we need to
verify it and change the urn_status from "upstream" to "registered" or
"invalid", depending on the outcome of the verification.  To do this,
in the "verify1"-step, a "verify" job is sent to the urn registration
agent and the eprints-record is marked as "pending-verification" ?

=cut

sub verify1_urn_registration1
{
  my $irma_na= shift;
  my $irma_jq= shift;
  my $context= shift; # this only applies to eprints!

print __LINE__, " verify1_urn_registration: context=[$context]\n";

  my $irma_db= $irma_na->connect();
  $irma_db->show_updates(1);
  my $x= $irma_db->get_all_x ('eprints', ['urn_status="upstream"']);
  # print __LINE__, " verify_urn_registration x: ", main::Dumper($x);

  my @x= keys %$x;
  my $cnt_urn_check_jobs= 0;

  print "items to verify: ", scalar (@x), "\n";
  my @updated_ids= ();
  while (my $id= shift (@x))
  {
    last if (defined ($MAX_SYNC) && $cnt_urn_check_jobs >= $MAX_SYNC);

    my $row= $x->{$id};
    print "row id=[$id] ", Dumper ($row);
    my ($ac_number, $ep_urn, $eprint_id)= map { $row->{$_} } qw(ac_number urn eprint_id);

    my $eprint_url= "https://othes.univie.ac.at/$eprint_id/"; # NOTE: specific for othes

    my ($ac_check, $ac_record)= IRMA::ac_number::check_ac_number ($irma_db, $ac_number, 'eprints', $eprint_id, $eprint_url);

    print "ac_record ac_number=[$ac_number] ac_check=[$ac_check]: ", Dumper ($ac_record);

    if (defined ($ac_record))
    {
      my ($ac_urn_st, $ac_urn)= map { $ac_record->{$_} } qw(urn_status urn_value);

      my %upd_ep;
      my $upd= 0;

      if ($ac_urn_st eq 'registered' && $ep_urn eq $ac_urn)
      { # that's fine, matching records, just mark as done.
        $upd_ep{urn_status}= 'registered';
        $upd |= 1;
      }
      elsif ($ac_urn_st eq 'registered' && $ep_urn ne $ac_urn)
      { # that's bad, different values
        $upd_ep{urn_status}= 'invalid';
        $upd |= 1;
      }
      elsif ($ac_urn_st eq 'requested')
      { # something is in progress, wait for the result
      }
      else
      {
        print __LINE__, " ATTN: ac_record.urn_status=[$ac_urn_st] unknown\n";
      }

      if ($upd & 1)
      {
        $irma_db->update ('eprints', $row->{id}, \%upd_ep);
      }
    }
    $cnt_urn_check_jobs++;
  }
}

=head2 export_csv

export data from irma db

=cut

sub export_csv
{
  my $irma_na= shift;
  my $context= shift; # this currently only applies to eprints!
  my $fnm_out= shift || sprintf ("export-%d.tsv", time());
  my $separator= shift || "\t";

print __LINE__, " export_csv: context=[$context] fnm_out=[$fnm_out]\n";

  if (-f $fnm_out)
  {
    print "ERROR: export file [$fnm_out] already exists!\n";
    return undef;
  }

  my @cols= qw(context ac_number ac_status urn_status urn_value xml_url context_url);
  unless (open (FO, '>:utf8', $fnm_out))
  {
    print "can not write to export file [$fnm_out]\n";
    return undef;
  }
  print FO join ($separator, @cols), "\n";

  my $irma_db= $irma_na->connect();
  $irma_db->show_updates(1);
  my $x= $irma_db->get_all_x ('ac_numbers', ['context=? and error_source is NULL and xml_urn is NULL and urn_status="registered"', $context]);
  # print __LINE__, " verify_urn_registration x: ", main::Dumper($x);

  my @x= keys %$x;
  my $cnt_urn_check_jobs= 0;

  print "items to export: ", scalar (@x), "\n";

  my $cnt= 0;
  while (my $id= shift (@x))
  {
    my $row= $x->{$id};
    # print "row id=[$id] ", Dumper ($row);
    print FO join ($separator, map { $row->{$_} } @cols), "\n";
    $cnt++;
  }
  close (FO);

  print "written $cnt rows to $fnm_out\n";

  $cnt;
}

sub reset_ac_errors
{
  my $irma_na= shift;
  my @ac_numbers= @_;

  my $irma_db= $irma_na->connect();

  foreach my $ac_number (@ac_numbers)
  {
    IRMA::ac_number::reset_ac_error ($irma_db, $ac_number);
  }
}

sub get_ac_errors
{
  my $irma_na= shift;
  my @ac_numbers= @_;

  my $irma_db= $irma_na->connect();

  IRMA::ac_number::get_ac_errors ($irma_db);
}

sub doi4ep
{
  print "starting doi4ep\n";
  # my $irma_na= get_irma_na_db($cnf);
  my $epr= get_eprints_db($cnf);
  my $epr_db= $epr->connect();
  # $epr_db->desc('eprint_creators_name'); # to identify the primary key

  my $epr_data= $epr->fetch_metadata();
  print __LINE__, " epr_data=[$epr_data]\n";
  $epr_db->show_query(0);

  my @tsv_data;
  my $cnt_synced= 0;
  print __LINE__, " MAX_SYNC=$MAX_SYNC\n";
  ITEM: foreach my $k (keys %$epr_data)
  {
    last if (defined ($MAX_SYNC) && $cnt_synced >= $MAX_SYNC);

    my $row= $epr_data->{$k};

    my ($tsv_row, $datacite_xml_path)= doigen($epr, $row);
    push (@tsv_data, $tsv_row) if (defined ($tsv_row));

    $cnt_synced++;
  }

  my $tsv_fnm= 'othes/identifiers_' . Util::ts::ts_ISO_gmt() . '.tsv';
  my @tsv_columns= qw( id na_id identifier context_id context_pid canonical_url ticket ts_md_fetch ts_dcd_registration );
  unless (open (TSV, '>:utf8', $tsv_fnm))
  {
    die "can't write to [$tsv_fnm]";
  }
  print __LINE__, " saving registration data to [$tsv_fnm]\n";
  print TSV join("\t", @tsv_columns), "\n";
  foreach my $tsv_row (@tsv_data)
  {
    print TSV join("\t", map { $tsv_row->{$_} } @tsv_columns), "\n";
  }
  close (TSV);

  # eprint_full_sync ($irma_na, $epr);
  print "finished doi4ep: tsv_fnm=[$tsv_fnm] cnt_synced=[$cnt_synced]\n";

  return ($tsv_fnm, $cnt_synced);
}

=head2 doigen($epr, $row)

=cut

sub get_othes_timestamp
{
  my $row= shift;
  my $name= shift;

  my @ts;
  foreach my $el (qw(year month day))
  {
    my $x= $row->{$name . '_' . $el};
    unless (defined ($x))
    {
      return undef if ($el eq 'year');
      $x= 1; # undef month or day becomes 1
    }
    push (@ts, $x);
  }

  foreach my $el (qw(hour minute second))
  {
    my $f= join('_', $name, $el);
    push (@ts, (exists ($row->{$f})) ? $row->{$f} : 0);
  }

  sprintf ("%4d-%02d-%02dT%02d:%02d:%02dZ", @ts);
}

sub doigen
{
  my $epr= shift;
  my $row= shift;

  # next unless (defined ($row->{urn}));
  # print __LINE__, " ac_number=[$ac_number] ", main::Dumper ($row);

  my ($eprintid)= map { $row->{$_} } qw(eprintid);

  my $doi= '10.25365/thesis.'. $eprintid; # TODO: retrieve prefix from config
  my $canonical_url= sprintf ("https://othes.univie.ac.at/%s/", $eprintid);
  my $datacite_xml_path= 'othes/DataCite_XML/' . $doi . '.xml';

  # my $lastmod= sprintf("%4d-%02d-%02dT%02d%02d%02d", map { $row->{$_} } qw(lastmod_year lastmod_month lastmod_day lastmod_hour lastmod_minute lastmod_second));
  my $lastmod= get_othes_timestamp($row, 'lastmod');

# TODO: utheses and datacite metadata should be considered independently
  if (-f $datacite_xml_path)
  {
    my @st= stat(_);
    my $mtime_ts= Util::ts::ts_ISO_gmt($st[9]);

    if ($mtime_ts gt $lastmod)
    {
      print __LINE__, " doi=[$doi] already processed datacite_xml_path=[$datacite_xml_path]; skipping\n";
      return undef;
    }
  }

  print __LINE__, " row: ", main::Dumper ($row);

  my $tsv_row=
  {
      id => '',
      na_id => 1,
      identifier => $doi,
      context_id => 2,
      context_pid => $eprintid,
      canonical_url => $canonical_url,
      ticket => '',
      ts_md_fetch => $lastmod . 'Z',
      ts_dcd_registration => Util::ts::ts_ISO_gmt() . 'Z',
  };

# BEGIN check language

    my ($lang_pdf, $files)= analyze_files(map { $row->{$_} } qw(eprintid fileinfo dir));

    my $language= Phaidra::Utils::iso639::iso_639_2_to_1($row->{sprache});
    my ($abstract, $abstract_eng)= map { strip_text($row->{$_}) } qw(abstract abstract_eng);

    print __LINE__, " eprintid=[$eprintid] language=[$language] lang_pdf=[$lang_pdf]\n";
    if ($language ne $lang_pdf)
    {
      print __LINE__, " ATTN: eprintid=[$eprintid] language=[$language] lang_pdf=[$lang_pdf]\n";
    }

    my $description_xml= add_description($abstract_eng) . add_description($abstract);
    print __LINE__, " description_xml=[$description_xml]\n";

# END check language

    my ($creators_xml)= $epr->get_creators($eprintid);
    my $title_xml= strip_text($row->{title});

    my $xml= <<"EOX";
<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">$doi</identifier>
  <creators>
$creators_xml  </creators>
  <titles>
    <title>$title_xml</title>
  </titles>
  <publisher>(:none)</publisher>
  <publicationYear>$row->{date_year}</publicationYear>
  <language>$language</language>
  <resourceType resourceTypeGeneral="Text">Thesis</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="nbn">$row->{urn}</alternateIdentifier>
  </alternateIdentifiers>
  <descriptions>
$description_xml  </descriptions>
</resource>
EOX

    unless (open (XML, '>:utf8', $datacite_xml_path))
    {
      # print __LINE__, " ATTN: can't write to [$datacite_xml_path]\n";
      die ("ATTN: can't write to [$datacite_xml_path]");
    }
    # print __LINE__, " xml=[$xml]\n";
    print __LINE__, " writing xml for doi=[$doi] to [$datacite_xml_path]\n";
    print XML $xml;
    close (XML);

# END DOIGEN

  ($tsv_row, $datacite_xml_path);
}

=head2 oma()

Othes Migration Agent: listen to requests in MongoDB and perform them

=cut

sub oma
{
  $db_ot2ut= IRMA::db::get_any_db($cnf, 'ot2ut_database') unless (defined ($db_ot2ut));

# my $col_sync= $db_ot2ut->get_collection('sync');
  my $col_req= $db_ot2ut->get_collection('requests');

  activity({ activity => 'listening', msg => ": context $ot2ut_context" });

  # send_message("oma is listening...");
  REQ: while($running && $serving_requests)
  {
    my $row= $col_req->find_one({ agent => 'oma', status => 'new' });
    unless (defined ($row))
    {
      print __LINE__, " oma sleeping until ", scalar localtime(time()+$oma_sleep_time), "\n";
      sleep ($oma_sleep_time);
      activity({ activity => 'sleeping', msg => ": context $ot2ut_context" }) if ($last_activity + $activity_period <= time());
      next REQ;
    }

    print __LINE__, " oma: row: ", Dumper($row);

    my $new_status= 'failed';
    my $next_action;
    if ($row->{action} eq 'send_batch')
    { # allow batch sizes up to 1000; be silent for batch sizes bigger than 10 items
      my $bs= $row->{batch_size};
      $bs= 10 unless ($bs > 0 && $bs <= 1000);
      $MAX_SYNC= $bs;
      $silent_upload_success= ($bs > 10) ? 1 : 0;

      my $eprint_status= $row->{eprint_status};

      if ($eprint_status eq 'buffer')
           { $ot2ut_eprint_status= 'buffer';  $ignore_errors= 1; }
      else { $ot2ut_eprint_status= 'archive'; $ignore_errors= 0; }

      $col_req->update( { _id => $row->{_id} }, { '$set' => { status => 'finish', ts_start => Util::ts::ts_ISO_gmt() }} );

      my $msg= "send_batch: sending $bs objects in $ot2ut_eprint_status to $ot2ut_context";
      activity({ activity => 'send_batch', msg => ': '. $msg});
      send_message($msg);

      my ($synced, $res)= ot2ut();
      send_message("send_batch: $res");

      $new_status= 'done' if (@$synced);
    }
    elsif ($row->{action} eq 'send_block')
    {
      my $block= $row->{block};
      $silent_upload_success= 1;
      $ignore_errors= 0;

      $col_req->update({ _id => $row->{_id}}, { '$set' => { status => 'in_progress', ts_start => Util::ts::ts_ISO_gmt() }});
      my $msg= "send_block: sending objects of block $block to $ot2ut_context";
      activity({ activity => 'send_block', msg => ': '. $msg});
      send_message($msg);

      my ($synced, $res)= ot2ut('block' => $block);
      send_message("send_block: block $block, result: $res");

      $new_status= 'done' if (@$synced);
      # policies_stats("processed block $block in context $ot2ut_context");
    }
    elsif ($row->{action} eq 'policies_stats')
    {
      $col_req->update({ _id => $row->{_id}}, { '$set' => { status => 'in_progress', ts_start => Util::ts::ts_ISO_gmt() }});
      policies_stats();
      $new_status= 'done';
    }
    elsif ($row->{action} eq 'send_ids')
    {
      $ignore_errors= 0;
      $col_req->update({ _id => $row->{_id}}, { '$set' => { status => 'in_progress', ts_start => Util::ts::ts_ISO_gmt() }});
      my @ids;
      foreach my $id (@{$row->{ids}}) { push (@ids, $id) if ($id =~ m#^\d+$#); }
      my $cnt= @ids;

      my $msg= "send_ids: sending $cnt objects to $ot2ut_context";
      activity({ activity => 'send_batch', msg => ': '. $msg});
      send_message($msg);
      $silent_upload_success= 0;

      my ($synced, $res)= ot2ut(@ids);
      send_message("send_ids: $res");
      $new_status= 'done' if (@$synced);
    }
    elsif ($row->{action} eq 'reload')
    {
      activity({ activity => 'reloading' });
      send_message('reloading');
      $next_action= 'reload';
      $new_status= 'done';
    }
    elsif ($row->{action} eq 'stop')
    {
      activity({ activity => 'stopping' });
      send_message('exiting');
      $next_action= 'exit';
      $new_status= 'done';
    }

    # update job
    $col_req->update({ _id => $row->{_id}}, { '$set' => { status => $new_status, ts_finish => Util::ts::ts_ISO_gmt() }});

    if (defined ($next_action))
    {
      if ($next_action eq 'reload') { exec($0); }
      elsif ($next_action eq 'exit') { exit(0); }
      $next_action= undef;
    }

    activity({ activity => 'listening', msg => ": context $ot2ut_context" });
  }
}

sub send_message
{
  my $text= shift or return undef;

  $col_msg= $db_ot2ut->get_collection('messages') unless (defined ($col_msg));
  return undef unless (defined ($col_msg));

  print __LINE__, ' ', scalar localtime(time()), " sending message [$text]\n";
  my $msg=
  {
    message => $text,
    priority => 'normal',
    state => 'new',
    to => 'oma'
  };
  $col_msg->insert($msg);
}

sub activity
{
  my $data= shift;

  unless (defined ($col_activity))
  {
    $db_ot2ut= IRMA::db::get_any_db($cnf, 'ot2ut_database') unless (defined ($db_ot2ut));

    $col_activity= $db_ot2ut->get_collection('activity');
    print __LINE__, " starting activity\n";
  }
  return undef unless (defined ($col_activity));

  $data->{agent}= 'oma'                   unless (exists($data->{agent}));
  $data->{e}= time()                      unless (exists($data->{e}));
  $data->{ts_gmt}= Util::ts::ts_ISO_gmt() unless (exists($data->{ts_gmt}));
  $data->{ts}=     Util::ts::ts()         unless (exists($data->{ts}));

  $col_activity->update( { agent => 'oma' },  $data, { upsert => 1 } );
  $last_activity= time();
}

=head2 ot2ut (pars)

  ot2ut() ...
  ot2ut('block' => $number) ... send objects in othes block $number
  ot2ut(@eprint_ids) ... send othes objects with these eprint ids

=cut

sub ot2ut
{
  my @eprint_ids= @_;

  print "starting ot2ut\n";
  # my $irma_na= get_irma_na_db($cnf);
  my $epr= get_eprints_db($cnf);

  my $upload_cnf= $cnf->{$ot2ut_context};
  die "no valid ot2ut context" unless (defined ($upload_cnf));

  my @extra_curl_parameters;
  if (exists ($upload_cnf->{headers}))
  {
    @extra_curl_parameters= map { ('--header', $_) } @{$upload_cnf->{headers}};
    # push (@upload_cmd, @extra_curl_parameters) if (@extra_curl_parameters);
  }

  $db_ot2ut= IRMA::db::get_any_db($cnf, 'ot2ut_database') unless (defined ($db_ot2ut));
  my $col_sync=           $db_ot2ut->get_collection('sync');
  my $col_att=            $db_ot2ut->get_collection('attachments');
  my $col_policy_utheses= $db_ot2ut->get_collection('policy.utheses');
  my $col_blk=            $db_ot2ut->get_collection('blocks');

  my $redis_connection= get_redis_db($upload_cnf);;

  # find items to upload
  if (!@eprint_ids)
  {
    print __LINE__, " fetching data\n";
    my $res1;

    if ($ot2ut_eprint_status eq 'archive')
    {
      $res1= $epr->fetch_data('archive', { doi => 0 });
    }
    elsif ($ot2ut_eprint_status eq 'buffer')
    {
      $res1= $epr->fetch_data('buffer');
    }

    if (defined ($res1))
    {
      push (@eprint_ids, keys %$res1);
    }
    else
    {
      print __LINE__, " ATTN: no eprints objects found";
      return undef;
    }
  }
  elsif ($eprint_ids[0] eq 'block')
  {
    my $block_num= $eprint_ids[1];
    @eprint_ids= $epr->get_eprint_ids_for_block($block_num);
    print __LINE__, " block_num=[$block_num], eprint_ids: ", join(' ', @eprint_ids), "\n";
    # return ([], 'dummy action, nothing happened...');
  }

  my @synced= ();
  my ($cnt_synced, $cnt_upload_ok, $cnt_errors_data, $cnt_errors_upload, $cnt_errors_ingest, $cnt_errors_attachments, $cnt_skipped)=
     (0, 0, 0, 0, 0, 0, 0);
  my ($cnt_att_synced, $cnt_att_ok, $cnt_att_errors_upload)= (0, 0, 0); # counters for the whole batch
  my $cnt_eprint_ids= @eprint_ids;
  print __LINE__, " ot2ut: ot2ut_eprint_status=$ot2ut_eprint_status cnt_eprint_ids=$cnt_eprint_ids MAX_SYNC=$MAX_SYNC\n";
  sleep(3);
  EPR: foreach my $eprint_id (@eprint_ids)
  {
    last unless ($running);
    last if (defined ($MAX_SYNC) && $cnt_synced >= $MAX_SYNC);

    # there are strange things going on at around midnight, so it might be best, to simply sleep it out...
    my @ts= localtime(time());
    if ($ts[2] == 23 && $ts[1] >= 50)
    {
      send_message("it's almost midnight! ot2ut pausing for 20 minutes...");
      activity({ activity => 'pause', note => ": sleep out midnight activities in the environment; context $ot2ut_context" });

      sleep(20*60);
      send_message("ot2ut pause ended");
    }

    activity({ activity => 'ot2ut', msg => ": processing $eprint_id; context $ot2ut_context" }); # CHECK if ($last_activity + $activity_period <= time());

    my $t_start= time();

    my $sync_info= $col_sync->find_one({ eprint_id => $eprint_id, context => $ot2ut_context });

    if (defined ($sync_info))
    {
      if ($sync_info->{error_code} ne 'ok' || $ot2ut_sync_anyway{$eprint_id})
      {
        print __LINE__, " earlier sync attempt had errors, retrying...\n";
        print __LINE__, " sync_info: ", Dumper($sync_info);
        $col_sync->remove( { _id => $sync_info->{_id} } );

        if ($sync_info->{error_code} eq 'ok')
        {
          my $context;
             if ($ot2ut_context eq 'ot2ut-test') { $context= 'test'; }
          elsif ($ot2ut_context eq 'ot2ut-entw') { $context= 'entw'; }
          elsif ($ot2ut_context eq 'ot2ut-prod') { $context= 'prod'; }

          my $block_nr= int($eprint_id/100);
          $col_blk->update({ nr => $block_nr, level => 0, context => $context },
                           { nr => $block_nr, level => 0, context => $context, '$inc' => { count_ok => -1 } },
                           { upsert => 0 });

          $block_nr= int($block_nr/100);
          $col_blk->update({ nr => $block_nr, level => 1, context => $context },
                           { nr => $block_nr, level => 1, context => $context, '$inc' => { count_ok => -1 } },
                           { upsert => 0 });
        }

        $sync_info= undef;
      }
    }

    my @res= generate_utheses_metadata($epr, $eprint_id);
    my ($errors, $warnings, $row, $lastmod, $ut, $utheses_json_path, $files, $docs, $utheses_upload_result_json_path)= @res;
    if (@res < 9)
    {
      print __LINE__, " something went wrong, not enough results: ", Dumper(\@res);
      next EPR;
    }

    my ($eprint_status)= map { $row->{$_} } qw(eprint_status);

    print __LINE__, " sync_info=[$sync_info]\n";
    if (defined ($sync_info))
    {
      print __LINE__, " sync_info: ", Dumper($sync_info);
      if ($sync_info->{error_code} eq 'ok' && !$force)
      {
        # TODO: check for updates in utheses row

        if ($sync_info->{lastmod} eq $lastmod)
        {
          print __LINE__, " eprint_id=[$eprint_id] already synced; skipping...\n";
          $cnt_skipped++;
          next;
        }
        else
        {
          print __LINE__, " ERROR/NOT YET IMPLEMENTED: already synced but othes object was modified again\n";
          print __LINE__, " ut2ot=[", $sync_info->{lastmod}, "] othes=[", $lastmod, "]\n";

          next;
        }
      }
    }

    my $ts_upload= ts_ISO_gmt();

    if (!defined ($files) || ref($files) ne 'ARRAY' || @$files < 1
        # || @$files != 1  # no attachments allowed yet
        # || @$files > 10 # testing ...
       )
    {
      push (@$errors, { error => 'num_files', note => 'currently limited to objects with exactly one file' } );
    }

    if (@$errors)
    { # report error
      print __LINE__, " ERRORS; ut: ", Dumper($ut);
      print __LINE__, " generate_utheses_metadata: errors: ", Dumper($errors) if (@$errors);
    }

    if (@$errors && ! $ignore_errors)
    {
      my $el=
      {
        eprint_id     => $eprint_id,
        eprint_status => $eprint_status,
        lastmod       => $lastmod,
        ts_upload     => $ts_upload,
        context       => $ot2ut_context,
        error_code    => 'conversion_errors',
        error_cnt     => scalar @$errors,
        errors        => $errors,
      };
      push (@synced, $el);
      $col_sync->insert($el);
      # NOTE: do not increment block counters, as this is an error

      send_message("upload error: eprint_id=[$eprint_id] eprint_status=[$eprint_status] lastmod=[$lastmod] [conversion errors]");
      my $utheses_errors_json_path= 'othes/utheses_json/errors/' . $eprint_id . '.json';
      Util::JSON::write_json_file($utheses_errors_json_path, $errors);

      $cnt_errors_data++;
    }
    else
    { # go ahead ...
      print __LINE__, " no errors; ut: ", Dumper($ut);
      print __LINE__, " files: ", Dumper($files);
      print __LINE__, " utheses_json_path=[$utheses_json_path]\n";

      my @docs= @{$docs->{documents}};
      my $main_file= shift(@docs);
      my ($local_filename, $lfnm)= map { $main_file->{$_} } qw(path_doc main);
      # $lfnm=~ s#\x{e4}#ae#g;

      # TODO: use curl for now
      my @upload_cmd= (qw(/usr/bin/curl -X POST -v -H Content-Type:multipart/form-data),
                       '-F', 'metadata=@' . $utheses_json_path, '-F', 'type=application/json;charset="UTF-8"',
                       '-F', 'file=@"' . $local_filename . '";filename="' . $lfnm . '"', '-F', 'type=application/pdf',
                       $upload_cnf->{import_url}, '-o' . $utheses_upload_result_json_path);

      push (@upload_cmd, @extra_curl_parameters) if (@extra_curl_parameters);

      print __LINE__, " upload_cmd: [", join(' ', @upload_cmd), "]\n";
      my $utheses_id;
      if ($do_upload)
      {
        my $t_curl= time();
        system(@upload_cmd);

        my $result_data;
        eval
        {
          # $result_data= from_json($upload_result);
          $result_data= Util::JSON::read_json_file($utheses_upload_result_json_path);
        };

        my $out_row; # mongodb record in the sync database (or collection)
        my ($upload_success, $td_start, $td_curl);
        if ($@)
        {
          print __LINE__, " can't parse upload_result; error=[$@]\n";
          push (@$errors, { error => 'upload_error', error_info => $@ });
          my $el=
          {
            eprint_id     => $eprint_id,
            eprint_status => $eprint_status,
            lastmod       => $lastmod,
            ts_upload     => $ts_upload,
            context       => $ot2ut_context,
            error_code    => 'upload_error',
            errors        => $errors,
          };

          push (@synced, $el);
          $col_sync->insert($el);
          $cnt_errors_upload++;
          # sleep(2);
          # NOTE: do not increment block counters, as this is an error
        }
        else
        {
          print __LINE__, " result_data: ", Dumper($result_data);

          my ($status, $import_status, $utheses_id1, $response_msg, $alerts)= map { $result_data->{$_} } qw(status importStatus uthesesId responseMsg alerts);
          print __LINE__, " status=[$status] response_msg=[$response_msg]\n";

          $td_start= time()-$t_start;
          $td_curl= time()-$t_curl;

          $out_row=
          {
            eprint_id        => $eprint_id,
            eprint_status    => $eprint_status,
            lastmod          => $lastmod,
            context          => $ot2ut_context,
            ts_upload        => $ts_upload,
            td_total         => $td_start,
            error_code       => 'unknown',
            utheses_id       => $utheses_id1,
            uploaded_fnm     => $lfnm,
            upload_status    => $status,
            response_msg     => $response_msg,
            attachment_count => scalar @docs,
          };

          if (defined ($utheses_id1) && $status eq '200')
          {
            $utheses_id= $utheses_id1;
            $out_row->{error_code}= $upload_success= 'ok';
            $out_row->{error_cnt}= 0;
            $cnt_upload_ok++;

            # REDIS Update
            if (defined ($redis_connection))
            {
              my $ut_url= $upload_cnf->{utheses_public_base_url} . $utheses_id1 . '#';
              my $rc= $redis_connection->set( $eprint_id => $ut_url );
              print __LINE__, " updated redis: eprint_id=[$eprint_id] ut_url=[$ut_url]\n";
              # sleep(10);
            }
          }
          else
          {
            $out_row->{error_code}= $upload_success= 'ingest_error';
            $cnt_errors_ingest++;
          }

          push (@synced, $out_row);

# moved down
#         $col_sync->insert($out_row);
#         send_message("upload $upload_success: eprint_id=[$eprint_id] eprint_status=[$eprint_status] lastmod=[$lastmod] context=[$ot2ut_context] utheses_id=[$utheses_id] time_total=$td_start time_upload=$td_curl") unless ($silent_upload_success && $upload_success eq 'ok');

        }

        # sleep(2);

        if (defined ($utheses_id))
        {
          # process remaining documents as attachments
          my ($attachment_number, $attachment_ok, $attachment_error)= (0, 0, 0);
          my $attachment_pid;
          my $curl_status;
          while (my $attachment= shift(@docs))
          {
            $attachment_number++;
            my $fnm_attachment_md=  'othes/utheses_json/' . $ot2ut_context . '/' . $eprint_id . '_' . $attachment_number . '_md.json';
            my $fnm_attachment_res= 'othes/utheses_json/' . $ot2ut_context . '/' . $eprint_id . '_' . $attachment_number . '_res.json';
            my $fnm_attachment_chg= 'othes/utheses_json/' . $ot2ut_context . '/' . $eprint_id . '_' . $attachment_number . '_chg.json';

            my $attachment_md=
            {
              attachment => my $ai=
              {
                file_name => $attachment->{main},
                file_mime_type => $attachment->{mime_type},
                lock_status => ($attachment->{security} eq 'public') ? 0 : 1,
                upload_date => Util::ts::ts_gmdate(),
                uploaded_by => 'ot2ut',
                size => int($attachment->{stat_doc}->[7] / 1024) . ' KB',
              },
              origin => "admin",
              rights_statement => "http://rightsstatements.org/vocab/InC/1.0/",
              data_model_type => "attachment",
            };

            $ai->{embargo_until_date}= join ('-', map { $attachment->{$_} } qw(date_embargo_year date_embargo_month date_embargo_day)) if (exists ($attachment->{date_embargo}));
            $ai->{description}= $attachment->{formatdesc} if (defined ($attachment->{formatdesc}));

            Util::JSON::write_json_file($fnm_attachment_md, $attachment_md);

# curl -X POST http://localhost:3000/attachment/add/#uthesesId/fromOthes -F "metadata=@uthesesDM.json" -F "file=@attachment.jpg" 
            my $url1= join ('/', $upload_cnf->{api_url}, qw(attachment add), $utheses_id, 'fromOthes');
            my @attachment_add_cmd= (qw(/usr/bin/curl -X POST -v -H Content-Type:multipart/form-data),
               '-F', 'metadata=@"' . $fnm_attachment_md . '"',
               '-F', 'file=@"' . $attachment->{path_doc} . '"',
               $url1, '-o' . $fnm_attachment_res);

            push (@attachment_add_cmd, @extra_curl_parameters) if (@extra_curl_parameters);

            my $att_status;
            print __LINE__, " attachment_add_cmd: [", join(' ', @attachment_add_cmd), "]\n";
            if ($do_upload)
            {
              my $t_curl= time();
              system(@attachment_add_cmd);
              $cnt_att_synced++;

              my $result_data1;
              eval { $result_data1= Util::JSON::read_json_file($fnm_attachment_res); };
              if ($@)
              {
                print __LINE__, " can't parse upload_result; error=[$@]\n";
                $cnt_att_errors_upload++;
                $attachment_error++;
                $att_status= $ai->{error_code}= 'upload_error';
                $ai->{errors}= [ { error => 'upload_error', error_info => $@ } ];
              }
              else
              {
                print __LINE__, " upload attchment [$attachment_number] result: ", Dumper($result_data1);
                my ($attachment_pid1, $curl_status1, $response_msg)= map { $result_data1->{$_} } qw(attachmentPid status responseMsg);
                $ai->{attachment_pid}= $attachment_pid= $attachment_pid1;
                $ai->{upload_status}= $curl_status= $curl_status1;
                $ai->{response_msg}= $response_msg;

                if ($curl_status1 eq '200')
                {
                  $cnt_att_ok++;
                  $attachment_ok++;
                  $att_status= $ai->{error_code}= 'ok';
                }
                else
                {
                  $cnt_att_errors_upload++;
                  $attachment_error++;
                  $att_status= $ai->{error_code}= 'ingest_error';
                }

                $ai->{security}= $attachment->{security};
                if ($attachment->{security} eq 'public')
                { # set attachment status to Active (in Phaidra) only when this attachment is public

# curl -X POST http://localhost:3000/attachment/changeStatus/#attachmentPid -F "status=A" 
                  my $url2= join ('/', $upload_cnf->{api_url}, qw(attachment changeStatus), $attachment_pid);
                  my @attachment_chg_cmd= (qw(/usr/bin/curl -X POST -v -H Content-Type:multipart/form-data -F status=A),
                                           $url2, '-o' . $fnm_attachment_chg);

                  push (@attachment_chg_cmd, @extra_curl_parameters) if (@extra_curl_parameters);

                  print __LINE__, " attachment_chg_cmd: [", join(' ', @attachment_chg_cmd), "]\n";
                  my $t_curl= time();
                  system(@attachment_chg_cmd);

                  my $result_data2;
                  eval { $result_data2= Util::JSON::read_json_file($fnm_attachment_res); };
                  if ($@)
                  {
                    print __LINE__, " can't parse upload_result; error=[$@]\n";
                    $ai->{activate}= 'error_curl';
                  }
                  else
                  {
                    print __LINE__, " change attachment [$attachment_number] result: ", Dumper($result_data2);
                    my ($attachment_pid2, $curl_status2, $response_msg2)= map { $result_data1->{$_ } } qw(attachmentPid status responseMsg);
                    $ai->{activate}= ($curl_status2 eq '200') ? 'ok' : 'error_activate';
                    $ai->{activate_status}= $curl_status2;
                    $ai->{activate_response_msg}= $response_msg2;
                  }
                } # if attachment is public

              } # when main document was uploaded successfully

            } # if ($do_upload)
            else
            { # attachment was not uploaded
              $att_status= 'no_upload';
            }

            # finish processing of this one attachment

            send_message("upload attachment $att_status: eprint_id=[$eprint_id] context=[$ot2ut_context] utheses_id=[$utheses_id] attachment_number=[$attachment_number] attachment_pid=[$attachment_pid] curl_status=[$curl_status]"); # unless ($silent_upload_success);

            $ai->{eprint_id}= $eprint_id;
            $ai->{context}= $ot2ut_context;
            $ai->{utheses_id}= $utheses_id;

            $col_att->insert($ai); # NOTE/TODO: no effort is made to check for duplicate uploads of attachments;
          } # end of processing for one attachment

          # TODO 2020-11-09: check, if uploading attachments returned errors, record this fact in the objects sync record
          if ($attachment_number > 0) # any attacments at all
          {
            # $out_row->{attachment_count}= $attachment_number;
            $out_row->{attachment_ok}=    $attachment_ok;
            $out_row->{attachment_error}= $attachment_error;

            if ($attachment_error)
            {
              $out_row->{error_code}= $upload_success= 'attachment_error';
              $cnt_errors_attachments++; # global counter
            }
          }

          $col_sync->insert($out_row);
          send_message("upload $upload_success: eprint_id=[$eprint_id] eprint_status=[$eprint_status] lastmod=[$lastmod] context=[$ot2ut_context] utheses_id=[$utheses_id] time_total=$td_start time_upload=$td_curl") unless ($silent_upload_success && $upload_success eq 'ok');

          my $context;
             if ($ot2ut_context eq 'ot2ut-test') { $context= 'test'; }
          elsif ($ot2ut_context eq 'ot2ut-entw') { $context= 'entw'; }
          elsif ($ot2ut_context eq 'ot2ut-prod') { $context= 'prod'; }
          my $block_nr= int($eprint_id/100);
          $col_blk->update({ nr => $block_nr, level => 0, context => $context },
                           { '$inc' => { count_ok => 1 } },
                           { upsert => 1 });

          $block_nr= int($block_nr/100);
          $col_blk->update({ nr => $block_nr, level => 1, context => $context },
                           { '$inc' => { count_ok => 1 } },
                           { upsert => 1 });

        }
        else
        {
          # no utheses_id defined, so upload must have gone wrong somehow
        }
      }
    }

    $cnt_synced++;
  }

  my $res;
  if ($cnt_synced)
  {
    $res= "synced $cnt_synced objects in context $ot2ut_context; data_errors: $cnt_errors_data; upload_errors: $cnt_errors_upload; ingest_errors: $cnt_errors_ingest; attachment_errors: $cnt_errors_attachments";
    my $fnm= sprintf('ot2ut_%s.tsv', ts_ISO());
    Util::Matrix::save_hash_as_csv(\@ot2ut_synced_columns, \@synced, $fnm, "\t", '',  "\n", 1);
    print __LINE__, " $res, see [$fnm]\n";
  }
  else
  {
    print __LINE__, " $res\n";
    $res= "synced no objects in context $ot2ut_context";
  }
  if ($cnt_skipped)
  {
    $res .= "; skipped $cnt_skipped objects";
  }

  (\@synced, $res);
}

sub generate_utheses_metadata
{
  my $epr= shift;
  my $eprintid= shift;

  my @errors= ();
  my @warnings= ();

  my $epr_db= $epr->connect();
  my $all_rows= $epr_db->get_all_x('eprint', ['eprintid=?', $eprintid]);
  my @keys= keys %$all_rows;
  print __LINE__, " keys=[", join(',', @keys), "]\n";
  if (@keys == 0)
  {
    print __LINE__, " WARNING: eprintid=[$eprintid] not found\n";
    return [ { error => 'eprintid_not_found' } ];
  }

  if (@keys > 1)
  {
    print __LINE__, " ERROR: eprintid=[$eprintid] returns multiple results\n";
    print __LINE__, " all_rows: ", Dumper($all_rows);
    return [ { error => 'eprintid_not_unique' } ];
  }

  my $row= $all_rows->{$eprintid};
  # print __LINE__, "row: ", Dumper ($row); exit;

  my $history= get_history($epr_db, $eprintid);
  # print __LINE__, " history: ", Dumper($history); exit;

  my ($eprint_status, $fileinfo, $dir)= map { $row->{$_} } qw(eprint_status fileinfo dir);

  my ($docs, $doc_errors)= get_documents($epr_db, $eprintid, $dir);
  print __LINE__, " docs: ", Dumper($docs);
  if (@$doc_errors)
  {
    print __LINE__, " doc_errors: ", Dumper($doc_errors);
    push (@errors, @$doc_errors);
  }

  my ($lang_pdf, $files)= analyze_files($eprintid, $fileinfo, $dir);
  print __LINE__, " lang_pdf=[$lang_pdf] files: ", Dumper($files);

  # merge info from get_documents() and analyze_files()
  my @d= @{$docs->{documents}};
  push (@errors, { error => 'no_file' }) unless (@d);
  my $main_file= $d[0]; # documents are ordered by their "pos" or "placement" attribute; the first on is the main document
  my %docs= map { $_->{path_doc} => $_ } @d;

  foreach my $fileinfo (@$files)
  {
    my $doc= $docs{$fileinfo->{path}};
    unless (defined ($doc))
    {
      push (@errors, { error => 'document_mismatch', fileinfo => $fileinfo });
      next;
    }
    $doc->{fileinfo}= $fileinfo;
  }

  print __LINE__, " docs: ", Dumper($docs);
  print __LINE__, " main_file: ", Dumper($main_file);

  my $utheses_json_path= 'othes/utheses_json/' . $ot2ut_context . '/' . $eprintid . '.json';
  my $utheses_upload_result_json_path= 'othes/utheses_json/' . $ot2ut_context . '/' . $eprintid . '_upload_result.json';

  my $lastmod= sprintf("%4d-%02d-%02dT%02d%02d%02d", map { $row->{$_} } qw(lastmod_year lastmod_month lastmod_day lastmod_hour lastmod_minute lastmod_second));
  # my $lastmod= get_othes_timestamp($row, 'lastmod'); that's a different format: yyyy-mm-ddTHH:MM:SSZ

  if (-f $utheses_json_path)
  {
    my @st= stat(_);
    my $mtime_ts= Util::ts::ts_ISO_gmt($st[9]);

    if ($mtime_ts gt $lastmod && !$force)
    {
      print __LINE__, " utheses metadata for eprintid=[$eprintid] already processed; procssing again\n";
      print __LINE__, " utheses_json_path=[$utheses_json_path}\n";
      # do not skip anymore return ([], undef);
    }
  }

  print __LINE__, " row: ", Dumper($row) if ($debug_level > 2);

  my ($err_st_id, $warn_st_id, $study_id, $studies_codes)= get_study_id($row->{matr}, $row->{studienkennzahl});
  push (@errors,   @$err_st_id)  if (@$err_st_id);
  push (@warnings, @$warn_st_id) if (@$warn_st_id);

  my $ut= new Univie::Utheses::Container();

  my @mapping_errors;
  my $utp= $ut->{public};

  $utp->{origin}= 'import';
  # not needed/wanted 2020-05-14: $utp->{datamodel}= 'container';
  $utp->{rights_statement}= 'http://rightsstatements.org/vocab/InC/1.0/';  # "In Copyright" or "Alle Rechte vorbehalten"

  if (defined ($row->{matr}))
  { # there are objects where it is NULL
    if (length ($row->{matr}) < 8)
    {
      push (@mapping_errors, { error => 'matrnr', matrnr => $row->{matr} } );
    }
    $utp->{student_id}= $row->{matr};
    $utp->{contact_email}=  'a'. $row->{matr} . '@unet.univie.ac.at';  # ANM: da kennen wir nur a<matr>@unet.univie.ac.at
  }

  $utp->{studies}= { codes => $studies_codes };
  if (defined ($row->{studiumsbezeichnung}))    # 31000x NULL
  { # there are many fields which have a blank at the end of the string
    my $s= $row->{studiumsbezeichnung};
    $s =~ s/^\s*//;
    $s =~ s/\s*$//;
    $utp->{studies}->{program_designation}= $s;
  }

  unless ($utheses_faculty_map_loaded)
  {

=begin comment

the structure of json file has slightly changed
    my $utheses_faculty_list= Util::JSON::read_json_file($fnm_utheses_faculty_map_old);
    # print __LINE__, " utheses_faculty_list: ", Dumper($utheses_faculty_list);
    my %utheses_faculty_map= map { $_->{oracle_id} => $_ } @$utheses_faculty_list;

=end comment
=cut

    # 2020-09-21: new structure; this includes historic faculty codes
    my $utheses_departments_list= Util::JSON::read_json_file($fnm_utheses_departments_map_new);
    # print __LINE__, " utheses_faculty_list: ", Dumper($utheses_faculty_list);
    my %utheses_faculty_map= map { $_->{oracle_id} => $_ } @{$utheses_departments_list->{departments}};

    $utheses_faculty_map= \%utheses_faculty_map;
    Util::JSON::write_json_file('@facultymap.json', $utheses_faculty_map);
    # print __LINE__, " utheses_faculty_map: ", Dumper($utheses_faculty_map); exit;
    $utheses_faculty_map_loaded= 1;
  }

  {
    my $faculty_code_str= $row->{fakultaet}; # like A32 etc..., but also NULL and ''
    $faculty_code_str= 'A108' if ($faculty_code_str eq 'PGC');

    my $faculty_info;
    if ($faculty_code_str =~ m#^A(\d+)$#)
    {
      my $faculty_code_num= $1;
      $faculty_info= $utheses_faculty_map->{$faculty_code_num};
    }
    elsif ($faculty_code_str =~ m#^[B-H]\d*$#) # e.g.: B, D2, F, F2, G, G2, H, H2
    { # historic codes, prepend 'hist-' to the string; this is how the reference file stores it.
      $faculty_info= $utheses_faculty_map->{'hist-' . $faculty_code_str};
    }

    if (defined ($faculty_info))
    {
      $utp->{studies}->{department}=
      {
        name => $faculty_info->{de},
        id => $faculty_info->{id},
      };
    }
    else
    {
      push (@mapping_errors, { error => 'faculty_no_match', fakultaet => $faculty_code_str });
    }
  }

=begin comment

  Die study_id kann man nicht wirklich errechnen.  Wenn keine study_id
  uebergeben wird, wird auf empfangender Seite ein frischer Container
  erzeugt.  Die study_id oder die utheses_id wird benoetigt, wenn man
  spaeter zu diesem Container etwas anhaengen will.

  # $utp->{study_id}= $study_id;

=end comment
=cut

  # ZZZ
  foreach my $an (keys %map_ot2ut_json_columns)
  {
    my $av= $row->{$an};
    my $an2= $map_ot2ut_json_columns{$an};
    print __LINE__, " an=[$an] an2=[$an2] av=[$av]\n";
    next if ($av eq '');
    $utp->{$an2}= $av;
  }

  # $utp->{utheses_id}= wird erzeugt beim Import, kennma ned wissn
  $utp->{utheses_status}= ($eprint_status eq 'archive')
                          ? 'published'
                          : 'thesis_doc_added';  # objects in eprint_status "buffer" are 'thesis_doc_added', formerly 'work_in_progress';

# Q1
# $utp->{utheses_status_last_modified}=  get_othes_timestamp($row, 'status_changed');
# $utp->{phaidra_thesis_doc_added_date}= get_othes_timestamp($history->{create}, 'timestamp');
  $utp->{othes}->{history_create}=       get_othes_timestamp($history->{create}, 'timestamp');
  $utp->{utheses_status_last_modified}=  get_othes_timestamp($history->{(exists ($history->{move_buffer_to_archive})) ? 'move_buffer_to_archive' : 'create'}, 'timestamp');
  $utp->{last_modified}=                 get_othes_timestamp($row, 'lastmod');

  $utp->{import}= # stored verbatim in mysql table utheses_import in column import_info
  {
    label => 'Othes', # used as value in column import_label
    info =>
    {
      source =>
      {
        repository  => 'E-Theses',
        application => 'EPrints3',
        host        => 'othesg8.mysql.univie.ac.at',
        db          => 'othesg8',
        table       => 'eprint',
        key         => 'eprintid',
        note        => 'primarykey',
        value       => $eprintid,

        # ev. eprint_row  => $row, # the full thing...

        # TODO: add information about the uploaded files
      },
      fields_processed => # TODO: may be obosolete
      {
        (map { $_ => $row->{$_} } qw( thesis_type )),
        # thesis_type: various strings; see %map_ot2ut_thesis_type
      },
      fields_not_yet_processed =>
      {
        (map { $_ => $row->{$_} } qw( pdf_ident place_of_pub edit_lock_user edit_lock_since edit_lock_until )),

        # type: always 'thesis'
        # place_of_pub: always 'wien'
        # metadata_visibility: always 'show'
        # always NULL: department 

        # various timestamps
        (map { $_ => get_othes_timestamp($row, $_) } qw( datestamp )),
      },
    },
  };

  if (@mapping_errors)
  {
    $utp->{import}->{info}->{mapping_errors}= \@mapping_errors;
    push (@errors,   @mapping_errors);
    # push (@warnings, @mapping_errors);
  }

  my ($creators_xml, $creators_json)= $epr->get_creators($eprintid);
  $ut->public('authors', $creators_json);

  foreach my $role (keys %map_ot2ut_roles)
  {
    print __LINE__, " extracting names for role=[$role]\n";
    my ($errors, $warnings, $names)= get_names_for_role($row, $map_ot2ut_roles{$role});

    push (@errors,   @$errors)   if (@$errors);
    push (@warnings, @$warnings) if (@$warnings);

    $ut->public($role, $names);
  }

  my ($errors1, $warnings1, $thesis)= get_thesis_data($row);
  push (@errors, @$errors1) if (@$errors1);
  push (@warnings, @$warnings1) if (@$warnings1);

  my ($errors2, $classifications)= $epr->get_classifications($eprintid);
  push (@errors, @$errors2) if (@$errors2);

  # 2020-05-14 nd: not needed: $thesis->{uploaded_by}= 'ot2ut';
  $thesis->{subject_classifications}= $classifications;
  $thesis->{number_of_pages}= "$main_file->{fileinfo}->{page_count}";  # Phaidra or utheses expects this as a string

  # 2020-06-30: modify filename for files containing sensitive information, but keep the original filename
  $thesis->{original_filename}= $main_file->{fileinfo}->{orig_fnm};
  $thesis->{upload_filename}= $main_file->{fileinfo}->{upl_fnm};

  # 2020-08-05:
  $thesis->{policies}->{fulltext_locked}= 0 if ($eprint_status eq 'archive' && $main_file->{security} eq 'public');

  # 2020-11-23: column formatdesc in table document contains notes by UBW staff for that particular document.
  # For attachments, these are all but the first document, this is stored as "description" in the attachment's metadata.
  # For the main document it self, this information was not stored anywhere.
  # Feature Request: store that information into utheses_note_2, e.g. by concatinating it
  if (defined ($main_file->{formatdesc}) && $main_file->{formatdesc} ne '')
  {
    $utp->{utheses_note_2}= (exists($utp->{utheses_note_2}))
                            ? join('; ', $utp->{utheses_note_2}, $main_file->{formatdesc})
                            : $main_file->{formatdesc};
  }

  $ut->public('thesis', $thesis);

  # Mon May 11 22:12:38 CEST 2020 asked nd about this, especially thesis_doc_added_date:
  my %phaidra= map { $_ => '' } qw(container_pid container_status container_created_date thesis_doc_pid thesis_doc_status);
  $phaidra{thesis_doc_added_date}= get_othes_timestamp($history->{create}, 'timestamp');
# $phaidra{thesis_doc_added_date}= get_othes_timestamp($row, 'datestamp');

  $ut->public('phaidra', \%phaidra);

  Util::JSON::write_json_file($utheses_json_path, $ut->{public});

  (\@errors, \@warnings, $row, $lastmod, $ut, $utheses_json_path, $files, $docs, $utheses_upload_result_json_path);
}

sub get_documents
{
  my $epr_db= shift;
  my $eprint_id= shift;
  my $dir= shift;

  my @errors= ();

  my $document_rows= $epr_db->get_all_x('document', ['eprintid=?', $eprint_id]);
  # print __LINE__, " document_rows: ", Dumper($document_rows);

  my @dirs= split('/', $dir);
  shift(@dirs); # remove "disk0" from the beginning

  my @notes;
  my $res=
  {
    eprint_id      => $eprint_id,
    cnt_docs       => 0,
    cnt_public     => 0,
    cnt_restricted => 0,
    cnt_embargo    => 0,
    show => 0, # internal flag
    notes => \@notes,
  };

  my @docs;
  DOCUMENT: foreach my $document_id (keys %$document_rows)
  {
    my $row= $document_rows->{$document_id};

    # print __LINE__, " document_row: ", Dumper($row);
    my ($main, $idx, $docid)= map { $row->{$_} } qw(main pos docid);

    # FIXUP: modifying database rows; this should be done in the database by the application!
    # eprint_id 7355: the first pdf is the attachment, the second one is the actual thesis; UB can not fix this in the app
    $row->{placement}= 2 if ($docid == 7538);
    $row->{placement}= 1 if ($docid == 7539);

    if ($row->{main} eq 'indexcodes.txt' || $row->{main} eq 'preview.png')
    { # ignore these ...
      next DOCUMENT;
    }

    my $rev_dir= sprintf("%02d", $idx);
    my $path_doc= join('/', '/backup/othes/eprints', @dirs, $rev_dir, $main); # TODO: parametrize!
    my (@stat_doc)= stat($path_doc);
    $row->{path_doc}= $path_doc;
    $row->{stat_doc}= \@stat_doc;
    unless (@stat_doc)
    {
      my $ec= { error => 'doc_missing', path_doc => $path_doc, rev_dir => $rev_dir, dir => $dir };
      push (@errors, $ec);
      $row->{error}= $ec;
    }

    if (defined ($row->{placement}) && $row->{pos} != $row->{placement})
    {
      push (@notes, "pos != placement: pos=[$row->{pos}] placement=[$row->{placement}]");
      $res->{show}++;
      $idx= $row->{placement};
    }

    $idx--; # NOTE: pos and placement start at 1
    # print __LINE__, " idx=[$idx]\n";
    if (defined($docs[$idx]))
    {
      push (@notes, "already a document at index=[$idx]");
      $res->{show}++;
      push (@docs, $row);
    }
    elsif ($idx < 0)
    {
      push (@notes, "negative index=[$idx]");
      $res->{show}++;
      push (@docs, $row);
    }
    else
    {
      $docs[$idx]= $row;
    }

    $res->{cnt_docs}++;
    if ($row->{security} eq 'public')
    {
      $res->{cnt_public}++;
    }
    else
    {
      $res->{cnt_restricted}++;
    }

    if (defined ($row->{date_embargo_year}))
    {
      $res->{cnt_embargo}++;
      $row->{date_embargo}= my $d= get_othes_timestamp($row, 'date_embargo');

      if ($d eq '')
      {
        push (@notes, "embargo date empty");
        $res->{show}++;
      }

      $doc_embargo_dates{$d}++;
    }
  }

  # document list may contain holes, that is, undefined items, so we remove these
  my @docs2;
  foreach my $doc (@docs)
  {
    push (@docs2, $doc) if (defined ($doc));
  }

  $res->{documents}= \@docs2,

  return ($res, \@errors);
}

sub get_history
{
  my $epr_db= shift;
  my $eprintid= shift;

  my $history_rows= $epr_db->get_all_x('history', ['objectid=?', $eprintid]);
  # print __LINE__, " history_rows: ", Dumper($history_rows);

  my %historyids;
  my ($create, $move_buffer_to_archive);
  foreach my $historyid (keys %$history_rows)
  {
    my $row= $history_rows->{$historyid};
    # print __LINE__, " history_row: ", Dumper($row);

    # NOTE: a revision can be present multiple times, so we need to sort by historyid
    # $revisions{$row->{revision}}= $row;
    $historyids{$historyid}= $row;

    $create=                 $row if ($row->{action} eq 'create'                 && !defined ($create));
    $move_buffer_to_archive= $row if ($row->{action} eq 'move_buffer_to_archive' && !defined ($move_buffer_to_archive));
  }
  my @historyids= sort { $a <=> $b } keys %historyids;
  print __LINE__, " historyids: ", join(' ', @historyids), "\n";

  my @events= map { $historyids{$_} } @historyids;

  my $history=
  {
    events => \@events,
    create => $create,
    last => $events[$#events],
  };

  # this only applies to objects with eprint_status='archive'; not present when eprint_status='buffer'
  $history->{move_buffer_to_archive}= $move_buffer_to_archive if (defined ($move_buffer_to_archive));

  $history;
}

sub get_study_id
{
  my $matr= shift;
  my $stkz= shift;

  my (@errors, @warnings, @stkz, $studies_codes);

  # NOTES:
  # * Bildungseinrichtungs-ID (here university_code) see
  #   https://wiki.univie.ac.at/pages/viewpage.action?pageId=71892188

  if (defined ($stkz) && $stkz ne '')
  {
    my ($unikz, $digits, $coop_kz);
       if ($stkz =~ m#^\s*U?([A-Z])\s*([\d ]+)U?([A-Z])?$#) { ($unikz, $digits, $coop_kz)= ('U'.$1, $2, $3); }
    elsif ($stkz =~ m#^\s*([\d ]+)$#) { ($unikz, $digits)= ('UA', $1); }
    else { push (@errors, { error => 'stkz_format_unknown', stkz => $stkz }); }

    if (defined ($unikz) && defined ($digits))
    {
      $digits=~ s/ //g;
      if ($digits)
      {
        while ($digits =~ s#^(\d\d\d)#{push(@stkz,$1);''}#ge){};
        if ($digits ne '')
        {
          if ($digits =~ m#^0?(2)$# && !defined ($coop_kz))
          {
            $coop_kz= $1;
          }
          else
          {
            push (@errors, { error => 'stkz_bad_format_ed', stkz => $stkz });
          }
        }

        $studies_codes= { university_code => $unikz, code_1 => $stkz[0] };
        $studies_codes->{code_2}= $stkz[1] if (defined ($stkz[1]));
        $studies_codes->{code_3}= $stkz[2] if (defined ($stkz[2]));

        if (defined ($coop_kz))
        {
          $studies_codes->{cooperation_code}= 'U'. $coop_kz if ($coop_kz =~ m#[A-Z]#);
          $studies_codes->{cooperation_code}= '0'. $coop_kz if ($coop_kz =~ m#^2$#);
        }
      }
      else
      {
        push (@errors, { error => 'stkz_bad_format_nd', stkz => $stkz });
      }
    }
    else
    {
      push (@errors, { error => 'stkz_bad_format', stkz => $stkz });
    }
  }
  else
  {
    push (@warnings, { warning => 'stkz_empty' });
  }

  my $study_id= join ('', $matr, @stkz);

  (\@errors, \@warnings, $study_id, $studies_codes);
}

sub get_thesis_data
{
  my $row= shift;

  my @errors= ();
  my @warnings= ();

  my ($lang, $abstract, $abstract_eng, $title, $title_ger, $title_eng, $title_zusatz, $keywords, $keywords_eng)=
     map { my $x= $row->{$_}; $x=~ s#\r##g; $x=~ s#^\s*##; $x=~ s#\s*$##; $x; }
     qw(sprache abstract abstract_eng title title_ger title_eng title_zusatz keywords keywords_eng);

  # $abstract=~ s#\r##g; $abstract_eng=~ s#\r##g;
  $lang= 'deu' if ($lang eq 'ger'); # see notes below

  my @j_titles;
  push (@j_titles, { type => 'parallel', title_lang => $lang, title_text => $title,        origin => 'title'        }) if ($title);
  push (@j_titles, { type => 'parallel', title_lang => 'deu', title_text => $title_ger,    origin => 'title_ger'    }) if ($title_ger);
  push (@j_titles, { type => 'parallel', title_lang => 'eng', title_text => $title_eng,    origin => 'title_eng'    }) if ($title_eng);
# push (@j_titles, { type => 'parallel', title_lang => $lang, title_text => $title_zusatz, origin => 'title_zusatz' }) if ($title_zusatz);
  $j_titles[0]->{type}= 'main';
  if ($title_zusatz)
  {
    $j_titles[0]->{subtitle_text}= $title_zusatz;
    $j_titles[0]->{subtitle_lang}= $lang;
  };

  my @j_abstracts;
  push (@j_abstracts, { language => 'deu', text => $abstract,     origin => 'abstract'     }) if ($abstract);
  push (@j_abstracts, { language => 'eng', text => $abstract_eng, origin => 'abstract_eng' }) if ($abstract_eng);

  # NOTE: cleanup_keywords() returns a list of cleaned up keywords!
  # my @keywords=     split(/\s*\/\s*/, $keywords);
  # my @keywords_eng= split(/\s*\/\s*/, $keywords_eng);
  my ($n_kw,  $l_kw)=  cleanup_keywords($keywords);
  my ($n_kwe, $l_kwe)= cleanup_keywords($keywords_eng);

  my @j_keywords;
  # NOTE: each keyword as a separate element
  # NOTE: keywords is always in german, not in the same language as the text itself

  # 2020-01-31: old schema
  # push (@j_keywords, map { { language => 'eng', text => $_ } } @keywords_eng) if (@keywords_eng);
  # push (@j_keywords, map { { language => 'deu', text => $_ } } @keywords)     if (@keywords);

  # 2020-01-31: new schema: text is now an array reference
  # push (@j_keywords, { language => 'eng', text => \@keywords_eng }) if (@keywords_eng);
  # push (@j_keywords, { language => 'deu', text => \@keywords     }) if (@keywords);

  # NOTE: all keywords separated by comma
  # push (@j_keywords, { language => 'eng', text => join(', ', @keywords_eng) }) if (@keywords_eng);
  # push (@j_keywords, { language => 'deu', text => join(', ', @keywords)     }) if (@keywords);

  # 2020-06-15: use cleaned keyword lists
  push (@j_keywords, { language => 'eng', text => $l_kwe }) if (@$l_kwe);
  push (@j_keywords, { language => 'deu', text => $l_kw  }) if (@$l_kw);

  # TODO: language logic needs to be improved, this is plain bad.
  my %thesis=
  (
    languages => [ $lang ],
    titles => \@j_titles,
    abstracts => \@j_abstracts,
    keywords => \@j_keywords,

    type => $map_ot2ut_thesis_type{$row->{thesis_type}},

    policies => {
      # lock_status => ($row->{sperre} eq 'FALSE') ? 0 : 1, # TRUE or NULL means the object is locked
      lock_status => ($row->{sperre} eq 'TRUE') ? 1 : 0,    # TRUE means the object is locked; FALSE and NULL means not locked
      authorisation_to_use_by_author => ($row->{einverstaendnis} eq 'TRUE') ? 1 : 0,
      fulltext_locked => ($row->{full_text_status} eq 'public') ? 0 : 1, # possible values for fulltext_locked: NULL, none, resticted

      # authorisation_to_use_link => "www.google.com", # should be the link or address where uploader gave permission to use this work

      # see date_sperre_year below
      lock_until_date => '',
      lock_request => 0,

      # see abstract_nicht_anzeigen below
      abstract_locked => 0,
      keywords_locked => 0,
    },

  );

  my $policies= $thesis{policies};
  if ($row->{date_sperre_year})
  {
    $policies->{lock_until_date}= get_othes_timestamp($row, 'date_sperre'); # can be NULL
    $policies->{lock_request}= 1;

    my $ts_now= Util::ts::ts_ISO3_gmt(time());
    print __LINE__, " ts_now=[$ts_now] lock_until_date=[$policies->{lock_until_date}]\n";
    $policies->{lock_status}= 0 if ($row->{eprint_status} eq 'archive' && $policies->{lock_until_date} lt $ts_now); # not locked if lock_until_date is in the past
  }

  if ($row->{eprint_status} eq 'buffer')
  {

=begin comment

nd 2020-06-30: fixup fulltext_locked for items with eprint_status='buffer' depending on einverstaendnis and sperre

NOTES:
* we use already processed and maybe fixed (read: corrected) values here
* einverstaendnis is used as authorisation_to_use_by_author
* sperre is used as lock_status
* this whole thing aims at those records where einverstaendnis='TRUE' and sperre='FALSE';
  for these we should get fulltext_locked=0, most everything else should be fulltext_locked=1,
  the few cases that may remain are controlled by full_text_status which is usually 'restricted'

mysql> select count(*), einverstaendnis, sperre, full_text_status
    ->   from eprint where eprint_status='buffer'
    ->  group by einverstaendnis, sperre, full_text_status;
+----------+-----------------+--------+------------------+
| count(*) | einverstaendnis | sperre | full_text_status |
+----------+-----------------+--------+------------------+
|        1 | NULL            | NULL   | none             |
|        1 | NULL            | NULL   | restricted       |
|        1 | NULL            | TRUE   | restricted       |
|        5 | TRUE            | NULL   | restricted       |
|        1 | TRUE            | TRUE   | restricted       |
|     2064 | TRUE            | FALSE  | restricted       |
|        4 | FALSE           | NULL   | restricted       |
|        2 | FALSE           | TRUE   | restricted       |
|     2868 | FALSE           | FALSE  | restricted       |
+----------+-----------------+--------+------------------+

=end comment
=cut

    $policies->{fulltext_locked}= 0 if ($policies->{authorisation_to_use_by_author} == 1 && $policies->{lock_status} == 0);
    $policies->{fulltext_locked}= 1 if ($policies->{authorisation_to_use_by_author} == 0);
  }

  if ($row->{abstract_nicht_anzeigen} eq 'TRUE')
  {
    $policies->{abstract_locked}= $policies->{keywords_locked}= 1;

=begin comment

mysql> select count(*), abstract_nicht_anzeigen from eprint group by abstract_nicht_anzeigen;
+----------+-------------------------+
| count(*) | abstract_nicht_anzeigen |
+----------+-------------------------+
|    20381 | NULL                    |
|    36215 |                         |
|      656 | TRUE                    |
+----------+-------------------------+
3 rows in set (0.20 sec)

=end comment
=cut

  }

  # ZZZ
  foreach my $an (keys %map_ot2ut_thesis_columns)
  {
    my $av= $row->{$an};
    my $an2= $map_ot2ut_thesis_columns{$an};
    print __LINE__, " an=[$an] an2=[$an2] av=[$av]\n";
    next if ($av eq '');
    $thesis{$an2}= $av;
  }

  my $assessment_date= get_othes_timestamp($row, 'date_app');

  if (defined ($assessment_date))
  {
    $thesis{assessment_date}= $assessment_date;
  }
  else
  { # 2020-05-29 15:12 nd Commented on gg's message: @nd hast du meine messages bezueglich eprint_id=1982 gesehen? dort ist date_app_year NULL, d.h. es gibt kein assessment date; wir haben 95 solcher objekte ...
    # diese objekte sollten wir uns notieren. bitte stattdessen einen leeren string uebergeben.

    push (@warnings,  { warning => 'date_app missing, can not assign assessment_date' });
    $thesis{assessment_date}= undef;
  }

  # print __LINE__, " thesis: ", Dumper (\%thesis);
  (\@errors, \@warnings, \%thesis);
}

sub fixup_name
{
  my $c= shift;

  unless ($fn_init)
  {
    my $fnm= 'fixup_names_'.time().'.tsv';
    open(FN_lst, '>:utf8', $fnm);
    print __LINE__, " opening $fnm for writing\n";
    print FN_lst join("\t", @fn_lst), "\n";
    $fn_init= 1;
  }

  # let the generic fixups begin
  my @n;
  my ($f, $ei, $cn)= map { $c->{$_ } } qw(name eprint_id column_name);

  my $info;

  my ($rc, $nn, $vn);

  if (exists ($name_fixup_by_eprint_id{$ei}) && exists ($name_fixup_by_eprint_id{$ei}->{$cn}))
  {
    $f= $name_fixup_by_eprint_id{$ei}->{$cn};
    $rc= 'by_id';
    push (@n, 'by_id');
  }
  elsif (exists ($name_fixup_by_name{$f}))
  {
    my $x= $name_fixup_by_name{$f};
    $f= $x->{fn} if (exists ($x->{fn}));

    $rc= 'by_name';
    push (@n, 'by_name');
  }
  else
  { # TODO: refactor this ...
    push (@n, 'aoup')  if ($f=~ s#\ba\.o\s+univ\.\s*prof\.\s*##i);
    push (@n, 'hr')    if ($f=~ s#\bhr\.\s*##i);
    push (@n, 'tit')   if ($f=~ s#\btit\.\s*##i);
    push (@n, 'ao1')   if ($f=~ s#\ba\.\s*\bo\.\s*##i);
    push (@n, 'ao1')   if ($f=~ s#\bao\s+##i);
    push (@n, 'ao')    if ($f=~ s#\bao\.\s*##i);
    push (@n, 'ir')    if ($f=~ s#\bi\.\s*r\.\s*##i);
    push (@n, 'o')     if ($f=~ s#\bo\.\s*##i);
    push (@n, 'frau')  if ($f=~ s#\bfrau\s+##i);
    push (@n, 'emer')  if ($f=~ s#\bemer\.\s*##i);
    push (@n, 'profin')if ($f=~ s#\bprofin\.\s*##i);
    push (@n, 'em2')   if ($f=~ s#\be\.m\.\s*##i);
    push (@n, 'em3')   if ($f=~ s#\bem\.\s*##i);
    push (@n, 'di')    if ($f=~ s#\bdipl\.[\-\s]*ing\.(in)?\s*##i);
    push (@n, 'dpsy')  if ($f=~ s#\bdipl\.[\-\s]*psych\.\s*##i);
    push (@n, 'dgeog') if ($f=~ s#\bdipl\.[\-\s]*geogr\.\s*##i);
    push (@n, 'dgeol') if ($f=~ s#\bdipl\.[\-\s]*geol\.\s*##i);
    push (@n, 'dtheo') if ($f=~ s#\bdipl\.[\-\s]*theol\.\s*##i);
    push (@n, 'dbio')  if ($f=~ s#\bdipl\.[\-\s]*biol\.\s*##i);
    push (@n, 'dp')    if ($f=~ s#\bdipl\.[\-\s]*p[äÄ]d\.\s*##i);
    push (@n, 'vp')    if ($f=~ s#\bv\.[\-\s]*prof\.\s*##i);
    push (@n, 'hp')    if ($f=~ s#\bhon\.[\-\s]*prof\.\s*##i);
    push (@n, 'up')    if ($f=~ s#\buniv\.?[\-\s]*prof\.(in)?\s*##i);
    push (@n, 'upt')   if ($f=~ s#\buniv\.[\-\s]*porf\.\s*##i); # typo ...
    push (@n, 'apin')  if ($f=~ s#\bass\.[\-\s]*prof\.in\s*##i);
    push (@n, 'ap')    if ($f=~ s#\bass\.[\-\s]*prof\.\s*##i);
    push (@n, 'acp')   if ($f=~ s#\bassoc\.[\-\s]*prof\.(in)?\s*##i);
    push (@n, 'azp')   if ($f=~ s#\bassoz\.[\-\s]*prof\.(in)?\s*##i);
    push (@n, 'pdoz')  if ($f=~ s#\bpriv\.[\s\-]*doz\.\s*##i);
    push (@n, 'udoz')  if ($f=~ s#\buniv\.[\s\-]*doz\.\s*##i);
    push (@n, 'uass')  if ($f=~ s#\buniv\.[\s\-]*ass\.\s*##i);
    push (@n, 'ing')   if ($f=~ s#\bing\.\s*##i);
    push (@n, 'hr')    if ($f=~ s#\bhr\s+##i);
    push (@n, 'pd')    if ($f=~ s#\bpd\s+##i);
    push (@n, 'ra')    if ($f=~ s#\bra\s+##i);
    push (@n, 'der')   if ($f=~ s#\bder\.\s*##i);
    push (@n, 'doz')   if ($f=~ s#\bdoz\.\s*##i);
    push (@n, 'p')     if ($f=~ s#\bprof\.\s*##i);
    push (@n, 'mgr')   if ($f=~ s#\bmgr\.a\s+##i);
    push (@n, 'maga')  if ($f=~ s#\bmag\.a\s+##i);
    push (@n, 'mmag')  if ($f=~ s#\bmmag\.\s*##i);
    push (@n, 'mag')   if ($f=~ s#\bmag\.\s*##gi);
    push (@n, 'rndr')  if ($f=~ s#\brndr\.\s*##i);
    push (@n, 'csc')   if ($f=~ s#\bcsc\.\s*##i);
    push (@n, 'phdr')  if ($f=~ s#\bphdr\.\s*##i);
    push (@n, 'ddr')   if ($f=~ s#\bddr\.\s*##i);
    push (@n, 'ddr2')  if ($f=~ s#\bddr\b\s*##i);
    push (@n, 'ddr2')  if ($f=~ s#\bpd\b\s*##i);
    push (@n, 'drin')  if ($f=~ s#\bdr\.in\s*##gi);
    push (@n, 'dra')   if ($f=~ s#\bdr\.a\s*##gi);
    push (@n, 'drt')   if ($f=~ s#\bdr\.\s*techn\.\s*##gi);
    push (@n, 'pddr')  if ($f=~ s#\bpd\s+dr\.\s*##gi);
    push (@n, 'hdr')   if ($f=~ s#\bhofratdr\.\s*##gi);
    push (@n, 'dr')    if ($f=~ s#\bdr[\.\s]+##gi);
    push (@n, 'habil') if ($f=~ s#\bhabil\.\s*##i);
    push (@n, 'rer')   if ($f=~ s#\brer\.\s*##i);
    push (@n, 'nat')   if ($f=~ s#\bnat\.\s*##i);
    push (@n, 'soc')   if ($f=~ s#\bsoc\.\s*##i);
    push (@n, 'oec')   if ($f=~ s#\boec\.\s*##i);
    push (@n, 'phil')  if ($f=~ s#\bphil\.\s*##i);
    push (@n, 'med')   if ($f=~ s#\bmed\.\s*##i);
    push (@n, 'pharm') if ($f=~ s#\bpharm\.\s*##i);
    push (@n, 'u')     if ($f=~ s#\buniv\.\s*##i);
    push (@n, 'hc1')   if ($f=~ s#\bh\.c\.\s*##i);
    push (@n, 'hc2')   if ($f=~ s#\bhc\.\s*##i);
    push (@n, 'phd1')  if ($f=~ s#\bph\.d\.\s*##gi);
    push (@n, 'ma')    if ($f=~ s#[,\s]+ma\s*$##i);
    push (@n, 'mas')   if ($f=~ s#[,\s]+mas\s*$##i);
    push (@n, 'bakk')  if ($f=~ s#[,\s]+bakk\.?\s*$##i);
#   push (@n, 'llm')   if ($f=~ s#[,\s]+llm\.\s*$##i);
    push (@n, 'llm')   if ($f=~ s#[,\s]+ll\.?m\.?\s*$##i);
    push (@n, 'phd2')  if ($f=~ s#,\s*ph\.?d\.\s*##i);
    push (@n, 'pdoz2') if ($f=~ s#,\s*privatdoz\.\s*##i);
  }

  $c->{fixed1}= $f;

  if (defined ($info))
  {
  }
  else
  {
    ($nn, $vn)= split(/\s*,\s*/, $f, 2);
    if ($vn)
    {
      $c->{nn}= $nn;
      $c->{vn}= $vn;
      push (@n, 'csv');
      $rc= 'split' unless (defined ($rc));
    }
    else
    {
      my @f= split(' ', $f);
      my (@vn, @nn);
      foreach my $n (@f)
      {
        if ($n =~ m#^[A-Z]\.$# || exists ($first_names{$n})) { push (@vn, $n) } else { push (@nn, $n); }
      }

      unless (@nn)
      { # if there are only firstnames, the last one will be the family name (just guessing)...
        push (@nn, pop(@vn));
        push (@n, 'fn-only');
      }

      $nn= $c->{nn}= join(' ', @nn);
      $vn= $c->{vn}= join(' ', @vn);
      push (@n, 'fn-picker');

      $rc= 'picked' unless (defined ($rc));
    }
  }

  $c->{notes1}= join(',', @n);

  print FN_lst join("\t", map { $c->{$_} } @fn_lst), "\n";
  $fn_cnt++;

  return ($rc, $nn, $vn);
}

sub get_names_for_role
{
  my $row= shift;
  my $column_names= shift;

  # print __LINE__, " column_names: ", Dumper($column_names);

  my @errors= ();
  my @warnings= ();

  my @result;
  foreach my $column_name (@$column_names)
  {
    my $names= $row->{$column_name};
    print __LINE__, " names=[$names]\n";
    next if ($names eq '' || $names eq '-' || $names eq 'k. A.' || $names =~ m#nicht angegeben *#);

    my @names= split (/\s*;\s*/, $names);
    foreach my $name (@names)
    {
      $name=~ tr/\x{2010}\x{2011}\x{2012}\x{2013}\x{2014}\x{2015}/-/s; # hyphen, non-breaking hyphen, figure dash, en-dash, em-dash
      $name=~ s/  +/ /g;
      my ($nn, $vn)= split (/\s*,\s*/, $name, 2);
      $nn =~ s/^\s*//;
      $vn =~ s/\s*$//;
      print __LINE__, " column_name=[$column_name] name=[$name] nn=[$nn] vn=[$vn]\n";

      if ($vn eq '' || !($vn =~ m#^\U\E[\w\-\'\.<>() ]+$#) || !($nn =~ m#^\U\E[\w\-\'\. ]+$#))
      { # TODO: add option to flag this as a warning instead of as an error

        my ($rc, $nn1, $vn1)= fixup_name({ name => $name, ac_number => $row->{ac_nummer}, eprint_id => $row->{eprintid}, column_name => $column_name });
        print __LINE__, " fixup_name: rc=[$rc] nn1=[$nn1] vn1=[$vn1]\n";

        if (defined ($rc) && ($rc eq 'by_id' || $rc eq 'by_name' || $rc eq 'split' || $rc eq 'picked'))
        {
          push (@result, { family_name => $nn1, given_name => $vn1 });
        }
        else
        {
          push (@errors, { error => 'bad_name', column_name => $column_name, name => $name } );
          push (@result, { family_name => $name }); # fill everything in into family_name
        }
      }
      else
      {
        push (@result, { family_name => $nn, given_name => $vn });
      }
    }
  }

  (\@errors, \@warnings, \@result);
}

sub analyze_files
{
  my $eprintid= shift;
  my $fileinfo= shift;
  my $dir= shift;

  print __LINE__, " dir=[$dir] fileinfo=[$fileinfo]\n";

  my @fileinfos= split(/\|/, $fileinfo);
  my @dirs= split('/', $dir);
  shift(@dirs); # remove "disk0" from the beginning

  my %lang;
  my @files;
  foreach my $fi (@fileinfos)
  {
    my ($icon, $filepath)= split(';', $fi);

    my $format= 'unknown';
    $format= 'pdf' if ($icon eq '/style/images/fileicons/pdf.png' || $icon eq '/style/images/fileicons/application_pdf.png');

    $filepath =~ s#%([\dA-Fa-f]{2})#chr(hex($1))#ge; # filenames are URL encoded, see 19072 for an example
    print __LINE__, "  vor utf8::decode filepath=[$filepath]\n";
    utf8::decode($filepath);
    print __LINE__, " nach utf8::decode filepath=[$filepath]\n";

    my @filepath= split('/', $filepath);
    my $upl_fnm= my $fnm= pop(@filepath);

=begin comment

... not needed after all.
    # fix upload filename here
    # check if local_filename contains matr as pattern
    if ($upl_fnm =~ m#^[\d\-]+_\d{7}\.pdf$#)
    {
      $upl_fnm= join('_', 'othes', $eprintid, int(rand(1000000))) . '.pdf';
    }

=end comment
=cut

    my @fnm= split(/\./, $fnm);
    my $ext= pop(@fnm);
    print __LINE__, " dir=[$dir] fi=[$fi] fnm=[$fnm] ext=[$ext]\n";

    my $rev_dir= sprintf("%02d", pop(@filepath));
    my $path_pdf= join('/', '/backup/othes/eprints', @dirs, $rev_dir, $fnm); # TODO: parametrize!
    print __LINE__, " path_pdf=[$path_pdf]\n";

    push (@files, my $f_obj= { format => $format, path => $path_pdf, orig_fnm => $fnm, upl_fnm => $upl_fnm,});

    if ($format eq 'pdf')
    {
      my $path_txt= join('/', '/backup/othes/eprints', @dirs, $rev_dir, join ('.', @fnm, 'txt'));
      print __LINE__, " path_txt=[$path_txt]\n";

      my @st_pdf= stat($path_pdf);
      my @st_txt= stat($path_txt);
      if (!@st_txt || $st_txt[9] < $st_pdf[9])
      {
        system ('pdftotext', $path_pdf);
      }

      my $verdict= `/home/gg/bin/guesslang.py '$path_txt'`;
      print __LINE__, " guesslang: verdict=[$verdict]\n";
      my @pages= split("\n", $verdict);
      foreach my $page (@pages)
      {
        my ($lang)= split(' ', $page);
        # print __LINE__, " lang=[$lang]\n";
        $lang{$lang}++;
      }
      
      $f_obj->{page_count}= scalar @pages;
    }
  }

    my $max= 0;
    my $max_lang;
    foreach my $lang (keys %lang)
    {
      next if ($lang eq 'UNKNOWN');
      my $count= $lang{$lang};
      print __LINE__, " lang=[$lang] count=[$count]\n";
      if ($count > $max)
      {
        $max_lang= $lang;
        $max= $count;
      }
    }

  print __LINE__, " max_lang=[$max_lang]\n";

  return ($max_lang, \@files);
}

sub add_description
{
  my $s= shift;

print __LINE__, " s: [$s]\n";
  if (length($s) > 1)
  {
    return (<<"EOX");
    <description descriptionType="Abstract">
$s
    </description>
EOX
  }
  '';
}

sub strip_text
{
  my $s= shift;
  $s=~ s/\r//g; $s=~ s/^\s*//g; $s=~ s/\s*$//g;
  $s=~ s/&/&amp;/g;
  $s=~ s/</&lt;/g;
  $s=~ s/>/&gt;/g;
  $s;
}

sub debug_classifications
{
  my $epr= get_eprints_db($cnf);

  my $cl= $epr->get_classification_table();

  print __LINE__, " cl: ", Dumper($cl);
}


sub debug_names
{
  my $epr= get_eprints_db($cnf);
  # print "epr: ", Dumper ($epr);

  my $epr_db= $epr->connect();
  # print "epr_db: ", Dumper ($epr_db);

  my @col_names= qw( betreuer betreuer_2 betreuer_3 mitbetreuer mitbetreuer_2 beurteiler_1 beurteiler_2 beurteiler_3 );

  open (FO, '>:utf8', 'all_names.tsv') or die;
  print FO join("\t", qw(col_name eprintid eprint_status value)), "\n";
  foreach my $col_name (@col_names)
  {
    # my $query= "$col_name IS NOT NULL AND $col_name<>'' AND $col_name NOT LIKE '%,%'";
    my $query= "$col_name IS NOT NULL AND $col_name<>''";
    # print __LINE__, " query=[$query]\n";
    my $keys= $epr_db->get_all_x('eprint', [$query], "eprintid,eprint_status,$col_name");
    # print __LINE__, " keys: ", Dumper ($keys);

    # print __LINE__, " col_name=[$col_name]\n";
    foreach my $key (keys %$keys)
    {
      my $r= $keys->{$key};
      # print __LINE__, " key=[$key] ", Dumper($r);
      print FO join("\t", $col_name, map { $r->{$_} } ('eprintid', 'eprint_status', $col_name)), "\n";
    }
  }
  close(FO);
}

sub debug_keywords
{
  my $epr= get_eprints_db($cnf);

  my $epr_db= $epr->connect();
  my @col_names_db= qw( eprintid eprint_status sprache keywords keywords_eng );

# my $search_term= "eprint_status in ('archive', 'buffer') and sprache<>'ger' and sprache<>'eng'";
  my $search_term= "eprint_status in ('archive', 'buffer')";

  my $keys= $epr_db->get_all_x('eprint', [$search_term], join(',', @col_names_db));

  open (FO, '>:utf8', 'all_keywords.tsv') or die;
  # print FO join("\t", qw( eprintid eprint_status lang n_kw kw n_kwe kwe )), "\n";
  print FO join("\t", qw( eprintid eprint_status lang lang_kw n_kw kw )), "\n";

  my (%all_keywords_de, %all_keywords_en);
  my %all_othes;
  foreach my $key (keys %$keys)
  {
    my $r= $keys->{$key};
    # print __LINE__, " key=[$key] ", Dumper($r);
    my ($id, $es, $lang, $kw, $kwe)= map { $r->{$_} } @col_names_db;

=begin comment

    # fixups
    if ($id == 44374) { $kwe =~ s#\|#/#g; }
    if ($id == 56308 || $id == 39005) { $kwe =~ s#\\#/#g; $kw =~ s#\\#/#g; }

=end comment
=cut

    my ($n_kw,  $l_kw)=  cleanup_keywords($kw);
    my ($n_kwe, $l_kwe)= cleanup_keywords($kwe);

    my $rec=
    {
      eprint_id => $id,
      eprint_status => $es,
      language  => $lang,

        kw => $kw,
      l_kw => $l_kw,
      n_kw => $n_kw,

        kwe => $kwe,
      l_kwe => $l_kwe,
      n_kwe => $n_kwe,
    };
    # print __LINE__, " keywords: ", Dumper ($rec);

    $all_othes{$id}= $rec;

    # print FO join("\t", $id, $es, $lang, join('/', @$n_kw), join(' / ', @$l_kw), join('/', @$n_kwe), join(' / ', @$l_kwe)), "\n";

    # print FO join("\t", qw( eprintid eprint_status lang lang_kw n_kw kw )), "\n";
    print FO join("\t", $id, $es, $lang, 'ger', join('/', @$n_kw),  join(' / ', @$l_kw)), "\n";
    print FO join("\t", $id, $es, $lang, 'eng', join('/', @$n_kwe), join(' / ', @$l_kwe)), "\n";

    # foreach my $kw_de (@$l_kw)  { push (@{$all_keywords_de{$kw_de}->{othes}}, $id); }
    # foreach my $kw_en (@$l_kwe) { push (@{$all_keywords_en{$kw_en}->{othes}}, $id); }
  }
  close(FO);

  # Util::JSON::write_json_file('/backup/othes/eprints/test/all_keywords_de.json', \%all_keywords_de);
  # Util::JSON::write_json_file('/backup/othes/eprints/test/all_keywords_en.json', \%all_keywords_en);
  Util::JSON::write_json_file('/backup/othes/eprints/test/othes_keywords.json', \%all_othes);
}

sub debug_abstracts
{
  my $epr= get_eprints_db($cnf);

  my $epr_db= $epr->connect();
  my @col_names_db= qw( eprintid eprint_status sprache abstract abstract_eng );

# my $search_term= "eprint_status in ('archive', 'buffer') and sprache<>'ger' and sprache<>'eng'";
# my $search_term= "eprint_status in ('archive', 'buffer') and sprache='ger'";
# my $search_term= "eprint_status in ('archive', 'buffer') and sprache='eng'";
# my $search_term= "eprint_status in ('archive', 'buffer')";
  my $search_term= "eprintid in (2276, 3432, 8314, 9358, 10236, 10941, 15148, 15934, 18224, 23898, 27575, 28791, 30614, 32692, 35111, 38069, 40982, 42122, 43078, 44504, 44510, 46380, 46381, 49927, 51776, 52780, 52925, 56916, 60835)";

  my $keys= $epr_db->get_all_x('eprint', [$search_term], join(',', @col_names_db));

  open (FO, '>:utf8', 'all_keywords.tsv') or die;
  # print FO join("\t", qw( eprintid eprint_status lang n_kw kw n_kwe kwe )), "\n";
  print FO join("\t", qw( eprintid eprint_status lang lang_kw n_kw kw )), "\n";

  my (%all_keywords_de, %all_keywords_en);
  my %all_othes;
  foreach my $key (keys %$keys)
  {
    my $r= $keys->{$key};
    print __LINE__, " key=[$key] ", Dumper($r);
    my ($id, $es, $lang, $abs, $abse)= map { $r->{$_} } @col_names_db;
    # $abs =~ tr/ \t\r\n/ /s;
    # print join("\t", $id, $abs), "\n";
  }

}

sub debug_stbez
{
  my $epr= get_eprints_db($cnf);

  my $epr_db= $epr->connect();
  my @col_names_db= qw( eprintid studiumsbezeichnung );

  $epr_db->show_query(1);
  my $search_term= "studiumsbezeichnung IS NOT NULL AND eprint_status IN ('archive', 'buffer')";
  my $keys= $epr_db->get_all_x('eprint', [$search_term], join(',', @col_names_db));

  foreach my $key (keys %$keys)
  {
    my $r= $keys->{$key};
    # print __LINE__, " key=[$key] ", Dumper($r);
    print "[", $r->{studiumsbezeichnung}, "]\n";
  }
}

sub debug_stkz
{
  my $epr= get_eprints_db($cnf);

  my $epr_db= $epr->connect();
  my @col_names_db= qw( eprintid studienkennzahl matr );

  my $search_term= "studienkennzahl IS NOT NULL AND eprint_status IN ('archive', 'buffer')";
  my $keys= $epr_db->get_all_x('eprint', [$search_term], join(',', @col_names_db));

  my %stkz;
  my %stcd;
  my $error_count= 0;
  foreach my $key (keys %$keys)
  {
    my $r= $keys->{$key};
    # print __LINE__, " key=[$key] ", Dumper($r);
    my ($id, $stkz, $matr)= map { $r->{$_} } @col_names_db;
    push (@{$stkz{$stkz}}, $id);

    my ($errors, $warnings, $study_id, $studies_codes)= get_study_id($matr, $stkz);
    my $stcd= join(':', map { $studies_codes->{$_} } qw(university_code code_1 code_2 code_3 cooperation_code));
    # print __LINE__, " id=[$id] stcd=[$stcd] ", Dumper($studies_codes);

    if (@$errors)
    {
      print __LINE__, " id=[$id] errors: ", Dumper($errors);
      $error_count++;
    }

    push (@{$stcd{$stcd}}, $id);
  }

  print '='x72, "\n";
  print __LINE__, " error_count=[$error_count]\n";
  foreach my $stkz (sort keys %stkz)
  {
    print join("\t", '['.$stkz.']', scalar @{$stkz{$stkz}}), "\n";
  }
  print '='x72, "\n";
  foreach my $stcd (sort keys %stcd)
  {
    print join("\t", $stcd, scalar @{$stcd{$stcd}}), "\n";
  }
}

sub get_redis_db
{
  my $ctx_cnf= shift;

  if (exists ($ctx_cnf->{redis}))
  {
    my $redis_connection= new Redis(server => $ctx_cnf->{redis}->{server});
    $redis_connection->select($ctx_cnf->{redis}->{database});
    print __LINE__, " connected to redis database\n";
    print "cnf: ", Dumper($ctx_cnf->{redis});
    print "con: ", Dumper($redis_connection);
    # sleep(30);
    return $redis_connection;
  }

  undef;
}

=head2 export_redirect

export redis rediction data

=cut

sub export_redirect
{
  # $cnf is global
  my $ctx_cnf= $cnf->{$ot2ut_context};

  my $epr= get_eprints_db($cnf);

  my $epr_db= $epr->connect();
  my @col_names= qw( eprintid lastmod_year lastmod_month lastmod_day lastmod_hour lastmod_minute lastmod_second );
  my $search_term= "eprint_status IN ('archive', 'buffer')";
  my $keys= $epr_db->get_all_x('eprint', [$search_term], join(',', @col_names));
  my @eprint_ids_in_mysql= keys %$keys; # mongodb stores eprint_id as string

  $db_ot2ut= IRMA::db::get_any_db($cnf, 'ot2ut_database') unless (defined ($db_ot2ut));
  my $col_sync= $db_ot2ut->get_collection('sync');
  my $cur_sync= $col_sync->find({ context => $ot2ut_context });

  my ($eprint_redirect_base_url, $utheses_public_base_url)= map { $ctx_cnf->{$_} } qw(eprint_redirect_base_url utheses_public_base_url);

  my @eprint_ids;
  my $max_eprint_id= 0;
  foreach my $eprint_id (@eprint_ids_in_mysql)
  {
    $eprint_ids[$eprint_id]= $eprint_redirect_base_url . $eprint_id  . '/';
    $max_eprint_id= $eprint_id if ($eprint_id > $max_eprint_id);
  }

  while ($running)
  {
    my $row_sync= $cur_sync->next();
    last unless (defined ($row_sync));
    # print __LINE__, " row_sync: ", Dumper($row_sync); last;

    my ($eprint_id, $utheses_id)= map { $row_sync->{$_} } qw(eprint_id utheses_id);

    my $ut_url= $utheses_public_base_url . $utheses_id . '#';

    $eprint_ids[$eprint_id]= $ut_url;
    $max_eprint_id= $eprint_id if ($eprint_id > $max_eprint_id);
  }

  my $fnm_redis= 'utheses_redirect_' . $ot2ut_context .'.redis';
  open (REDIS, '>:utf8', $fnm_redis) or die "can't write to [$fnm_redis]";
  print REDIS "FLUSHDB\n";
  my $cnt= 0;
  for(my $i= 0; $i <= $max_eprint_id; $i++)
  {
    my $ut_url= $eprint_ids[$i];

    if ($ut_url)
    {
      printf REDIS ("set %d %s\n", $i, $ut_url);
      $cnt++;
    }
  }
  close (REDIS);

  print __LINE__, " export_redirect: exported $cnt redirects, max_eprint_id= $max_eprint_id to $fnm_redis\n";
  ($cnt, $max_eprint_id, $fnm_redis);
}

sub update_policies
{
  my @upd_eprint_ids= @_;

  my %upd_eprint_ids= map { $_ => 1 } @upd_eprint_ids;

  %doc_embargo_dates= (); # global variable, clear hash from data from previous runs

  my $epr= get_eprints_db($cnf);
  # print "epr: ", Dumper ($epr);

  activity({ activity => 'update_policies' });

  my $epr_db= $epr->connect();
  # print "epr_db: ", Dumper ($epr_db);

  $db_ot2ut= IRMA::db::get_any_db($cnf, 'ot2ut_database') unless (defined ($db_ot2ut));
  my $col_utp= $db_ot2ut->get_collection('utheses.policies');

  if (defined ($refresh_oldest_policies) && $refresh_oldest_policies > 0 && $refresh_oldest_policies <= 10000)
  {
    my $c1= $col_utp->find( {}, { '_id' => 1, eprint_id => 1, generated => 1 });
    $c1->sort( { generated => 1 } );
    $c1->limit( $refresh_oldest_policies );
    my @old= $c1->all();
    foreach my $old (@old) { $upd_eprint_ids{$old->{eprint_id}}= 1; }
  }

  my @col_names= qw( eprintid lastmod_year lastmod_month lastmod_day lastmod_hour lastmod_minute lastmod_second );
  # $epr_db->show_query(1);
  my $search_term= "eprint_status IN ('archive', 'buffer')";
  my $keys= $epr_db->get_all_x('eprint', [$search_term], join(',', @col_names));
  my @eprint_ids_in_mysql= map { $_."" } sort { $a <=> $b } keys %$keys; # mongodb stores eprint_id as string

  my $ts_start= Util::ts::ts_ISO_gmt(time());

  my ($cnt_updated, $cnt_inserted, $cnt_unchanged)= (0, 0, 0, 0);
  my (@lst_updated, @lst_inserted, @lst_unchanged);

  foreach my $eprint_id (@eprint_ids_in_mysql)
  {
    last unless ($running);
    
    my $x1= $keys->{$eprint_id};
    my $x1_lastmod= sprintf("%4d-%02d-%02dT%02d%02d%02d", map { $x1->{$_} } qw(lastmod_year lastmod_month lastmod_day lastmod_hour lastmod_minute lastmod_second));
    # print __LINE__, " x1: ", Dumper($x1);

    my $utp_info= $col_utp->find_one({ eprint_id => $eprint_id });

    if (!exists ($upd_eprint_ids{$eprint_id}) && defined ($utp_info) && $utp_info->{lastmod} eq $x1_lastmod)
    {
      print __LINE__, " NOTE: already processed, no change: x1_lastmod=[$x1_lastmod] eprint_id=[$eprint_id]\n";
      $cnt_unchanged++;
      push (@lst_unchanged, $eprint_id);
      next;
    }
    print __LINE__, " eprint_id=[$eprint_id] x1_lastmod=[$x1_lastmod]\n";

    print __LINE__, ' ', '='x70, "\n";
    my @res= generate_utheses_metadata($epr, $eprint_id);
    if (@res < 9)
    {
      print __LINE__, " something went wrong, not enough results: ", Dumper(\@res);
      return undef;
    }
    my ($errors, $warnings, $row, $lastmod, $ut, $utheses_json_path, $files, $docs, $utheses_upload_result_json_path)= @res;

    my $show= $docs->{show};

    my $cnt_errors=   @$errors;
    my $cnt_warnings= @$warnings;

    my $full_data=
    {
      eprint_id     => $eprint_id,
      lastmod       => $lastmod,
      date_sperre   => get_othes_timestamp($row, 'date_sperre'),
      generated     => Util::ts::ts_ISO_gmt(time()),

      ut_public => $ut->{public},
      docs      => $docs,
      files     => $files,

      cnt_errors   => $cnt_errors,   errors   => $errors,
      cnt_warnings => $cnt_warnings, warnings => $warnings,

      utheses_json_path               => $utheses_json_path,
      utheses_upload_result_json_path => $utheses_upload_result_json_path,
    };

    map { $full_data->{$_}= $row->{$_} } qw(eprint_status einverstaendnis sperre full_text_status urn doi ac_nummer);

    # $show++ if ($docs->{cnt_embargo} > 0);
    # $show++ if ($docs->{cnt_public} > 0 && $docs->{cnt_restricted} > 0);

    if ($show)
    {
      # print __LINE__, " documents: eprint_id=[$eprint_id] show=[$show] ", Dumper($docs);
      print __LINE__, " full_data: eprint_id=[$eprint_id] show=[$show] ", Dumper($full_data);
    }

    if (defined ($utp_info))
    {
      my $rc_upd= $col_utp->update( { _id => $utp_info->{_id} }, $full_data);
      $cnt_updated++;
      push (@lst_updated, $eprint_id);

      print __LINE__, " rc_upd: ", Dumper($rc_upd);
    }
    else
    {
      my $rc_ins= $col_utp->insert( $full_data);
      $cnt_inserted++;
      push (@lst_inserted, $eprint_id);

      print __LINE__, " rc_ins: ", Dumper($rc_ins);
    }
    print __LINE__, ' ', '='x70, "\n";
  }

  my @removed_from_othes= $col_utp->find( { eprint_id => { '$nin' => \@eprint_ids_in_mysql } }, { '_id' => 1, eprint_id => 1 })->all();
  print __LINE__, " removed from othes ", Dumper(\@removed_from_othes);
  my @eprint_id_removed_from_othes= map { $_->{eprint_id} } @removed_from_othes;

  my %stats=
  (
    agent         => 'update_policies',
    ts_start      => $ts_start,
    ts_finish     => Util::ts::ts_ISO_gmt(time()),

    cnt_inserted  => $cnt_inserted,
    cnt_updated   => $cnt_updated,
    cnt_unchanged => $cnt_unchanged,
    lst_inserted  => \@lst_inserted,
    lst_updated   => \@lst_updated,
    lst_unchanged => \@lst_unchanged,
    removed_from_othes => \@eprint_id_removed_from_othes,
  );

  my $col_stats= $db_ot2ut->get_collection('statistics');
  $col_stats->insert( \%stats );

  $stats{lst_unchanged}= '<deleted>';
  print __LINE__, " stats: ", Dumper(\%stats);

  print __LINE__, " embargo dates: ", Dumper(\%doc_embargo_dates);

  return {};
}

sub policies_stats
{
  my $msg= shift;

  %doc_embargo_dates= (); # global variable, clear hash from data from previous runs

  $db_ot2ut= IRMA::db::get_any_db($cnf, 'ot2ut_database') unless (defined ($db_ot2ut));
  activity({ activity => 'policies_stats' });

  # prepare: get info from sync database
  my $col_sync= $db_ot2ut->get_collection('sync');
  my $cur_sync= $col_sync->find({});

  my %synced;
  my (@upload_errors, %upload_errors);
  my (%totals, @blocks, @metablocks, @eprint_ids);
  while ($running)
  {
    my $row_sync= $cur_sync->next();
    last unless (defined ($row_sync));

    # print __LINE__, " row_sync: ", Dumper($row_sync); last;
    my ($eprint_id, $lastmod, $context, $utheses_id, $error_code)= map { $row_sync->{$_} } qw(eprint_id lastmod context utheses_id error_code);

       if ($context eq 'ot2ut-test') { $context= 'test'; }
    elsif ($context eq 'ot2ut-entw') { $context= 'entw'; }
    elsif ($context eq 'ot2ut-prod') { $context= 'prod'; }
    elsif ($context =~ /deleted/) {} # ignore
    else { die ("invalid context $context"); } # ... this should not happen, unless someone writes an undefined context into the sync collection

    my $block_nr=     int($eprint_id/100);
    my $metablock_nr= int($block_nr/100);
    $totals{$context}->{cnt_total}++;
    $blocks[$block_nr]->{$context}->{cnt_total}++;
    $metablocks[$metablock_nr]->{$context}->{cnt_total}++;

    $eprint_ids[$eprint_id]->{$context}= $row_sync;

    if (defined ($utheses_id) && $error_code eq 'ok')
    {
      $synced{$eprint_id}->{$context}= [ $lastmod, $utheses_id ];

      $totals{$context}->{cnt_ok}++;
      $blocks[$block_nr]->{$context}->{cnt_ok}++;
      $metablocks[$metablock_nr]->{$context}->{cnt_ok}++;
    }
    else
    {
      push (@{$upload_errors{$error_code}}, $eprint_id);

      $totals{$context}->{cnt_error}++;
      $blocks[$block_nr]->{$context}->{cnt_error}++;
      $metablocks[$metablock_nr]->{$context}->{cnt_error}++;
    }
  }

  # MAIN PART: analyze othes policies collection
  my $col_utp= $db_ot2ut->get_collection('utheses.policies');
  my $cur_utp= $col_utp->find({});
  
  my @columns= qw(eprint_status einverstaendnis sperre hds fts docs pub restr embargo errors);
  if ($flag_add_utheses_policies)
  {
    push (@columns, qw(lrq lst kwl absl ftl));
  }
  if ($flag_add_identifiers)
  {
    push (@columns, qw(urn doi));
  }

  my $cctab= new cctab(columns => \@columns, base_path => $base_path);

  my $max; #= 1000;
  my @contexts= qw(entw test prod);
  my (%cnt_errors, %cnt_warnings, %all_errors);
  my @lst_nonpublic_doc_first; # list of objects with cnt_public > 1 && cnt_restricted > 1 && first document is not public
  my @lst_docs_with_notes;
  my %docs_with_holes;
  while ($running)
  {
    my $row_utp= $cur_utp->next();
    last unless (defined ($row_utp));
    # print __LINE__, " row_utp: ", Dumper($row_utp); last;

    my ($eprint_id, $eprint_status, $date_sperre, $einverstaendnis, $lastmod, $full_text_status, $sperre, $cnt_errors, $cnt_warnings, $errors, $warnings, $docs, $utp, $urn, $doi, $ac_number)=
       map { $row_utp->{$_} } qw(eprint_id eprint_status date_sperre einverstaendnis lastmod full_text_status sperre cnt_errors cnt_warnings errors warnings docs ut_public urn doi ac_nummer);

    $eprint_id += 0;
    my $block_nr=     int($eprint_id/100);
    my $metablock_nr= int($block_nr/100);
    $totals{othes}->{cnt_total}++;
    $blocks[$block_nr]->{othes}->{cnt_total}++;
    $metablocks[$metablock_nr]->{othes}->{cnt_total}++;

    $eprint_ids[$eprint_id]->{othes}= $lastmod;

    my ($cnt_docs, $cnt_public, $cnt_restricted, $cnt_embargo)=
       map { $docs->{$_} } qw(cnt_docs cnt_public cnt_restricted cnt_embargo);

    push (@lst_nonpublic_doc_first, $eprint_id) if ($cnt_public > 1 && $cnt_restricted > 1 && $docs->{documents}->[0]->{security} ne 'public');
    push (@lst_docs_with_notes, $eprint_id) if (@{$docs->{notes}} > 0);
    
    foreach my $doc (@{$docs->{documents}})
    {
      unless (defined ($doc))
      {
        $docs_with_holes{$eprint_id}++;
        next;
      }

      if (exists ($doc->{date_embargo}))
      {
        my $d= $doc->{date_embargo};
        if ($d eq '2099-12-31T00:00:00Z') { $doc_embargo_dates{$d}++; } else { push (@{$doc_embargo_dates{$d}}, $eprint_id); }
      }
    }

    my $has_errors= ($cnt_errors > 0) ? 'yes' : 'no';
    my $has_date_sperre= 'no';
    if (defined ($date_sperre))
    {
      my $ts_now= Util::ts::ts_ISO3_gmt(time());
      $has_date_sperre=($date_sperre lt $ts_now) ? 'past' : 'future';
    }

    my @bucket_selectors= ($eprint_status, $einverstaendnis, $sperre, $has_date_sperre, $full_text_status, $cnt_docs, $cnt_public, $cnt_restricted, $cnt_embargo, $has_errors);

    my ($utptp, $lrq, $lst, $kwl, $absl, $ftl);
    my $utpt= $utp->{thesis};
    if (defined ($utpt))
    {
      ($utptp, $urn, $doi)= map { $utpt->{$_} } qw(policies urn doi);
    }

    if (defined ($utptp))
    {
      ($lrq, $lst, $kwl, $absl, $ftl)= map { $utptp->{$_} } qw(lock_request lock_status keywords_locked abstract_locked fulltext_locked);
    }

    if ($flag_add_utheses_policies)
    {
      push (@bucket_selectors, $lrq, $lst, $kwl, $absl, $ftl);
    }

    if ($flag_add_identifiers)
    {
      $urn= 'present' if (defined ($urn));
      $doi= 'present' if (defined ($doi));

      push (@bucket_selectors, $urn, $doi);
    }

    my $bucket= $cctab->bucket(1, @bucket_selectors);

    my $info= { ac_number => $ac_number };
    if (defined ($errors) && @$errors)
    {
      $info->{errors}= $errors;
      foreach my $error (@$errors)
      {
        my $error_name= $error->{error};
        $error_name= 'bad_name_buffer' if ($error_name eq 'bad_name' && $eprint_status eq 'buffer');
        $cnt_errors{$error_name}++;

        $error->{eprint_id}= "$eprint_id";
        push (@{$all_errors{$error_name}}, $error);
      }
    }
    if (defined ($warnings) && @$warnings)
    {
      $info->{warnings}= $warnings;
      foreach my $warning (@$warnings) { $cnt_warnings{$warning->{warning}}++; }
    }

    $bucket->{othes}++;
    $bucket->{ids}->{$eprint_id}= $info;

    if (exists ($synced{$eprint_id}))
    {
      my $s= $synced{$eprint_id};
      # print __LINE__, " eprint_id=[$eprint_id]; synced: ", Dumper ($s);
      foreach my $context (@contexts)
      {
        if (exists ($s->{$context}))
        {
          my $x= $s->{$context};
          $bucket->{$context}++;
          # push (@{$bucket->{$context . '_ids'}} => [ $eprint_id, $x->[1] ]);
          $bucket->{ids}->{$eprint_id}->{$context}= $x->[1];
        }
      }
      delete($synced{$eprint_id});
    }

    last if (defined($max) && --$max <= 0);
  }

# print __LINE__, " working: cctab: ", Dumper($cctab);
 { # BEGIN annotations
  my @s1= qw(archive FALSE FALSE no restricted 1 1 0 0 no);
  push (@s1, qw(0 0 0 0 1))    if ($flag_add_utheses_policies);
  push (@s1, qw(present NULL)) if ($flag_add_identifiers);
  my $b1= $cctab->bucket(0, @s1);
  if (defined ($b1))
  {
    $b1->{annotation}= { bgcolor => 'pink', note => 'restricted aber public doc' };
    # print __LINE__, " special bucket found: ", Dumper($b1);
  }

  my @s2= qw(archive FALSE FALSE no public 1 1 0 0 no);
  push (@s2, qw(0 0 0 0 0))    if ($flag_add_utheses_policies);
  push (@s2, qw(present NULL)) if ($flag_add_identifiers);
  my $b2= $cctab->bucket(0, @s2);
  if (defined ($b2))
  {
    $b2->{annotation}= { bgcolor => 'pink', note => 'einverstaendnis FALSE aber public' };
    # print __LINE__, " special bucket found: ", Dumper($b2);
  }

  my @s2b= qw(archive TRUE FALSE no public 1 1 0 0 no);
  push (@s2b, qw(0 0 1 1 0))    if ($flag_add_utheses_policies);
  push (@s2b, qw(present NULL)) if ($flag_add_identifiers);
  my $b2b= $cctab->bucket(0, @s2b);
  if (defined ($b2b))
  {
    $b2b->{annotation}= { bgcolor => 'pink', note => 'abstract und keywords locked, aber pdf public; bekommt keine DOI' };
    # print __LINE__, " special bucket found: ", Dumper($b2b);
  }

  my @s2c= qw(archive TRUE NULL no public 1 1 0 0 no);
  push (@s2c, qw(0 0 1 1 0))    if ($flag_add_utheses_policies);
  push (@s2c, qw(present NULL)) if ($flag_add_identifiers);
  my $b2c= $cctab->bucket(0, @s2c);
  if (defined ($b2c))
  {
    $b2c->{annotation}= { bgcolor => 'pink', note => 'abstract und keywords locked, aber pdf public; bekommt keine DOI' };
    # print __LINE__, " special bucket found: ", Dumper($b2c);
  }
  # ZZZZ

  my @s3= qw(archive TRUE FALSE no restricted 1 1 0 0 no);
  push (@s3, qw(0 0 0 0 1))    if ($flag_add_utheses_policies);
  push (@s3, qw(present NULL)) if ($flag_add_identifiers);
  my $b3= $cctab->bucket(0, @s3);
  if (defined ($b3))
  {
    $b3->{annotation}= { bgcolor => 'pink', note => 'einverstaendnis TRUE aber restricted mit public document' };
    # print __LINE__, " special bucket found: ", Dumper($b3);
  }

  my @s4a= qw(archive TRUE NULL no public 1 1 0 0 no);
  push (@s4a, qw(0 0 0 0 0))       if ($flag_add_utheses_policies);
  push (@s4a, qw(present present)) if ($flag_add_identifiers);
  my $b4a= $cctab->bucket(0, @s4a);
  if (defined ($b4a))
  {
    $b4a->{annotation}= { bgcolor => 'yellow', note => 'sperre NULL? viele errors und warnings; wo kommen diese Daten her?' };
    # print __LINE__, " special bucket found: ", Dumper($b4a);
  }

  my @s4b= qw(archive TRUE NULL no public 1 1 0 0 yes);
  push (@s4b, qw(0 0 0 0 0)) if ($flag_add_utheses_policies);
  push (@s4b, qw(NULL NULL)) if ($flag_add_identifiers);
  my $b4b= $cctab->bucket(0, @s4b);
  if (defined ($b4b))
  {
    $b4b->{annotation}= { bgcolor => 'yellow', note => 'sperre NULL? viele errors und warnings; wo kommen diese Daten her?' };
    # print __LINE__, " special bucket found: ", Dumper($b4b);
  }

  my @s4c= qw(archive TRUE NULL no public 1 1 0 0 yes);
  push (@s4c, qw(0 0 0 0 0))       if ($flag_add_utheses_policies);
  push (@s4c, qw(present present)) if ($flag_add_identifiers);
  my $b4c= $cctab->bucket(0, @s4c);
  if (defined ($b4c))
  {
    $b4c->{annotation}= { bgcolor => 'yellow', note => 'sperre NULL? viele errors und warnings; wo kommen diese Daten her?' };
    # print __LINE__, " special bucket found: ", Dumper($b4c);
  }

  my @s5a= qw(archive TRUE FALSE no public 1 1 0 0 no);
  push (@s5a, qw(0 0 0 0 0))       if ($flag_add_utheses_policies);
  push (@s5a, qw(present present)) if ($flag_add_identifiers);
  my $b5a= $cctab->bucket(0, @s5a);
  if (defined ($b5a))
  {
    $b5a->{annotation}= { bgcolor => 'lightgreen', note => 'the good ones' };
    # print __LINE__, " special bucket found: ", Dumper($b5a);
  }

  my @s5b= qw(archive TRUE FALSE no public 1 1 0 0 no);
  push (@s5b, qw(0 0 0 0 0))    if ($flag_add_utheses_policies);
  push (@s5b, qw(present NULL)) if ($flag_add_identifiers);
  my $b5b= $cctab->bucket(0, @s5b);
  if (defined ($b5b))
  {
    $b5b->{annotation}= { bgcolor => 'lightgreen', note => 'the good ones; DOI wird nachgereicht' };
    # print __LINE__, " special bucket found: ", Dumper($b5b);
  }

  my @s5c= qw(archive TRUE FALSE no public 1 1 0 0 no);
  push (@s5c, qw(0 0 0 0 0)) if ($flag_add_utheses_policies);
  push (@s5c, qw(NULL NULL)) if ($flag_add_identifiers);
  my $b5c= $cctab->bucket(0, @s5c);
  if (defined ($b5c))
  {
    $b5c->{annotation}= { bgcolor => 'lightgreen', note => 'the good ones; URN fehlt noch, DOI wird nachgereicht' };
    # print __LINE__, " special bucket found: ", Dumper($b5c);
  }

  my @s6a= qw(archive FALSE FALSE no restricted 1 0 1 1 no);
  push (@s6a, qw(0 0 0 0 1))    if ($flag_add_utheses_policies);
  push (@s6a, qw(present NULL)) if ($flag_add_identifiers);
  my $b6a= $cctab->bucket(0, @s6a);
  if (defined ($b6a))
  {
    $b6a->{annotation}= { bgcolor => 'lightblue', note => 'keine Volltextfreigabe; NBN vergeben' };
    # print __LINE__, " special bucket found: ", Dumper($b6a);
  }

  my @s6a2= qw(archive FALSE FALSE no restricted 1 0 1 1 no);
  push (@s6a2, qw(0 0 0 0 1))    if ($flag_add_utheses_policies);
  push (@s6a2, qw(NULL NULL)) if ($flag_add_identifiers);
  my $b6a2= $cctab->bucket(0, @s6a2);
  if (defined ($b6a))
  {
    $b6a2->{annotation}= { bgcolor => 'lightblue', note => 'kein Volltext; NBN noch nicht vergeben' };
    # print __LINE__, " special bucket found: ", Dumper($b6a2);
  }

  my @s6b= qw(archive FALSE FALSE no restricted 1 0 1 1 no);
  push (@s6b, qw(0 0 0 0 1))         if ($flag_add_utheses_policies);
  push (@s6b, qw(present present)) if ($flag_add_identifiers);
  my $b6b= $cctab->bucket(0, @s6b);
  if (defined ($b6b))
  {
    $b6b->{annotation}= { bgcolor => '#33a2ff', note => 'Volltext gesperrt; DOI wurde offenbar vorher vergeben' };
    # print __LINE__, " special bucket found: ", Dumper($b6b);
  }

  my @s6c= qw(archive FALSE FALSE no restricted 1 0 1 0 no);
  push (@s6c, qw(0 0 0 0 1))    if ($flag_add_utheses_policies);
  push (@s6c, qw(present NULL)) if ($flag_add_identifiers);
  my $b6c= $cctab->bucket(0, @s6c);
  if (defined ($b6c))
  {
    $b6c->{annotation}= { bgcolor => 'pink', note => 'Volltext restricted, kein Embargo' };
    # print __LINE__, " special bucket found: ", Dumper($b6b);
  }
 } # END annotations

  my $bucket_cnt= $cctab->show_tsv(['othes', @contexts], 'counters.tsv');

  my $now= scalar localtime(time());
  my $msg_html= 'update';
  my %stale_uploads= ();
  my $idx_html= join('/', $base_path, 'index.html');
  open(IDX, '>:utf8', $idx_html);
  print IDX <<"EOX";
<html>
<head>
<meta charset="UTF-8" />
<meta http-equiv="refresh" content="300" />
<title>othes to utheses migration statistics</title>
<style>
  td { text-align:right; }
</style>
</head>
<body>
<h1>othes to utheses transfer statistics</h1>
<p>last refreshed: $now</p>
<p>Status of transfers from othes to various utheses instances.</p>
<p><font color="red">$msg</font></p>
<p><a href="buckets.html" target="buckets">$bucket_cnt buckets</a></p>
<h2>upload counters</h2>
<table border="1">
<tr>
  <th>metablock</th>
  <th width="100px">othes</th>
  <th colspan="3" width="200px">entw</th>
  <th colspan="3" width="200px">test</th>
  <th colspan="3" width="200px">prod</th>
</tr>
<tr>
  <th>number</th>
  <th>total</th>
  <th>count</th><th>pct</th><th>err</th>
  <th>count</th><th>pct</th><th>err</th>
  <th>count</th><th>pct</th><th>err</th>
</tr>
EOX
# <p><a href="http://xx2.test.univie.ac.at:3001/html/metablocks" target="opa">Live Counter</a></p>

  my %incomplete_blocks= ();

  my $col_blk=  $db_ot2ut->get_collection('blocks');
  for (my $metablock_nr= 0; $metablock_nr <= $#metablocks; $metablock_nr++)
  {
    my $mb_othes= $metablocks[$metablock_nr]->{othes}->{cnt_total};

    my $mb_html= sprintf("metablock_%d.html", $metablock_nr);
    my $mb_html_fp= join('/', $base_path, $mb_html);

    print IDX "<tr>\n  <td><a href=\"$mb_html\" target=\"metablocks\">", $metablock_nr, " (${metablock_nr}yyxx)</a></td>\n  <td>", $mb_othes, "</td>\n";

    $col_blk->update({ nr => $metablock_nr, level => 1, context => 'othes' },
                     { nr => $metablock_nr, level => 1, context => 'othes', count_ok => $mb_othes, count_errors => 0 },
                     { upsert => 1 });

    foreach my $context (qw(entw test prod))
    {
      my $c= $metablocks[$metablock_nr]->{$context}->{cnt_ok} || 0;
      my $e= $metablocks[$metablock_nr]->{$context}->{cnt_error} || 0;

      my $pct= $c*100.0/$mb_othes;
      my $ck1= ($pct == 100.0) ? 'lightgreen' : 'lightblue';
      my $ck2= ($e == 0) ? 'lightgreen' : 'lightpink';

      printf IDX ("  <td bgcolor=\"$ck1\">%d</td><td bgcolor=\"$ck1\">%7.4f %%</td><td bgcolor=\"$ck2\">%d</td>\n", $c, $pct, $e);

      $col_blk->update({ nr => $metablock_nr, level => 1, context => $context },
                       { nr => $metablock_nr, level => 1, context => $context, count_ok => $c, count_errors => $e },
                       { upsert => 1 });
    }
    print IDX "</tr>\n";

    open (MB, '>:utf8', $mb_html_fp);
    print MB <<"EOX";
<html>
<head>
<meta charset="UTF-8" />
<meta http-equiv="refresh" content="300" />
<title>othes to utheses migration metablock $metablock_nr</title>
<style>
  td { text-align:right; }
</style>
</head>
<body>
<table border="1">
<tr>
  <th>block</th>
  <th width="100px">othes</th>
  <th colspan="3" width="200px">entw</th>
  <th colspan="3" width="200px">test</th>
  <th colspan="3" width="200px">prod</th>
</tr>
<tr>
  <th>number</th>
  <th>total</th>
  <th>count</th><th>pct</th><th>err</th>
  <th>count</th><th>pct</th><th>err</th>
  <th>count</th><th>pct</th><th>err</th>
</tr>
EOX

    my $block_start= $metablock_nr*100;
    my $block_last= ($metablock_nr == 6) ? $MAX_BLOCK : $block_start+99;
    for (my $block_nr= $block_start; $block_nr <= $block_last; $block_nr++)
    {
      next unless (defined ($blocks[$block_nr]));

      my $block= $blocks[$block_nr];

      my $b_html= sprintf("block_%d.html", $block_nr);
      my $b_html_fp= join('/', $base_path, $b_html);

      my $b_othes= $block->{othes}->{cnt_total};
      print MB "<tr>\n  <td><a href=\"$b_html\" target=\"blocks\">", $block_nr, " (${block_nr}xx)</a></td>\n  <td>", $b_othes, "</td>\n";

      $col_blk->update({ nr => $block_nr, level => 0, context => 'othes' },
                       { nr => $block_nr, level => 0, context => 'othes', count_ok => $b_othes, count_errors => 0 },
                       { upsert => 1 });

      foreach my $context (qw(entw test prod))
      {
        my $c= $block->{$context}->{cnt_ok} || 0;
        my $e= $block->{$context}->{cnt_error} || 0;
        my $pct= $c*100.0/$b_othes;
        my $ck2= ($e == 0) ? 'lightgreen' : 'pink';
        my $ck1;

        if ($pct == 100.0)
        {
          $ck1= 'lightgreen'
        }
        elsif ($pct >= 100.0)
        { # uploaded objects which were deleted from othes in the meantime
          $ck1= 'pink';
        }
        else
        {
          $ck1= 'lightblue';
          push (@{$incomplete_blocks{$context}}, $block_nr);
        }

        printf MB ("  <td bgcolor=\"$ck1\">%d</td><td bgcolor=\"$ck1\">%7.4f %%</td><td bgcolor=\"$ck2\">%d</td>\n", $c, $pct, $e);

        $col_blk->update({ nr => $block_nr, level => 0, context => $context },
                         { nr => $block_nr, level => 0, context => $context, count_ok => $c, count_errors => $e },
                         { upsert => 1 });
      }
      print MB "</tr>\n";

      open (BLOCK, '>:utf8', $b_html_fp);
      print BLOCK <<"EOX";
<html>
<head>
<meta charset="UTF-8" />
<meta http-equiv="refresh" content="300" />
<title>othes to utheses migration block $block_nr</title>
<style>
  td { text-align:right; }
</style>
</head>
<body>
<p>last refreshed: $now</p>
<table border="1">
<tr>
  <th x_width="100px" colspan="2" width="25%">othes</th>
  <th x_width="100px" colspan="2" width="25%">entw</th>
  <th x_width="100px" colspan="2" width="25%">test</th>
  <th x_width="100px" colspan="2" width="25%">prod</th>
</tr>
<tr>
  <th>eprint_id</th><th>last_mod</th>
  <th>uth_id</th><th>upload</th>
  <th>uth_id</th><th>upload</th>
  <th>uth_id</th><th>upload</th>
</tr>
EOX

      my $items= $block->{items};
      my $first= $block_nr*100;
      my $last= $first+99;
      for (my $eprint_id= $first; $eprint_id <= $last; $eprint_id++)
      {
        my $x= $eprint_ids[$eprint_id];
        next unless (defined ($x));

        my $lastmod= $x->{othes};
        print BLOCK <<EOX;
<tr>
  <td><a href="https://othes-legacy.univie.ac.at/$eprint_id/" target="othes">$eprint_id</a>
  <td>$lastmod</td>
</td>
EOX

        foreach my $context (qw(entw test prod))
        {
          unless (exists ($x->{$context}))
          {
            print BLOCK "  <td>&nbsp;</td><td>&nbsp;</td>\n";
            next;
          }

          my $c= $x->{$context};
          my ($error_code, $utheses_id, $errors, $ts_upload)= map { $c->{$_} } qw(error_code utheses_id errors ts_upload);
          if ($error_code eq 'ok')
          {
            my $link;

               if ($context eq 'test') { $link= 'https://utheses-frontend.ctest.univie.ac.at/client/#/detail/' . $utheses_id . '/'; }
            elsif ($context eq 'entw') { $link= 'https://utheses-frontend-entw-utheses.ctest.univie.ac.at/#/detail/' . $utheses_id . '/'; }
          # elsif ($context eq 'prod') { $link= 'https://utheses-admin-ui-utheses-prod.cprod.univie.ac.at/client#/view/document/utheses/' . $utheses_id . '/'; }
            elsif ($context eq 'prod') { $link= 'https://utheses-admin-ui-utheses-prod.cprod.univie.ac.at/client#/detail/' . $utheses_id . '/'; }

            my $ts_color= 'lightgreen';
            if ($ts_upload lt $lastmod) { $ts_color= 'lightpink'; push (@{$stale_uploads{$context}}, [ $eprint_id, $utheses_id ]); }
            print BLOCK <<EOX;
  <td bgcolor="lightgreen"><a href="$link" target="$context">$utheses_id</a></td>
  <td bgcolor="$ts_color">$ts_upload</td>
EOX
          }
          else
          {
            print BLOCK "  <td bgcolor=\"lightpink\">$error_code ", Dumper($errors), "</td><td>$ts_upload</td>\n";
          }
        }
        print BLOCK "</tr>\n";
      }

      print BLOCK <<"EOX";
</table>
</body>
</html>
EOX

      close (BLOCK);
    }

    print MB <<"EOX";
</table>
</body>
</html>
EOX
    close (MB);

  }

  # print summaries
  my $total_othes= $totals{othes}->{cnt_total};
  print IDX "<tr>\n  <td>total</td>\n  <td>", $total_othes, "</td>\n";
  foreach my $context (qw(entw test prod))
  {
    my $c= $totals{$context}->{cnt_ok};
    my $e= $totals{$context}->{cnt_error};
    my $pct= ($total_othes == 0) ? 0 : $c*100.0/$total_othes;
    my $ck1= ($pct == 100.0) ? 'lightgreen' : 'lightblue';
    my $ck2= ($e == 0) ? 'lightgreen' : 'lightpink';
    printf IDX ("  <td bgcolor=\"$ck1\">%d</td><td bgcolor=\"$ck1\">%7.4f %%</td><td bgcolor=\"$ck2\">%d</td>\n", $c, $pct, $e);
  }
  print IDX "</tr>\n";

  print IDX "<tr>\n  <td>missing</td>\n  <td>&nbsp;</td>\n";
  foreach my $context (qw(entw test prod))
  {
    my $m= $total_othes - $totals{$context}->{cnt_ok};
    my $pct= ($total_othes == 0) ? 0 : $m*100.0/$total_othes;
    my $ck1= ($m == 0) ? 'lightgreen' : 'lightblue';
    printf IDX ("  <td bgcolor=\"$ck1\">%d</td><td bgcolor=\"$ck1\">%7.4f %%</td><td bgcolor=\"$ck1\">&nbsp;</td>\n", $m, $pct);
  }
  print IDX "</tr>\n";

  print IDX <<EOX;
</table>

<h2>migration tables</h2>
<ul>
<li><a href="sync_ot2ut-entw.tsv">entw</a></li>
<li><a href="sync_ot2ut-test.tsv">test</a></li>
<li><a href="sync_ot2ut-prod.tsv">prod</a></li>
</ul>

EOX

  # print statistics
  # print __LINE__, " cctab: ", Dumper($cctab);
  
  my @stale_contexts= sort keys %stale_uploads;
  if (@stale_contexts)
  {
    print IDX <<EOX;
<h2>stale uploads</h2>
<p>Objects, which were updated on othes after the upload to the target instance.</p>
EOX

    # my %stale_count;
    foreach my $ctx (@stale_contexts)
    {
      my $stu= $stale_uploads{$ctx};
      print IDX "<h3>stale uploads context $ctx</h3>\n";
      print IDX "<p>count: ", scalar(@$stu), "</p>\n";
      my $last_block= -1;
      foreach my $stale_object (@$stu)
      {
        my ($eprint_id, $utheses_id)= @$stale_object;
        my $block= int($eprint_id/100);
        if ($block != $last_block)
        {
          print IDX "</p>\n" if (defined ($last_block));
          print IDX "<p><b><a href=\"block_${block}.html\" target=\"blocks\">Block $block</a></b>:";
          $last_block= $block;
        }
        print IDX ' ', $eprint_id;
        # $stale_count{$ctx}++;
      }
      print IDX "</p>\n" if ($last_block >= 0);
    }
    # print IDX "stale counter: ", join(' ', %stale_count), "\n";

    Util::JSON::write_json_file('stale_uploads.json', \%stale_uploads);
  }

  if (exists ($incomplete_blocks{'entw'}))
  {
    my $ibe= $incomplete_blocks{'entw'};
    print IDX "<h2>incomplete blocks in context entw</h2>\n";
    print IDX join(' ', map { '<a href="http://xx2.test.univie.ac.at:3001/html/block/'.$_.'" target="opa">'.$_.'</a>' } @$ibe), "\n";
  }

  if (exists ($incomplete_blocks{'test'}))
  {
    my $ibt= $incomplete_blocks{'test'};
    print IDX "<h2>incomplete blocks in context test</h2>\n";
    print IDX join(' ', map { '<a href="http://xx2.test.univie.ac.at:3001/html/block/'.$_.'" target="opa">'.$_.'</a>' } @$ibt), "\n";
  }

  if (exists ($incomplete_blocks{'prod'}))
  {
    my $ibp= $incomplete_blocks{'prod'};
    print IDX "<h2>incomplete blocks in context prod</h2>\n";
    print IDX join(' ', map { '<a href="http://xx2.test.univie.ac.at:3001/html/block/'.$_.'" target="opa">'.$_.'</a>' } @$ibp), "\n";
  }

  Util::JSON::write_json_file('incomplete_blocks.json', \%incomplete_blocks);

  print IDX "<h2>errors</h2>\n". Dumper(\%cnt_errors);
  print IDX "<h2>warnings</h2>\n". Dumper(\%cnt_warnings);

  print IDX "<h2>nonpublic_doc_first</h2>\n". Dumper(\@lst_nonpublic_doc_first) if (@lst_nonpublic_doc_first);
  print IDX "<h2>docs with notes</h2>\n". Dumper(\@lst_docs_with_notes) if (@lst_docs_with_notes);
  print IDX "<h2>embargo dates</h2>\n". Dumper(\%doc_embargo_dates);
  print IDX "<h2>errors</h2>\n". Dumper(\%upload_errors);

print IDX <<"EOX";
</body>
</html>
EOX
  close(IDX);

  # show objects which were uploaded to utheses but are no longer present in othes
  my @synced_not_found= sort { $a <=> $b } keys %synced;
  if (@synced_not_found)
  {
    print __LINE__, " ATTN: ", scalar @synced_not_found, " objects synced to utheses but not present at othes:\n", Dumper (\%synced);
  }

  Util::JSON::write_json_file('all_errors.json', \%all_errors);
  # Util::JSON::write_json_file('upload_errors.json', \@upload_errors);
  Util::JSON::write_json_file('docs_with_holes.json', \%docs_with_holes);
}

=head2 export_migration_data

export table with data from utheses migration

=cut

sub export_migration_data
{
  $db_ot2ut= IRMA::db::get_any_db($cnf, 'ot2ut_database') unless (defined ($db_ot2ut));

  my %data;

  # pass 1: get data from sync db
  print __LINE__, " get data from sync db\n";
  my @sync_fields= qw(eprint_id eprint_status utheses_id ts_upload upload_status);
  {
    my $col_sync= $db_ot2ut->get_collection('sync');
    my $cur_sync= $col_sync->find({ context => $ot2ut_context });
    $cur_sync->fields( { map { $_ => 1 } @sync_fields } );

    print __LINE__, " reading sync db\n";
    while ($running)
    {
      my $row_sync= $cur_sync->next();
      last unless (defined ($row_sync));
      # print __LINE__, " row_sync: ", Dumper($row_sync);
      my %rec= map { $_ => $row_sync->{$_} } @sync_fields;
      $data{$rec{eprint_id}}= \%rec;
    }
  }

  # pass 2: get data from utheses.policies
  print __LINE__, " get data from utheses.policies\n";
  my @utp_fields= qw(eprint_id ac_nummer urn doi lastmod);
  {
    my $col_utp= $db_ot2ut->get_collection('utheses.policies');
    my $cur_utp= $col_utp->find();
    $cur_utp->fields( { map { $_ => 1 } @utp_fields } );
    print __LINE__, " reading utp\n";
    while ($running)
    {
      my $row_utp= $cur_utp->next();
      last unless (defined ($row_utp));
      my $id= $row_utp->{eprint_id};
      if (exists ($data{$id}))
      {
        my $rec= $data{$id};
        foreach my $f (@utp_fields) { $rec->{$f}= $row_utp->{$f}; }
      }
    }
  }

  # 3: get data from alma.marc
  print __LINE__, " get data from alma.marc\n";
  my @marc_fields= qw(ac_number mms_id fetched lib_code);
  my @marc_extra_fields= qw(marc_record ts_fetched);

  my $mex= Alma::MARC_Extractor->new(\@marc_fields);

  {
    my $db_marc= get_marc_db($cnf);
    my $col_marc= $db_marc->get_collection('alma.marc');
    print __LINE__, " checking alma.marc\n";
    my $num= 0;
    MARC: foreach my $eprint_id (keys %data)
    {
      my $rec= $data{$eprint_id};
      my $ac_nummer= $rec->{ac_nummer};

      if ((++$num % 1000) == 0)
      {
        print __LINE__, " num=[$num] eprint_id=[$eprint_id] ac_nummer=[$ac_nummer]\n"
      }
      $rec->{marc_record}= 'no_ac_number';

      next MARC unless (defined ($ac_nummer)); # no ac_number (or ac_nummer) no fun!

      my $marc= $col_marc->find_one({ ac_number => $ac_nummer });
      unless (defined ($marc))
      {
        $rec->{marc_record}= 'not_found';
        next MARC;
      }

      $rec->{marc_record}= 'found';
      $rec->{ts_fetched}= Util::ts::ts_ISO_gmt($marc->{fetched});
      $mex->extract_identifiers($marc, $rec);
    }
  }

  # 4: get bucket codes from bucket.lists
  print __LINE__, " get data from bucket.lists\n";
  {
    $db_ot2ut= IRMA::db::get_any_db($cnf, 'ot2ut_database') unless (defined ($db_ot2ut));
    my $col_bl= $db_ot2ut->get_collection('bucket.lists');
    my $cur_bl= $col_bl->find();
    print __LINE__, " cur_bl=[$cur_bl]\n";

    BUCKET: while ($running)
    {
      my $bucket= $cur_bl->next();
      last BUCKET unless (defined ($bucket));
      my ($bucket_code, $eprint_ids)= map { $bucket->{$_} } qw( code ids );
      # print __LINE__, " bucket_code=[bucket_code] eprint_ids=[$eprint_ids]\n";

      foreach my $id (keys %$eprint_ids)
      {
        # print __LINE__, " id=[$id]\n";
        next unless (exists ($data{$id}));
        $data{$id}->{bucket_code}= $bucket_code;
      }
    }
  }

  # write tsv
  {
    my $tsv_fnm= sprintf ("%s/sync_%s.tsv", $base_path, $ot2ut_context);
    print __LINE__, " writing migration table [$tsv_fnm]\n";

    # my @tsv_columns= (@sync_fields, @utp_fields, @marc_fields, @marc_extra_fields); # NOTE: eprint_id is there several times

    my @tsv_columns= qw( eprint_id eprint_status lastmod
                         urn doi ac_nummer bucket_code
                         utheses_id ts_upload upload_status
                         ac_number mms_id fetched lib_code marc_record ts_fetched ts_marc
                         df_urn val_urn df_doi val_doi df_othes val_othes df_utheses val_utheses
                        );

    open (TSV, '>:utf8', $tsv_fnm) or die;
    print TSV join("\t", @tsv_columns), "\n";
    foreach my $eprint_id (sort { $a <=> $b } keys %data)
    {
      my $rec= $data{$eprint_id};
      print TSV join("\t", map { $rec->{$_} } @tsv_columns), "\n";
    }
    close (TSV);
  }

}

sub cleanup_keywords
{
  my $s= shift;

  my @notes;

  push (@notes, 'kw_ws_eol')    if ($s =~ s#[\t\s\n]+$##g);    # ignore spaces at the end
  push (@notes, 'kw_delim_eol') if ($s =~ s#[\.,;/]+$##g); # ignore delimiters at the end
  push (@notes, 'kw_lf')        if ($s =~ s#[\r]+##g);  # 
  push (@notes, 'kw_tab')       if ($s =~ s#[\t]+# #g); # tabs are blanks
  push (@notes, 'kw_nl')        if ($s =~ s#\n+# / #g); # newline as delimiter
  $s=~ s/^\s*//;
# $s=~ s/\s*$//;
# $s=~ s/ +/ /g;

  return (['kw_empty'], []) if ($s eq '' or $s =~ /^\s*nicht\s*angegeben\s*\.*\s*$/);

  my @keywords;

  my @kw1= split('\s+/[\s\/]+', $s); # "foo / / bar" should be only two fields
  if (@kw1 == 1)
  {
    my @kw2= split('\s*;\s*', $s);
    my @kw3= split('\s*,\s*', $s);
    # print __LINE__, " kw3: ", Dumper(\@kw3);

    if (@kw2 > 1)
    {
      push (@notes, 'kw_semicolon');
      @keywords= @kw2;
    }
    elsif (@kw3 > 1)
    {
      push (@notes, 'kw_comma');
      @keywords= @kw3;
      # print __LINE__, " KW3\n";
    }
    else
    {
      my @kw4= split('\s*/\s+', $s);
      if (@kw4 > 1)
      {
        @keywords= @kw4;
        push (@notes, 'kw_slash2');
      }
      else
      {
        my @kw5= split('\s*/\s*', $s);
        if (@kw5 > 1)
        {
          @keywords= @kw4;
          push (@notes, 'kw_slash3');
        }
        else
        {
          push (@notes, 'kw_single');
          @keywords= @kw1;
        }
      }
    }
  }
  else
  {
    @keywords= @kw1;
    push (@notes, 'kw_slash');
  }

# print __LINE__, " keywords: ", Dumper (\@keywords);
  my @res;
  foreach my $kw (@keywords)
  {
    push (@res, $kw) if ($kw ne '');
  }

# print __LINE__, " res: ", Dumper (\@res);
  (\@notes, \@res);
}

package IRMA::ac_number;

=head2 check_ac_number ($irma_db, $ac_number, $context, $context_id, $context_url)

parameters:

  0: irma_db
  1: ac_number
  2: context (such as "eprints", "eod")
  3: context_id (identifiert for the given context, e.g. 12345 in eprints or o:123456 in eod)
  4: context_url (pointer to object's landing page)

Return values:

Negative values are error codes (TODO: add some reporting), postitive
values are a bit field of the kind of changes performed.

 -2: context conflict
 -1: multiple ac_number entries not cleaned up
  0: nothing to do
  1: updated or added item
  2: multiple records removed

=cut

sub check_ac_number
{
  my $irma_db= shift;
  my $ac_number= shift;
  my $context= shift;
  my $context_id= shift;
  my $context_url= shift;

  # $irma_db->verbose(1);
  # my $irma_dbh= $irma_db->connect();
  my $query= ['ac_number=?', $ac_number];
  my $x= $irma_db->get_all_x ('ac_numbers', $query);
  # print __LINE__, " check_ac_number x: ", main::Dumper($x);
  my @x= keys %$x;

  my $res= 0;
  my $ac_record;

  if (@x > 1)
  {
    print __LINE__, " ac_number=[$ac_number] exists too often: ", main::Dumper ($x);
    if ($delete_bad_ac_entries)
    {
      $irma_db->delete_all_x ('ac_numbers', $query);
      @x= ();
      $res |= 2;
    }
    else
    {
      return (-1, undef);
    }
  }

  if (@x == 1)
  {
    # print __LINE__, " ac_number known\n";
    $ac_record= $x->{$x[0]};
    # print __LINE__, " row: ", main::Dumper ($row);

    if ($ac_record->{context} ne $context)
    {
      print "ac_number in other context: [", $ac_record->{context}, "], should be [$context]\n";
      $res= -2;
    }
    # TODO: else compare fields and updated if necessary...
  }
  elsif (@x == 0)
  {
    $ac_record=
    {
      ac_number   => $ac_number,
      context     => $context,
      context_id  => $context_id,
      context_url => $context_url,
      ac_status   => 'new'
    };

    my $id= $irma_db->insert ('ac_numbers', $ac_record);
    print __LINE__, " ac_number=[$ac_number] not yet known, added; new id=$id\n";
    $res |= 1;
  }

  ($res, $ac_record);
}

sub reset_ac_error
{
  my $irma_db= shift;
  my $ac_number= shift;

  # $irma_db->verbose(1);
  my $query= ['ac_number=?', $ac_number];

  my $x_ep= $irma_db->get_all_x ('eprints', $query);
  if (defined ($x_ep))
  {
    print __LINE__, " reset_ac_error x_ep: ", main::Dumper($x_ep);
    foreach my $id (keys %$x_ep)
    {
      my %upd=
      (
        ac_number_status => undef,
        item_status => 'new',
      );
      print __LINE__, " reset_ac_error upd: ", main::Dumper(\%upd);
      $irma_db->update('eprints', $id, \%upd);
    }
  }

  my $x_ac= $irma_db->get_all_x ('ac_numbers', $query);
  if (defined ($x_ac))
  {
    print __LINE__, " reset_ac_error x_ac: ", main::Dumper($x_ac);
    foreach my $id (keys %$x_ac)
    {
      my %upd=
      (
        xml_ts       => undef,
        xml_urn      => undef,
        xml_url      => undef,
        ac_status    => 'new',
        error_source => undef,
        error_text   => undef,
      );
      print __LINE__, " reset_ac_error upd: ", main::Dumper(\%upd);
      $irma_db->update('ac_numbers', $id, \%upd);
    }
  }
}

sub get_ac_errors
{
  my $irma_db= shift;

  # $irma_db->verbose(1);
  my $query= ['error_source is not null'];

  $irma_db->get_all_x ('ac_numbers', $query);
}

package cctab;

sub new
{
  my $class= shift;
  my %par= @_;

  my $self=
  {
    buckets => {},
    values => [],
  };
  bless $self, $class;

  foreach my $par (keys %par)
  {
    $self->{$par}= $par{$par};
  }

  $self;
}

sub normalize
{
  my $val= shift;

  if (!defined($val)) { $val= 'NULL'; }
  elsif ($val eq '')  { $val= 'empty'; }
  elsif ($val =~ m#^\d+$# && $val > 1) { $val= '2+'; }

  $val;
}

sub bucket
{
  my $self= shift;
  my $create= shift;
  my @pars= @_;

  my $p= $self->{buckets};
  my $v= $self->{values};

  my @norm;
  for (my $i= 0; $i <= $#pars; $i++)
  {
    my $par= $pars[$i];
    my $norm= normalize($par);
    push (@norm, $norm);
    # print __LINE__, " par=[$par] norm=[$norm]\n";

    unless (exists($p->{$norm}))
    {
      return undef unless ($create);
      $p->{$norm}= {};
    }
    $p= $p->{$norm};

    $v->[$i]->{$norm}++;
  }
  
  # print __LINE__, " norm=[", join(':', @norm), "]\n";
  $p->{code}= join ('', map { substr($_,0,1); } @norm);

  $p;
}

sub show_tsv # TODO: rename ...
{
  my $self= shift;
  my $counters= shift;
  my $fnm_counters= shift || 'counters.tsv';
  # my $trailer= shift;

  $db_ot2ut= IRMA::db::get_any_db($cnf, 'ot2ut_database') unless (defined ($db_ot2ut));
  my $col_bc= $db_ot2ut->get_collection('bucket.counters');
  my $col_bl= $db_ot2ut->get_collection('bucket.lists');
  $col_bl->remove({});

  my @columns= @{$self->{columns}};
  my $column_count= @columns;

  my $b= $self->{buckets};
  my @rows= ();

  enumerate(\@rows, $b, $column_count, []);

  my @counters= @$counters;
  my @heading= ('bucket', 'code', @columns, @counters);

  my $idx_html= join('/', $self->{base_path}, 'buckets.html');
  open (BUCKETS, '>:utf8', $idx_html) or die;
  print BUCKETS <<"EOX";
<html>
<head>
<meta charset="UTF-8" />
<title>ot2ut bucket list</title>
<style>
  td { text-align:right; }
</style>
</head>
<body>
<table border="1">
  <tr>
EOX
  foreach my $hdr (@heading) { print BUCKETS "<th>$hdr</th>"; }
  print BUCKETS <<EOX;
  </tr>
EOX

  open (TSV, '>:utf8', $fnm_counters) or die;
  print __LINE__, " saving bucket_counters to '$fnm_counters'\n";
  print TSV join("\t", @heading), "\n";

  my $cnt_ac_numbers= 0;

  # print __LINE__, " rows: ", main::Dumper(\@rows);
  my $bucket_nr= 0;
  foreach my $row (@rows)
  {
    $bucket_nr++;
    my ($vals, $bucket)= @$row;
    my @vals= @$vals;

    my $bucket_code= $bucket->{code};

    print TSV join("\t", $bucket_nr, $bucket_code, @vals, map { $bucket->{$_} } @counters), "\n";
    Util::JSON::write_json_file("bucket_${bucket_code}.json", $bucket);

    $col_bl->update({ code => $bucket_code }, $bucket, { upsert => 1 });

    my $annotation= $bucket->{annotation};
    my $row_info;
    if (defined ($annotation))
    {
      # print __LINE__, " bucket has annotation: ", main::Dumper($annotation);
      $row_info= ' bgcolor="green"';
      if (exists ($annotation->{bgcolor}))
      {
        $row_info= ' bgcolor="'. $annotation->{bgcolor}. '"';
      }
    }

    # my $fnm_lst= sprintf("bucket_%d.html", $bucket_nr);
    my $fnm_lst= sprintf("bucket_%s.html", $bucket_code);
    my $path_lst= join ('/', $self->{base_path}, $fnm_lst);
    open (LST, '>:utf8', $path_lst) or die;
    print LST <<"EOX";
<html>
<head>
<meta charset="UTF-8" />
<title>bucket $bucket_nr</title>
<style>
  td { text-align:right; }
</style>
</head>
<body>
<p>code: $bucket_code</p>
<table>
EOX

    my @ctr= map { $bucket->{$_} } @counters;
    foreach my $col (@columns)
    {
      my $val= shift (@vals);
      my $desc= $bucketlist_column_descriptions{$col} || '&nbsp;';
      print LST "<tr><th>$col</th><td>$val</td><td style=\"text-align:left\">$desc</td></tr>\n";
    }
    print LST "</table>\n";

    if (defined ($annotation))
    {
      print LST "<p>Note: ", $annotation->{note}, "</p>\n";
    }
    print LST <<"EOX";
<table border="1">
<tr><th width="150px">ac_number</th><th width="100px">othes</th><th width="100px">entw</th><th width="100px">test</th><th width="100px">prod</th><th>errors</th><th>warnings</th></tr>
EOX

    my $ids= $bucket->{ids};
    # print __LINE__, " bucket: ", main::Dumper($bucket);
    my @ids= CORE::sort { $a <=> $b } map { $_+0 } keys %$ids;
    # print __LINE__, " ids: ", join(' ', @ids), "\n";
    # my @ids2= sort { $a cmp $b } @ids;
    # print __LINE__, " ids2: ", join(' ', @ids2), "\n";
    # print LST '<!-- ids: xx '. join(' ', @ids), " -->\n";
    foreach my $id (@ids)
    {
      my $p= $ids->{$id};
      print LST "<tr>";

      if (exists($p->{ac_number}) && defined ($p->{ac_number}))
      {
        my $ac= $p->{ac_number};
        my $l= "https://ubdata.univie.ac.at/" . $ac;
        print LST "<td><a href=\"$l\" target=\"alma\">$ac</a></td>";
        $cnt_ac_numbers++;
      }
      else
      {
        print LST "<td>&nbsp;</td>";
      }

      print LST "<td><a href=\"https://othes-legacy.univie.ac.at/$id/\" target=\"othes\">$id</a></td>";
      # print __LINE__, " p: ", main::Dumper($p);

      foreach my $context (qw(entw test prod))
      {
        unless (exists ($p->{$context}))
        {
          print LST "<td>&nbsp;</td>";
          next;
        }

        my $other= $p->{$context};
        my $link;

           if ($context eq 'test') { $link= 'https://utheses-frontend.ctest.univie.ac.at/client/#/detail/' . $other . '/'; }
        elsif ($context eq 'entw') { $link= 'https://utheses-frontend-entw-utheses.ctest.univie.ac.at/#/detail/' . $other . '/'; }
        elsif ($context eq 'prod') { $link= 'https://utheses-admin-ui-utheses-prod.cprod.univie.ac.at/client#/detail/' . $other . '/'; }
  #        if ($context eq 'test') { $link= 'https://utheses-frontend.ctest.univie.ac.at/client/?#/view/document/utheses/' . $other; }
  #     elsif ($context eq 'entw') { $link= 'https://utheses-frontend-entw-utheses.ctest.univie.ac.at/?#/view/document/utheses/' . $other; }
  #     elsif ($context eq 'prod') { $link= 'unknown'; }

        print LST "<td><a href=\"$link\" target=\"$context\">$other</a></td>";
      }

      if (exists ($p->{errors})) { print LST '<td>', main::Dumper($p->{errors}), "</td>\n"; }
      else { print LST "<td>&nbsp;</td>\n"; }

      if (exists ($p->{warnings})) { print LST '<td>', main::Dumper($p->{warnings}), "</td>\n"; }
      else { print LST "<td>&nbsp;</td>\n"; }

      print LST "</tr>\n";
    }

    print LST <<"EOX";
</table>
</body>
</html>
EOX
    close (LST);

    print BUCKETS "<tr$row_info><td><a href=\"$fnm_lst\" target=\"bucket\">$bucket_nr</a></td>";
    print BUCKETS "<td>", $bucket_code, "</td>";
    foreach my $val (@$vals)
    {
      print BUCKETS "<td>$val</td>";
    }

    ctr: foreach my $ctr (@counters)
    {
      my $cnt= $bucket->{$ctr};
      if (defined ($cnt)) { print BUCKETS "<td>$cnt</td>"; }
      else { print BUCKETS "<td>&nbsp;</td>"; }
    }
    print BUCKETS "</tr>\n";

  }

  print BUCKETS <<EOX;
</table>
</body>
</html>
EOX

  close (BUCKETS);
  close (TSV);

  $bucket_nr;
}

sub enumerate
{
  my $r= shift;
  my $b= shift;
  my $c= shift;
  my $v= shift;

  foreach my $key (sort keys %$b)
  {
    my @v= @$v;
    push (@v, $key);

    if ($c == 1)
    {
      push(@$r, [\@v, $b->{$key}]);
    }
    else
    {
      enumerate ($r, $b->{$key}, $c-1, \@v);
    }
  }
}

__END__

=head1 TODO

=head2 statistics

print counter after major operations

=head3 --rms

=head3 --mab4ep

  * cnt_aleph_queries
  * cnt_aleph_checked
  ** cnt_aleph_ok
  ** cnt_aleph_failed

=head3 --nbn4ep

=head2 refactoring

  * nothing open ...

=head1 DATABASE TABLES

=head2 ac_numbers

=head3 ac_status

  * new: item was added and needs to be processed
  * error: something is broken, see error_source and error_text for details
  * requested: mab record was requested from Aleph and is pending
  * available: ???
  * unknown: mab record could not be requested
  * verified: mab record has been checked and it seems to be ok
  * done: item is completly processed, no further processing/checking necessary; NOTE: not used?

=head3 error_source

  * link_validation: check if link in Aleph points to the right resource
  * mab_fetch: no MAB data found for given ac_number; this usually means, the AC-number does not exist in Aleph

=head3 urn_status

  * new: newly created entry (--> requested)
  * requested: urn:nbn:at agent was asked to request and register an urn:nbn:at identifier
  * failed: (not used); if something breaks
  * registered: registration agent performed it's task

=head1 Aleph MAB Data extractions

 * URL '655:e:1:u'
 * URN '552:b:1:a'

=head1 Alma MARC Data extractions

 * URL '856:4: :u'
 * URN '024:7: :a'

=head2 eprints

=head3 item_status

  * new
  * urn_registered:

=head4 item_status new

  this is currently always "new", no matter how far the processing has gone.

  --- 8< ---
  mysql> select count(*),item_status from eprints group by item_status;
  +----------+-------------+
  | count(*) | item_status |
  +----------+-------------+
  |    35316 | new         |
  +----------+-------------+
  1 row in set (0.03 sec)
  --- >8 ---

=head3 ac_number_status

  apparently not used, always undef?

  --- 8< ---
  mysql> select count(*),ac_number_status from eprints group by ac_number_status;
  +----------+------------------+
  | count(*) | ac_number_status |
  +----------+------------------+
  |    35316 | NULL             |
  +----------+------------------+
  1 row in set (0.03 sec)
  --- >8 ---


=head3 urn_status

  * NULL
  * upstream : urn was already found in the upstream database; => should be verified (see ~ L1350)
  * torequest : should be ordered from upstream?
  * requested
  * registered : data stored in resolver
  * invalid : urn does not match that found in ac_numbers

=head2 unprocessed items

Items in eprints need to have a properly formatted entry in "matr".
Maybe there should be a way to specify those items, that should be
processed even without that requirement, e.g.:

 * 52581

=head1 TODO/utheses

=head2 Keywords geandert (2020-KW06)



=head1 NOTES

=head2 language codes

  https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes
  * deu ... german (ger is only a synonym in ISO 639-2)
  * eng ... english

  EPrints uses "ger" while Phaidra seems to use "deu".  Utheses first used "ger" and may have switched to "deu" in the meantime.  [TODO: checkwhat utheses really wants]

=head2 timestamps

  https://wiki.univie.ac.at/display/ST5/u%3Atheses+-+Datumswerte+-+Relationen

=head2 special cases

 * kein sperrdatum (datum_sperre_year is null) && sperre is NULL: Selbsthochlader als Quelle [2020-06-30]

=head1 BUGS

=head2 DOI registration

=head3 Entity encoding

Example 1:
 https://othes.univie.ac.at/20315/
 https://doi.datacite.org/dois/10.25365%2Fthesis.20315

Title "Management of risky R & D projectsM" ends up as "Management of
risky R D projectsM" in DataCite XML.

Notes:
 * "projectsM" is a typo in eprints itself [fixed on othes; TODO: fix DataCite]
 * & in abstracts is encoded as &amp; in the DataCite.XML file, but DataCite itself seems to strip that out again
 * as of 2020-01-20, title is processed with strip_text()

