#!/usr/bin/perl

=begin comment

Generate updates for Aleph/Alma with new NBN that are currently missing
in their database.  Used once, but the batch was not processed for Aleph
import and then came the Alma conversion, so I have no idea if the format
is wrong or what else prevented the import on their side.

=end comment
=cut

use strict;

my @eprint_ids= check (shift);
write_batch (undef, @eprint_ids);

sub write_batch
{
  my $fnm= shift;
  my @eprint_ids= @_;

  my @ts= localtime(time());
  my $ts= sprintf ("%04d-%02d-%02dT%02d%02d%02d", $ts[5]+1900, $ts[4]+1, $ts[3], $ts[2], $ts[1], $ts[0]);

  $fnm= 'do_generate_abstracts_' . $ts . '.sh' unless ($fnm);

  die "file [$fnm] already exists" if (-f $fnm);

  open (FO, '>:utf8', $fnm) or die "can't write to [$fnm]\n";

  print FO <<EOX;
#!/bin/sh -v
# automatically generated on $ts
#
EOX
  foreach my $eprint_id (@eprint_ids)
  {
    print FO "/usr/share/eprints/bin/generate_abstracts othes $eprint_id\n";
  }

  close (FO);
}

sub check
{
  my $fnm= shift;

  open (FI, '<:utf8', $fnm) or die "can't read [$fnm]\n";
  print "reading [$fnm]\n";

  my $pick= 0;
  my @eprint_ids= ();
  while (<FI>)
  {
    chop;

    # print ">>> $pick [$_]\n";

    if (m#note=\[sync-back urn=\[urn:nbn:at:at-ubw:1.*\]\]#) { $pick= 1; }
    elsif ($pick == 1) { $pick= ($_ eq 'ssu=[UPDATE `eprint` SET urn=? WHERE `eprintid`=?]') ? 2 : 0 }
    elsif ($pick == 2)
    {
      #    [vals: urn:nbn:at:at-ubw:1-12511.70929.824160-6,43647]
      if (m#vals: ([a-z\d\-\.:]+),(\d+)#)
      {
        my ($nbn, $eprint_id)= ($1, $2);
        print "match: nbm=[$nbn] eprint_id=[$eprint_id]\n";
        push (@eprint_ids, $eprint_id);
      }
      $pick= 0;
    }
  }
  close (FI);

  @eprint_ids;
}
