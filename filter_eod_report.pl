#!/usr/bin/perl

use strict;

use Data::Dumper;
$Data::Dumper::Indent= 1;

use Util::Simple_CSV;

my $detail_level= 1;
# 0 .. only the most necessary things (corrections and new phaira urls)
# 1 .. plus everything that is missing, e.g. added NBNs (urn) or Handles (hdl)

my @pars= ();
while (defined (my $arg= shift (@ARGV)))
{
     if ($arg eq '-')  { push (@pars, '-'); }
  elsif ($arg eq '--') { push (@pars, @ARGV); @ARGV= (); }
  elsif ($arg =~ /^--(.+)/)
  {
    my ($opt, $val)= split ('=', $1, 2);
       if ($opt eq 'help') { usage(); }
    elsif ($opt eq 'heavy') { $detail_level= 1; }
    elsif ($opt eq 'light') { $detail_level= 0; }
    else { usage(); }
  }
  elsif ($arg =~ /^-(.+)/)
  {
    foreach my $opt (split ('', $1))
    {
         if ($opt eq 'h') { usage(); exit (0); }
      elsif ($opt eq 'H') { $detail_level= 1; }
      elsif ($opt eq 'L') { $detail_level= 0; }
      else { usage(); }
    }
  }
  else
  {
    push (@pars, $arg);
  }
}

print join (' ', __FILE__, __LINE__, 'caller=['. caller() . ']'), "\n";

my $fnm_in=  shift(@pars) || 'eod_data.tsv';
my $fnm_out= shift(@pars) || 'report_bib_'. (($detail_level) ? 'heavy' : 'light') .'.tsv';

my @columns_copy= qw(ac_number mms_id alma_notes ticket val_hdl);
my @columns_out=  qw(ac_number mms_id alma_fetched alma_notes ticket url nbn hdl);

my $tsv_in= Util::Simple_CSV->new(load => $fnm_in, separator => "\t");

open (TSV_OUT, '>:utf8', $fnm_out) or die;
print TSV_OUT join("\t", @columns_out), "\n";

my $data= $tsv_in->{data};

# print __LINE__, " data: ", Dumper ($data);
my $count= 0;
foreach my $row (@$data)
{
  # print __LINE__, " row: ", Dumper ($row);
  my ($update_url, $update_urn, $update_hdl, $update_doi)= map { $row->{$_} } qw( update_phaidra_url update_urn update_hdl update_doi );
  # printf ("update_url=[%s] update_urn=[%s] update_hdl=[%s] update_doi=[%s]\n", $update_url, $update_urn, $update_hdl, $update_doi);

  if ($update_url || ($detail_level && ($update_urn || $update_hdl)))
  {
    my %out= map { $_ => $row->{$_} } @columns_copy;
    $out{url}= $update_url if (defined ($update_url));
    $out{nbn}= $update_urn if (defined ($update_urn));
    $out{hdl}= $update_hdl if (defined ($update_hdl));

    $out{alma_fetched}= $row->{ts_fetched};
    $out{hdl}= $row->{hdl} if (($detail_level == 0 && $row->{val_hdl} ne $row->{hdl}) || $detail_level > 0); # set handle only when not alreadys set...

    # print __LINE__, " out: ", Dumper(\%out);
    print TSV_OUT join("\t", map { $out{$_} } @columns_out), "\n";
    $count++;
  }
}

close(TSV);

print __LINE__, " saved $count records to $fnm_out\n";


