#!/usr/bin/perl

use strict;

use Data::Dumper;
$Data::Dumper::Indent= 1;

use Util::Simple_CSV;

conv_export_to_mab ('export-1476807570.tsv', 'loader.txt');
exit (0);


sub conv_export_to_mab
{
  my $csv_fnm= shift;
  my $mab_loader= shift;

  if (-f $mab_loader)
  {
    print "file [$mab_loader] already exists\n";
    return undef;
  }
  unless (open (FO, '>:utf8', $mab_loader))
  {
    print "can not write to file [$mab_loader]\n";
    return undef;
  }

  my $csv= new Util::Simple_CSV ('load' => $csv_fnm, 'separator' => "\t");
  foreach my $row (@{$csv->{data}})
  {
    # print "row: ", Dumper ($row);
    my ($ac_number, $urn_value)= map { $row->{$_} } qw(ac_number urn_value);
    print FO <<EOX;
$ac_number 552b  L \$\$a$urn_value
$ac_number 655o  L \$\$mV:AT-OBV;B:AT-UBW\$\$qtext/html\$\$uhttps://resolver.obvsg.at/$urn_value\$\$xUBW\$\$3Volltext//Hochschulschriftenserver der UB Wien
EOX
  }

  close (FO);
}

__END__

AC08486132 552b  L $$aurn:nbn:at:at-ubw:1-29045.11470.739859-8
AC08486132 655o  L $$mV:AT-OBV;B:AT-UBW$$qtext/html$$uhttps://resolver.obvsg.at/urn:nbn:at:at-ubw:1-29045.11470.739859-8$$xUBW$$3Volltext//Hochschulschriftenserver der UB Wien
AC10725029 552b  L $$aurn:nbn:at:at-ubw:1-29038.50227.163765-2
AC10725029 655o  L $$mV:AT-OBV;B:AT-UBW$$qtext/html$$uhttps://resolver.obvsg.at/urn:nbn:at:at-ubw:1-29038.50227.163765-2$$xUBW$$3Volltext//Hochschulschriftenserver der UB Wien 

