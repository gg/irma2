#!/usr/bin/perl

use strict;

use FileHandle;
use utf8;

binmode( STDOUT, ':utf8' ); autoflush STDOUT 1;
binmode( STDERR, ':utf8' ); autoflush STDERR 1;
binmode( STDIN,  ':utf8' );

use Data::Dumper;
$Data::Dumper::Indent= 1;

use Digest::MD5::File qw(file_md5_hex);
use Digest::MD5 qw(md5_hex);
use Encode qw(encode_utf8);

use Util::ts;
use Util::JSON;
use Redmine::DB::MySQL;

use Phaidra::Utils::iso639;

use lib 'lib';
use Univie::Utheses::API;
use DataCite::API;
use IRMA::NA;
use Alma::MARC_Extractor;

my @TSV_COLUMNS= qw( utheses_id fulltext_locked suffix doi nbn ac_number langs language persistent_link xml_fnm errors );

my $op_mode= 'process';
my $fnm_tsv= 'utheses/utheses_info.tsv'; # TODO: timestamp!

# TODO: make these configurable too!
my $agent_config_file= '/etc/irma/pidagent.json';

my $MAX_MARC_AGE= 86400*60;
my $MAX_MARC_REQUESTS= 10_000;

my %metrics;

my $do_register_doi= 0;
my $agent_name= 'pidagent';
my $agent_id= $$;
my $fix_problems= 0;

my @pars= ();
while (my $arg= shift (@ARGV))
{
  if ($arg eq '--') { push (@pars, @ARGV); @ARGV= (); }
  elsif ($arg =~ /^--(.+)/)
  {
    my ($opt, $val)= split ('=', $1, 2);
       if ($opt eq 'help') { usage(); }
    elsif ($opt eq 'register-doi') { $do_register_doi= 1; }
    elsif ($opt eq 'fix') { $fix_problems= 1; }
    elsif ($opt eq 'fmb') { my $op_mode= 'fetch_metadata_bulk'; }
    else { usage(); }
  }
  elsif ($arg =~ /^-(.+)/)
  {
    foreach my $opt (split ('', $1))
    {
         if ($opt eq 'h') { usage(); exit (0); }
      # elsif ($opt eq 'x') { $x_flag= 1; }
      else { usage("unknown option $arg"); }
    }
  }
  else
  {
    push (@pars, $arg);
  }
}

# load configuration
my $agent_cnf= Util::JSON::read_json_file ($agent_config_file);
# print __LINE__, " agent_cnf: ", main::Dumper ($agent_cnf); exit(0);
my $dc_cnf= Util::JSON::read_json_file ($agent_cnf->{DataCite_config});
# print __LINE__, " dc_cnf: ", main::Dumper ($dc_cnf); exit(0);
my $ut_cnf= $dc_cnf->{repositories}->{'utheses.univie.ac.at'};
# print __LINE__, " ut_cnf: ", main::Dumper ($ut_cnf); exit(0);
my $dc_reg_cnf= $dc_cnf->{doi_registries}->{my $dc_cnf_name= $ut_cnf->{registry}};
# print __LINE__, " dc_reg_cnf: ", main::Dumper ($dc_reg_cnf); exit(0);

# prepare APIs
my $ut_api= new Univie::Utheses::API( config => $ut_cnf->{api_config} );
die "no ut_api" unless (defined ($ut_api));

my $reg_obj= new DataCite::API (config => $dc_reg_cnf, xmode => 'test');
die "no reg_obj" unless (defined ($reg_obj));

# get handles for various databases
my $marc_db=  IRMA::db::get_any_db($agent_cnf, 'marc_database');
my $marc_col= $marc_db->get_collection('alma.marc');
my $req_col= $marc_db->get_collection('requests');

my @marc_fields= qw(ac_number mms_id fetched lib_code);
my $mex= Alma::MARC_Extractor->new(\@marc_fields);

my $agent_db= IRMA::db::get_any_db($agent_cnf, 'pidagent_database');
# print __LINE__, " agent_db=[$agent_db]\n";
my $ut_col= $agent_db->get_collection('utheses');
my $dc_col= $agent_db->get_collection('datacite');
my $q_col= $agent_db->get_collection('queue');
my $problems_col= $agent_db->get_collection('problems');

my ($nx_metrics_temp, $nx_metrics_prom);
my @REPORT;

if (exists ($agent_cnf->{'collector-textfile-directory'})
    && defined (my $p= $agent_cnf->{'collector-textfile-directory'})
   )
{
  $nx_metrics_temp= join('/', $p, 'ut1.temp');
  $nx_metrics_prom= join('/', $p, 'ut1.prom');
}

my $running= 0;
if ($op_mode eq 'process')
{
  do
  {
    while (my $par= shift (@pars))
    {
      if ($par =~ m#^(\d+)$#
          || $par =~ m#https?://utheses.univie.ac.at/detail/(\d+)[\#/]?#
         )
      {
        my $utheses_id= $1;
        my @actions= process_utheses_item($utheses_id);
        print __LINE__, " process_utheses_item($utheses_id) ==> [", join(', ', @actions), "]\n";
        push (@REPORT, join(' ', $utheses_id, @actions));
      }
      elsif ($par =~ m#^(\d+)\.\.(\d+)$# || $par =~ m#^(\d+)\-(\d+)$# || $par =~ m#(blk)(\d+)$#)
      {
        my ($start, $end)= ($1, $2);
        ($start, $end)= ($end*100, $end*100+99) if ($start eq 'blk');
        my $item_count= $end-$start;
        if ($start <= $end && (($item_count <= 2000 && $do_register_doi) || ($item_count <= 5000 && !$do_register_doi)))
        {
          foreach (my $utheses_id= $start; $utheses_id <= $end; $utheses_id++)
          {
            push (@pars, "$utheses_id"); # cast to string!
          }
        }
      }
      elsif ($par =~ m#^AC\d{8}$#)
      {
        my @actions= process_ac_number($par);
        print __LINE__, " process_ac_number($par) ==> [", join(', ', @actions), "]\n";
      }
      elsif ($par eq 'queue')
      {
        my @utheses_ids= get_job_from_queue();
        print __LINE__, ' queued utheses_ids: ', join(', ', @utheses_ids), "\n";
        sleep(10);
        push (@pars, @utheses_ids, 'cleanup');
      }
      elsif ($par eq 'cleanup')
      {
        cleanup_queue();
      }
      elsif ($par eq 'gpdcr')
      {
        my $utheses_ids1= gpdcr();
        my $utheses_ids2= remove_registered_DOIs($utheses_ids1);
        print __LINE__, " gpdcr utheses_ids2: ", join (' ', @$utheses_ids2), "\n";
        push (@pars, @$utheses_ids2);
      }

      sleep(2) if (@pars);
    }
  } while($running);
}
elsif ($op_mode eq 'fetch_metadata_bulk')
{
  fetch_metadata_bulk(\@pars);
}

print __LINE__, " metrics: ", Dumper(\%metrics);
write_metrics(\%metrics);
print __LINE__, " REPORT:\n";
foreach my $l (@REPORT) { print $l, "\n"; }

exit(0);

sub gpdcr
{
  my ($status, $txt, $info)= $ut_api->getPendingDoisCreateRequest();
  # print __LINE__, " gpdcr: info: ", Dumper($info);

  my @utheses_ids= ();
  if ($status eq '200')
  {
    my $p= $info->{pendingDois};
    my $fnm= 'Pending_DOI_Create_Requests_'. Util::ts::ts_ISO_gmt(). '.json';
    Util::JSON::write_json_file($fnm, $info);
    print __LINE__, " saved $fnm\n";
    if (defined ($p) && ref($p) eq 'HASH')
    {
      push (@utheses_ids, map { $p->{$_}->{utheses_id} } keys %$p);
    }
    else
    {
      die "unexpected type of pendingDois";
    }
  }

  $metrics{ut1_gpdcr_count}= @utheses_ids;

  (wantarray) ? @utheses_ids : \@utheses_ids;
}

sub remove_registered_DOIs
{
  my $utheses_id_list= shift;

  print __LINE__, " remove_registered_DOIs()\n";

  # this is not useful, because new IDs are not listed here yet
  # my $cur= $dc_col->find({ utheses_id => { '$in' => $utheses_id_list }, reg_status => { '$ne' => 1} });
  my @utheses_ids= ();
  my @registered_ids= ();
  my ($gpdcr_checked, $gpdcr_queued)= (0, 0);
  foreach my $ut_id (@$utheses_id_list)
  {
    my $dc_info= $dc_col->find_one({ utheses_id => "$ut_id" });
    $gpdcr_checked++;
    if ($dc_info->{reg_status} == 1)
    {
      push (@registered_ids, $ut_id);
      next;
    }

    print __LINE__, " ut_id=[$ut_id] ", Dumper($dc_info);
    push (@utheses_ids, $ut_id);
    $gpdcr_queued++;
  }

  print __LINE__, " registered ids: ", join(' ', @registered_ids), "\n";
  $metrics{ut1_gpdcr_already_registered}= @registered_ids;
  $metrics{ut1_gpdcr_checked}= $gpdcr_checked;
  $metrics{ut1_gpdcr_queued}= $gpdcr_queued;

  (wantarray) ? @utheses_ids : \@utheses_ids;
}

sub cleanup_queue
{
  my $j= $q_col->find_one({status => "in_progress", agent_name => $agent_name, agent_id => $agent_id});
  if (defined ($j))
  {
    print __LINE__, " finishing old job ", Dumper($j);
    $q_col->update({_id => $j->{_id}}, { '$set' => {status => "done", agent_name => $agent_name, agent_id => $agent_id }});
    sleep(10);
  }
}

sub get_job_from_queue
{
  my $j= $q_col->find_one({status => "in_progress", agent_name => $agent_name});
  if (defined ($j))
  {
    print __LINE__, " resuming old job ", Dumper($j);
  }
  else
  {
    $j= $q_col->find_one({status => "new"});
  }
  return () unless (defined ($j));
  print __LINE__, " found new job ", Dumper($j);

  $q_col->update({_id => $j->{_id}}, { '$set' => {status => "in_progress", agent_name => $agent_name, agent_id => $agent_id }});

  my @ut_ids;
  if (exists($j->{utheses_ids})) { push (@ut_ids, @{$j->{utheses_ids}}); }
  if (exists($j->{utheses_id}))  { push (@ut_ids,   $j->{utheses_id}); }

  return @ut_ids;
}

sub process_utheses_item
{
  my $utheses_id= shift;

  print __LINE__, " process_utheses_item: utheses_id=[$utheses_id] ", '='x50, "\n";

  my $register_doi_ok= 1;

  if ($fix_problems) { remove_problem('utheses', $utheses_id); }
  # first: compare existing utheses record (if it exists) with data from utheses
  my $ut_data= $ut_col->find_one( { utheses_id => $utheses_id } );

  my $row= {};
  my ($error_code, $status, $xml)= get_utheses_metadata($row, $utheses_id, $ut_data);

  print __LINE__, " error_code=[$error_code] status=[$status]\n";
  print __LINE__, " xml=[$xml]\n";
  print __LINE__, " row: ", Dumper($row);

  my @actions= ('fetched_utheses');
  unless ($status eq '200')
  {
    push (@actions, 'no_utheses_record');
    $metrics{ut1_no_record}++;
    return @actions;
  }

  unless ($error_code eq 'ok')
  {
    push (@actions, "error_code=[$error_code]");
    report_problem( { area => 'utheses', problem => 'error_code', utheses_id => $utheses_id, error_code => $error_code } );
    $register_doi_ok= 0;
    $metrics{ut1_utheses_errors}++;
  }

  my @x= analyze_marc_record($row);
  push (@actions, @x);

  unless (defined ($agent_db))
  {
    $metrics{ut1_missing_agent_db}++;
    return @actions;
  }

  if (defined ($ut_data))
  {
    my @cmp_fields= qw(doi urn nbn ac_number);
    my @problems= ();
    foreach my $f (@cmp_fields)
    {
      if (exists($ut_data->{$f}) && defined($ut_data->{$f}) && $row->{$f} ne $ut_data->{$f})
      {
        push (@problems, { problem => 'missmatch', field => $f, recorded => $ut_data->{$f}, found => $row->{$f} });
      }
    }

    if (@problems)
    {
      report_problem( { area => 'utheses', problem => 'utheses_data_missmatch', utheses_id => $utheses_id, problems => \@problems } );
      push(@actions, 'problem report utheses');
      $metrics{ut1_utheses_data_missmatch}++;
      return (@actions);
    }
  }

  # second: update utheses record in database
  my $res_upd= $ut_col->update( { utheses_id => $row->{utheses_id} }, $row, { upsert => 1 } );
  print __LINE__, " res_upd: ", Dumper($res_upd);

  my ($ok, $verdict, $msg1, $msg2);
  if ($do_register_doi && $register_doi_ok)
  {
    if ($fix_problems)
    {
      remove_problem('datacite', $row->{utheses_id}, 'utheses_id');
      remove_problem('datacite', $row->{doi});
    }

    print __LINE__, " row: ", Dumper($row);
    my ($utheses_id, $doi, $xml_md5, $url)= map { $row->{$_} } qw(utheses_id doi xml_md5 persistent_link);
    my $dc_record= $dc_col->find_one( { doi => $doi } );

    # prepare registration data
    my %reg_info=
    (
      doi          => $row->{doi},
      url          => $url,
      context      => 'utheses',
      registry     => $dc_cnf_name,
      utheses_id   => $utheses_id,
      ac_number    => $row->{ac_number},
      xml_md5      => $xml_md5,
    # xml_fnm      => $row->{xml_fnm},
      ts_epoch     => time(),
      ts_iso_gmt   => Util::ts::ts_ISO_gmt(),
    );

    # check existing datacite registration record, if it exists
    if (defined ($dc_record))
    {
      print __LINE__, " datacite record found: ", Dumper($dc_record);

      # check if record matches with new data
      my @cmp_fields= qw(registry context url utheses_id ac_number);
      my @problems= ();
      foreach my $f (@cmp_fields)
      {
        if (exists($dc_record->{$f}) && defined($dc_record->{$f}))
        {
          if ($reg_info{$f} ne $dc_record->{$f})
          {
            push (@problems, { problem => 'missmatch', field => $f, recorded => $dc_record->{$f}, found => $reg_info{$f} });
          }
          # else: record matches, that's ok
        }
        else
        {
          push (@problems, { problem => 'missing', field => $f });
        }
      }

      if (@problems)
      {
        report_problem( { area => 'datacite', problem => 'datacite_data_missmatch', utheses_id => $utheses_id, doi => $doi, problems => \@problems } );
        push(@actions, 'problem report datacite');
        $metrics{ut1_datacite_data_missmatch}++;
        return (@actions);
      }

      if ($dc_record->{xml_md5} eq $xml_md5 && $dc_record->{reg_status} == 1)
      {
        print __LINE__, " datacite metadata unchanged; not updating!\n";
        push (@actions, 'datacite doi metadata unchanged');
        $metrics{ut1_datacite_doi_metadata_unchanged}++;
        return @actions;
      }
    }

    ($ok, $verdict, $msg1, $msg2)= register_doi_with_DataCite_for_utheses($row, $xml);
    print __LINE__, " ok=[$ok] msg1=[$msg1] msg2=[$msg2] verdict=[$verdict]\n";
    push (@actions, (($ok) ? 'datacite doi registration ok' : 'datacite doi registration failed'), $verdict);

    $reg_info{reg_status}= $ok;
    $reg_info{reg_verdict}= $verdict,
    $reg_info{reg_message1}= $msg1;
    $reg_info{reg_message2}= $msg2;

    print __LINE__, " insert into datacite; reg_info: ", Dumper(\%reg_info);
    my $res= $dc_col->update( { doi => $doi }, \%reg_info, { upsert => 1 } );
    print __LINE__, " insert res: ", Dumper($res);

    if ($ok)
    {
      $metrics{ut1_datacite_doi_registration_ok}++;
    }
    else
    {
      report_problem( { area => 'datacite', problem => 'registration failure', utheses_id => $utheses_id, doi => $doi, reg_info => \%reg_info } );
      $metrics{ut1_datacite_doi_registration_failure}++;
    }
  }

  @actions;
}

sub process_ac_number
{
  my $ac_number= shift;

  my $row= {};
  my @actions= analyze_marc_record($row, $ac_number);

  if (exists($row->{val_utheses}))
  {
    my $ut_link= $row->{val_utheses};
    if ($ut_link =~ m#https?://utheses.univie.ac.at/detail/(\d+)[\#/]?#)
    {
      my $utheses_id= $1;
      my ($error_code, $status, $xml)= get_utheses_metadata($row, $utheses_id);
      $metrics{ut1_alma_utheses_link_ok}++;
    }
    else
    {
      # TODO: report bad link in Alma
      $metrics{ut1_alma_utheses_link_bad}++;
    }
  }

  print __LINE__, " process_ac_number: ac_number=[$ac_number] row: ", Dumper($row);
}

sub analyze_marc_record
{
  my $row= shift;
  my $ac_number= shift || $row->{ac_number};

  unless (defined ($marc_db))
  {
    return ($row->{marc_record}= 'no_marc_db');
    $metrics{ut1_no_marc_db}++;
  }
  unless ($ac_number =~ m#^AC\d{8}$#)
  {
    return ($row->{marc_record}= 'invalid_ac_number');
    $metrics{ut1_invalid_ac_number}++;
  }

  my @actions= ();
  my $marc_rec= $marc_col->find_one({ ac_number => $ac_number });
  print __LINE__, " marc_rec: ", Dumper($marc_rec);
  my $request_marc_rec= 0;

  if (defined ($marc_rec))
  {
    my $marc_fetched= $marc_rec->{fetched};

    my $best_before= $marc_fetched + $MAX_MARC_AGE;
    my $now= time();
    print __LINE__, " marc_fetched=[$marc_fetched] best_before=[$best_before] now=[$now]\n";

    if ($best_before > $now)
    {
      $row->{marc_record}= 'ok';
      $metrics{ut1_alma_marc_record_fresh}++;
    }
    else
    {
      print __LINE__, " marc_record too old\n";
      $row->{marc_record}= 'too_old';
      $request_marc_rec= 1;
      $metrics{ut1_alma_marc_record_requested}++;
    }

    # check marc record, even when it is too old
    $row->{marc}= my $x= {};
    $mex->extract_identifiers($marc_rec, $x);
    push (@actions, 'marc_rec_checked');
  }
  else
  {
    $row->{marc_record}= 'not_found';
    $request_marc_rec= 1;
    $metrics{ut1_alma_marc_record_not_found}++;
  }

  if ($request_marc_rec)
  {
    my $req=
    {
      agent        => 'alma_cat',
      status       => 'new',
      action       => 'update_alma_2xml',
      ac_number    => $ac_number,
      requested_by => $agent_name,
    };
    $req_col->insert ($req);

    push (@actions, 'marc_rec_requested');
    print __LINE__, " analyze_marc_record: insert requests: ", Dumper($req);
  }

  @actions;
}

sub register_doi_with_DataCite_for_utheses
{
  my $row= shift;
  my $xml= shift;

  my ($rejected, $accepted);
  my $ok= 0;

  unless (defined ($row))
  {
    $rejected= "can't register DOI: no utheses data found";
    goto END;
  }

  unless (defined ($xml))
  {
    $rejected= "can't register DOI: no xml data generated";
    goto END;
  }

  my ($doi, $url, $errors, $pol)= map { $row->{$_} } qw(doi persistent_link datacite_conversion_errors policies);
  my ($ftl, $lks)= map { $pol->{$_} } qw(fulltext_locked lock_status);
  if (!defined ($ftl) || $ftl)
  {
    $rejected= "can't register DOI: fulltext locked ($ftl)";
    goto END;
  }

  if (!defined ($lks) || $lks)
  {
    $rejected= "can't register DOI: lock_status=[$lks]";
    goto END;
  }

  if (@$errors)
  {
    $rejected= "can't register DOI $doi due to errors: ", join(', ', @$errors);
    goto END;
  }

  my ($reg_res, $reg_msg1, $reg_msg2)= $reg_obj->register_doi ($doi, $xml, $url);
  if ($reg_res)
  {
    $accepted= "register_doi doi=[$doi] url=[$url] OK";
    $ok= 1;
  }
  else
  {
    $rejected= "register_doi doi=[$doi] url=[$url] was not ok";
  }

END:

  if ($rejected)
  {
    print "REJECTED: ", $rejected, "\n";
  }
  else
  {
    print "ACCEPTED: ", $accepted, "\n";
  }

  return ($ok, ($rejected || $accepted), $reg_msg1, $reg_msg2);
}

sub fetch_metadata_bulk
{
  my $utheses_ids= shift;

  open (TSV, '>:utf8', $fnm_tsv) or die;
  print TSV join("\t", @TSV_COLUMNS), "\n";
  open (ERRORS, '>>:utf8', '@errors.lst') or die;

  foreach my $utheses_id (@$utheses_ids)
  {
    next if ($utheses_id eq 'utheses_id'); # CSV column name...

    my $row;
    my ($error_code, $status, $xml)= get_utheses_metadata($row, $utheses_id);

    if (defined ($row))
    {
      print TSV join("\t", map { $row->{$_} } @TSV_COLUMNS), "\n";
    }
  }
}

=head2 my ($error_code, $status, $xml)= get_utheses_metadata($row, $utheses_id)

return DataCite_XML document for a given utheses_id and fill in $row
with important information

=cut

sub get_utheses_metadata
{
  my $row= shift;
  my $utheses_id= shift;
  my $ut_data= shift;

  print __LINE__, " utheses_id=[$utheses_id]\n";
  my $info= $ut_api->getContainerPublicMetadata($utheses_id);
  print __LINE__, " info: ", Dumper($info);

  my $xml;
  my $status= $info->{status};
  my $error_code= 'unknown';
  if ($status eq '200')
  {
    $xml= utheses2datacite($row, $info, $utheses_id);
    print __LINE__, " row: ", Dumper($row);
    $row->{xml_md5}= my $md5_hex= md5_hex(encode_utf8($xml));

    if (defined ($row->{doi}))
    {
      if (defined ($xml))
      {
        my $save_xml= 1;
        # print __LINE__, " xml=[$xml]\n";
        $row->{xml_fnm}= my $xml_fnm= 'utheses/DataCite_XML/'. $row->{doi}. '.xml';
        # print __LINE__, " DataCite_XML=[$xml_fnm] xml=[$xml]\n";

        print __LINE__, " ut_data=[$ut_data]\n";
        if (defined ($ut_data))
        { # we have xml info in the database, check if they match
          print __LINE__, " ut_data: ", Dumper($ut_data);
          if (defined (my $xml_fnm= $ut_data->{xml_fnm}) && defined (my $xml_md5= $ut_data->{xml_md5}))
          {
            print __LINE__, " xml_fnm=[$xml_fnm] xml_md5=[$xml_md5] md5_hex=[$md5_hex]\n";
            if ($md5_hex eq $xml_md5)
            {
              $save_xml= 0;
              print __LINE__, " md5 unchanged, will not save it again; save_xml=0\n";
            }
          }
        }

        if ($save_xml)
        {
          print __LINE__, " save_xml=[$save_xml], saving DataCite XML to [$xml_fnm]\n";
          open (XML, '>:utf8', $xml_fnm) or die "can't write to $xml_fnm"; # TODO: or do something else...
          print XML $xml;
          close (XML);

          $row->{xml_md5}= my $md5= file_md5_hex($xml_fnm);
          print __LINE__, " xml_fnm=[$xml_fnm] md5=[$md5]\n";
        }
      }

      if ($row->{datacite_conversion_error_count} == 0)
      {
        $error_code= 'ok';
      }
      else
      {
        printf ERRORS ("utheses_id=%d datacite conversion errors\n", $utheses_id);
        $error_code= 'datacite_conversion_errors';
      }
    }
    else
    {
      printf ERRORS ("utheses_id=%d no doi defined\n", $utheses_id);
      $error_code= 'no_doi_defined';
    }
  }
  else
  {
    printf ERRORS ("utheses_id=%d bad status=%s\n", $utheses_id, $status);
    $error_code= 'bad_status';
  }

  $row->{error_code}= $error_code;
  return ($error_code, $status, $xml);
}

=head2 my $xml= utheses2datacite ($row, $info, $utheses_id)

Transcribe utheses $info for a given utheses_id into xml and return that.
$row is filled with information extracted from $info.

=cut

sub utheses2datacite
{
  my $row= shift;
  my $info= shift;
  my $utheses_id= shift;

  my @datacite_conversion_errors;

  my ($md)= map { $info->{$_} } qw(metadata);
  my ($th, $authors, $phaidra)= map { $md->{$_} } qw(thesis authors phaidra);

  my ($doi, $nbn, $ac_number, $persistent_link)= map { $th->{$_} } qw(doi urn ac_number persistent_link);
  # TODO: check doi, nbn, ac_number for syntactic validity
  my $suffix;
  if ($doi =~ m#^10.25365/(thesis\.\d+)$#)
  {
    $suffix= $1;
    # rewrite DOI, if necessary, e.g. for the test environment
    $doi= join ('/', $ut_cnf->{use_prefix}, $suffix) if (exists($ut_cnf->{use_prefix}));
    # $doi= join ('/', '10.493943', $suffix); # TEST: fake prefix
  }
  else
  {
    push (@datacite_conversion_errors, "bad_doi=[$doi]");
  }
  push (@datacite_conversion_errors, "bad_nbn=[$nbn]") unless ($nbn =~ m#^urn:nbn:at:at-ubw:1-\d{5}\.\d{5}\.\d{6}-\d$#);
  push (@datacite_conversion_errors, "bad_ac_number=[$ac_number]") unless ($ac_number =~ m#^AC\d{8}$#);

  push (@datacite_conversion_errors, "bad_url=[$persistent_link]") unless ($persistent_link =~ m#^https://utheses.univie.ac.at/detail/\d+/?$#);

  my ($titles, $abstracts, $publication_date, $langs, $policies)= map { $th->{$_} } qw(titles abstracts publication_date languages policies);

  my $publication_year;
  if ($publication_date =~ m#^(\d{4})-?#)
  {
    $publication_year= $1;
  }
  else
  {
    push (@datacite_conversion_errors, "bad_pub_date=[$publication_date]");
  }

  my $main_language;
  foreach my $lang (@$langs)
  {
    my $language= Phaidra::Utils::iso639::iso_639_2_to_1($lang);
    if (defined ($language))
    {
      $main_language= $language unless (defined ($main_language));
    }
    else
    {
      push (@datacite_conversion_errors, "bad_language=[$lang]");
      $language= ':tba';
    }
  }

  my $xml= << "EOX";
<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">$doi</identifier>
  <creators>
EOX

  # $xml .= "  <urxn>bla</urxn>\n";  # TEST: check what happens with invalid XML

  foreach my $author (@$authors)
  {
    my ($family, $given)= map { xml_escape($_) } ($author->{family_name}, $author->{given_name});
    $xml .= << "EOX";
    <creator>
      <creatorName>$family, $given</creatorName>
    </creator>
EOX
  }

  $xml .= << "EOX";
  </creators>
  <titles>
EOX

  foreach my $title (@$titles)
  {
    my $language= Phaidra::Utils::iso639::iso_639_2_to_1(my $tl= $title->{title_lang});
    unless (defined ($language))
    {
      push (@datacite_conversion_errors, "bad_title_language=[$tl]");
      $language= ':tba';
    }
    my $tt= 'titleType="TranslatedTitle" ' if ($title->{type} eq 'parallel');

    my $text= xml_escape($title->{title_text});
    $xml .= <<"EOX";
    <title ${tt}xml:lang="$language">
$text
    </title>
EOX
  }

  $xml .= << "EOX";
  </titles>
  <publisher>:none</publisher>
  <publicationYear>$publication_year</publicationYear>
  <language>$main_language</language>
  <resourceType resourceTypeGeneral="Text">Thesis</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="URL">$persistent_link</alternateIdentifier>
    <alternateIdentifier alternateIdentifierType="URN">$nbn</alternateIdentifier>
  </alternateIdentifiers>
  <descriptions>
EOX

  foreach my $abstract (@$abstracts)
  {
    my $language= Phaidra::Utils::iso639::iso_639_2_to_1(my $al= $abstract->{language});
    unless (defined ($language))
    {
      push (@datacite_conversion_errors, "bad_abstract_language=[$al]");
      $language= ':tba';
    }

    # TODO: text should be escaped, if necessary
    my $text= xml_escape($abstract->{text});
    $xml .= << "EOX";
    <description descriptionType="Abstract" xml:lang="$language">
$text
    </description>
EOX
  }

  $xml .= << "EOX";
  </descriptions>
</resource>
EOX

  $row->{utheses_fetched}= time();
  $row->{utheses_id}= $utheses_id;
  $row->{suffix}= $suffix;
  $row->{doi}= $doi;
  $row->{nbn}= $nbn;
  $row->{ac_number}= $ac_number;
  $row->{langs}= join(',', @$langs);
  $row->{language}= $main_language;
  $row->{persistent_link }=$persistent_link;
  $row->{datacite_conversion_errors}= \@datacite_conversion_errors;
  $row->{datacite_conversion_error_count}= scalar @datacite_conversion_errors;
  $row->{phaidra}= $phaidra;   # ... mapped, see below
  $row->{policies}= $policies; # ... mapped, see below

  # foreach my $an (qw(fulltext_locked lock_status lock_until_date)) { $row->{$an}= $policies->{$an} }
  # foreach my $an (qw(container_pid container_status thesis_doc_pid thesis_doc_status)) { $row->{$an}= $phaidra->{$an} }

  $xml;
}

sub xml_escape
{
  my $s= shift;

  $s=~ s/&/&amp;/g;
  $s=~ s/</&lt;/g;
  $s=~ s/>/&gt;/g;

  $s;
}

sub report_problem
{
  my $problem= shift;

  $problem->{ts_iso_gmt}= Util::ts::ts_ISO_gmt();

  my $area= $problem->{area};
  my $check_id;
     if ($area eq 'utheses')  { $check_id= 'utheses_id' }
  elsif ($area eq 'marc')     { $check_id= 'ac_number' }
  elsif ($area eq 'datacite') { $check_id= 'doi' }

  $problems_col->update( { area => $area, $check_id => my $id= $problem->{$check_id} }, $problem, { upsert => 1 } ); # replace problem report, if one already exists

  print __LINE__, " ATTN: problem reported for $check_id=$id: ", Dumper($problem);
}

sub remove_problem
{
  my $area= shift;
  my $id= shift;
  my $check_id= shift;

  unless (defined ($check_id))
  {
       if ($area eq 'utheses')  { $check_id= 'utheses_id' }
    elsif ($area eq 'marc')     { $check_id= 'ac_number' }
    elsif ($area eq 'datacite') { $check_id= 'doi' }
  }

  $problems_col->remove({ area => $area, $check_id => $id });
}

sub write_metrics
{
  my $metrics= shift;
  open (FO, '>:utf8', $nx_metrics_temp) or die "can't write to $nx_metrics_temp";
  my $now= time();
  print FO <<"EOX";
# HELP ut1_last_run last time ut1 ran
# TYPE ut1_last_run counter
ut1_last_run $now
EOX

  foreach my $mn (sort keys %$metrics)
  {
    my $cnt= $metrics->{$mn};
    print FO <<"EOX";
# HELP $mn ut1 gauge
# TYPE $mn gauge
$mn $cnt
EOX
  }

  close(FO);

  rename($nx_metrics_temp, $nx_metrics_prom);
  print __LINE__, " metrics written to $nx_metrics_prom\n";
}

__END__

=head1 TODO

* handle signals like SIGINT


