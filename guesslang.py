#!/usr/bin/python3

from guess_language import guess_language
from argparse import ArgumentParser, FileType
import sys
import signal


__authors__ = ["GG"]
__date__ = 20191031
__description__ = 'A simple pdf example'

def guesslang(fnm):

  fin= open(fnm, 'rt')
  text= fin.read()
  fin.close()

  pages= text.split("\f")

  # Note: there should not be anything behind the last Form Feed character
  lastpage= pages.pop()
  # print ('--- [lastpage] ----------------------------------------\n', lastpage)
  lastlang= guess_language(lastpage)

  page_num= 1
  for page in pages:
    # print ('--- [page ', page_num, '] ----------------------------------------\n', page)
    lang= guess_language(page)
    print(lang, ' ', page_num, ' ', fnm)
    page_num= page_num+1

  if (lastlang != 'UNKNOWN'):
    print(lastlang, ' ', page_num+1, ' ', fnm)

s = signal.signal(signal.SIGINT, signal.SIG_IGN)

argc= len(sys.argv)
# print(argc)
if argc > 1:
  for i in range(1,argc):
    guesslang(sys.argv[i])
else:
  text= sys.stdin.read()
  lang= guess_language(text)
  print(lang, ' ', 'stdin')

signal.signal(signal.SIGINT, s)

