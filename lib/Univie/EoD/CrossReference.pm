package Univie::EoD::CrossReference;

use strict;

use Data::Dumper;

use JSON;
use IRMA::db;
use Util::ts;

my @foxml_columns= qw(ownerId state model ac_number aleph_url pid);
my @marc_fields= qw(ac_number mms_id fetched lib_code);

sub new
{
  my $class= shift;

  my $self=
  {
    ac_numbers => {},
    pids => {},
    books => [],
  };

  bless ($self, $class);
  $self->set(@_);

  $self;
}

sub set
{
  my $self= shift;
  my %par= @_;
  foreach my $par (keys %par)
  {
    $self->{$par}= $par{$par};
  }
  $self;
}

sub get_book_list
{
  my $self= shift;
  $self->{books};
}

sub get_db_col
{
  my $self= shift;
  my $db= shift;
  my $col_name= shift;

  return $self->{_db}->{$db}->{$col_name} if (defined ($self->{_db}->{$db}->{$col_name}));

  my $inv_db=  IRMA::db::get_any_db($self->{agent_cnf}, $db);
  $self->{_db}->{$db}->{$col_name}= my $col= $inv_db->get_collection($col_name);
  print __LINE__, " db=[$db] col_name=[$col_name] col=[$col]\n";
  # print __LINE__, " col: ", Dumper($col);
  $col;
}

sub get_book_inventory
{
  my $self= shift;
  my $search= { ownerId => $self->{agent_cnf}->{ownerId}, state => 'Active', model => 'Book' };
  $self->find_books($search);
}

sub get_book_by_ac_number
{
  my $self= shift;
  my $ac_numbers= shift;

  my $search= { ownerId => $self->{agent_cnf}->{ownerId}, state => 'Active', model => 'Book' };
  _modify_search ($search, 'ac_number', $ac_numbers);
  $self->find_books($search);
}

sub get_book_by_pid
{
  my $self= shift;
  my $pids= shift;

  my $search= { ownerId => $self->{agent_cnf}->{ownerId}, state => 'Active', model => 'Book' };
  _modify_search ($search, 'pid', $pids);
  $self->find_books($search);
}

sub _modify_search
{
  my $s= shift;
  my $what= shift;
  my $val= shift;

  if (ref($val) eq 'ARRAY') { $s->{$what} = { '$in' => $val } }
  else { $s->{$what}= $val; }
}

sub find_books
{
  my $self= shift;
  my $search= shift;
  # print __LINE__, " search: ", join(', ', %$search), "\n"; ... can contain {$in:[...]} clauses!
  # print __LINE__, " search: ", Dumper ($search);
  print __LINE__, " search: ", JSON::encode_json ($search), "\n";

  my $foxml_col= $self->get_db_col('inventory_database', 'foxml.data');
  my $cur= $foxml_col->find( $search );
  # print __LINE__, ' cur: ', Dumper($cur);

  FOXML_REC: while (my $foxml_rec= $cur->next())
  {
    # print __LINE__, " foxml_rec: foxml_rec=[$foxml_rec] found:", Dumper($foxml_rec);
    if (exists ($self->{agent_cnf}->{prefiltered_pids}->{$foxml_rec->{pid}}))
    {
      print __LINE__, " NOTE: prefiltered pid; foxml_rec: ", Dumper($foxml_rec);
      next FOXML_REC;
    }

    # print __LINE__, " foxml_rec=[$foxml_rec] added: ", Dumper($foxml_rec);
    $self->add_book($foxml_rec);
  }
}

sub add_book
{
  my $self= shift;
  my $foxml_rec= shift;

  # print __LINE__, " foxml_rec: ", Dumper($foxml_rec);
  my %book= map { $_ => $foxml_rec->{$_} } @foxml_columns;
  # print __LINE__, " book: ", Dumper(\%book);

  $book{phaidra_url}= 'https://phaidra.univie.ac.at/'. $book{pid};
  push (@{$self->{ac_numbers}->{$book{ac_number}}} => \%book);
  $self->{pids}->{$book{pid}}= \%book;

  push (@{$self->{books}}, \%book);

  \%book;
}

sub flag_duplicate_ac_numbers
{
  my $self= shift;

  my $ac_numbers= $self->{ac_numbers};
  print __LINE__, " checking for duplicate ac_numbers\n";
  foreach my $ac_number (keys %$ac_numbers)
  {
    my $x= $ac_numbers->{$ac_number};
    # print __LINE__, " ac_number=[$ac_number] pids=[", join(', ', map { $_->{pid} } @$x), "]\n";

    if (@$x != 1)
    { # either this is a duplicate or a member of a collection (ZS) where the Alma record should point to the collection instead
      print __LINE__, " duplicate_ac_number=[$ac_number] pids=[", join(', ', map { $_->{pid} } @$x), "]\n";

      # TODO: find out, why this is a duplicate, possibly do not mark the "canonical" version which should be identified in a ticket or so
      # for now, just leave them out of further processing.
      foreach my $book (@$x)
      {
        $book->{ac_number_note}= 'dup';
      }
    }
  }
}

sub check_book
{
  my $self= shift;
  my $book= shift;

  my @irma_notes= ();
  my ($ac_number, $pid, $ac_number_note)= map { $book->{$_} } qw(ac_number pid ac_number_note);

  unless (defined ($ac_number) && $ac_number =~ m#^AC\d{8}$#)
  {
    $book->{problem}= "invalid ac_number=[$ac_number]";
    $book->{problem}= 
    return ('invalid_ac_number');
  }

  return 'ac_number_note'      if ($ac_number_note ne '');
  return 'filtered_ac_numbers' if (exists ($self->{agent_cnf}->{filtered_ac_numbers}->{$ac_number}));
  return 'filtered_pids'       if (exists ($self->{agent_cnf}->{filtered_pids}->{$pid}));

  # check tickets
  my $ticket_col= $self->get_db_col('stage_database', 'eod.tickets');
  my @tickets= $ticket_col->find({ ac_number => $ac_number })->all();
  print __LINE__, " ac_number=[$ac_number] tickets: ", scalar @tickets, "\n";
  # print __LINE__, ' ', Dumper(\@tickets);
  if (@tickets  > 1)
  {
    $book->{ticket}= join(',', map { $_->{ticket} } @tickets);
    return 'multiple_tickets';
  }
  elsif (@tickets == 1)
  {
    my $t0= @tickets[0];
    $book->{ticket}= $t0->{ticket};
    $book->{ticket_status}= $t0->{status};
    $book->{ticket_pid}= $t0->{custom_fields}->{ph_pid}->[1];
    $book->{ticket_url}= $t0->{custom_fields}->{ph_url}->[1];
    $book->{vt}=         $t0->{custom_fields}->{vt}->[1];
  }

  # check IRMA for registered identifiers
  my $irma_col= $self->get_db_col('stage_database', 'irma.map');
  my @irma_records= $irma_col->find({ ac_number => $ac_number })->all();
  # my @irma_records= $irma_col->find({ pid => $pid })->all(); # NOTE: there is no field named "pid" in the irma record!
  print __LINE__, " ac_number=[$ac_number] irma_records: ", scalar @irma_records, "\n";
  # print __LINE__, Dumper(\@irma_records);
  if (@irma_records > 1) { return 'multiple_irma_records' }
  elsif (@irma_records == 1)
  {
    my $ir0= $irma_records[0];
    foreach my $field (qw(hdl urn doi))
    {
      $book->{$field}= $ir0->{$field} if (exists ($ir0->{$field}));
    }
  }
  elsif (@irma_records == 0)
  {
    print __LINE__, " ATTN: ac_number=[$ac_number] no irma_record!\n";
    $self->{counters}->{missing_irma_record}++;
  }

  my $marc_col= $self->get_db_col('stage_database', 'alma.marc');
  my $marc= $marc_col->find_one({ ac_number => $ac_number });
  # print __LINE__, " marc: ", Dumper($marc);
  return 'missing_marc_record' unless (defined ($marc));

  my @alma_notes= ();
  $book->{ts_fetched}= Util::ts::ts_ISO_gmt($marc->{fetched});

  my $mex= Alma::MARC_Extractor->new(\@marc_fields);
  $mex->{mex_ot2ut}= 0; $mex->{mex_phaidra}= 1;
  $mex->extract_identifiers2($marc, $book);

  if (exists($book->{mex}))
  {
    my $mex= $book->{mex};
    # print __LINE__, " ac_number=[$ac_number] mex: ", Dumper($mex);
    foreach my $mf (keys %$mex)
    {
      my $mfa= $mex->{$mf};

      if (@$mfa != 1)
      { # this should be inspected
        print __LINE__, " ATTN: ac_number=[$ac_number] multiple mex entries for $mf; mex: ", Dumper($mex);
        return 'multiple_mex_entries';
      }

      foreach my $mfe (@$mfa)
      {
        my ($df, $val)= @$mfe;
        my $copy= 0;
        if ($mf eq 'phaidra')
        {
          if ($val =~ m#^https?://phaidra.univie.ac.at/(.*)(o:\d+)$#)
          {
            $copy= 1; # only transcribe those URLs that really look like some
            my ($extra, $found_pid)= ($1, $2);
            my $correct_url= 'https://phaidra.univie.ac.at/'. $found_pid;
            if ($correct_url ne $book->{phaidra_url} || $extra ne '')
            {
              $book->{update_phaidra_url}= $book->{phaidra_url};
              $self->{counters}->{update_phaidra_url}++;
              push(@alma_notes, 'update_phaidra_url');
					  }
          }
        }
        elsif ($mf eq 'hdl')
        {
          if ($val =~ m#^11353/10\.(\d+)$#)
          {
            $copy= 1; # only transcribe those URLs that really look like some
            if ($book->{hdl} && $val ne $book->{hdl})
            { # TODO:
              $book->{update_hdl}= $book->{hdl};
              $self->{counters}->{update_hdl}++;
              push(@alma_notes, 'update_hdl');
            }
          }
        }
        elsif ($mf eq 'doi')
        {
          if ($val =~ m#^10.25365/digital-copy\.(\d+)$#)
          {
            $copy= 1; # only transcribe those URLs that really look like some
            # TODO: DOI checken
            if ($val ne $book->{doi})
            { # TODO:
              $book->{update_doi}= $book->{doi};
              $self->{counters}->{update_doi}++;
              push(@alma_notes, 'update_doi');
            }
          }
          else
          {
            print __LINE__, " ATTN: unknown DOI format in marc record: df=[$df] val=[$val]\n";
            $self->{counters}->{bad_doi}++;
          }
        }
        elsif ($mf eq 'urn')
        {
          if ($val =~ m#^urn:nbn:at:at-ubw-\d{5}\.\d{5}\.\d{6}-\d$#)
          {
            $copy= 1; # only transcribe those URLs that really look like some
            # TODO: check if this is the known nbn!
            if ($val ne $book->{urn})
            { # TODO:
              $book->{update_urn}= $book->{urn};
              $self->{counters}->{update_urn}++;
              push(@alma_notes, 'update_urn');
            }
          }
          else
          {
            print __LINE__, " ATTN: unknown NBN (urn) format in marc record: df=[$df] val=[$val]\n";
            $self->{counters}->{bad_urn}++;
          }
        }

        if ($copy)
        {
          $book->{'df_'.$mf}= $df;
          $book->{'val_'.$mf}= $val;
        }
        else
        {
          push(@alma_notes, "junk_${mf}_in_marc");
          print __LINE__, " ATTN: ac_number=[$ac_number] pid=[$pid] junk data mf=[$mf] in marc record: df=[$df] val=[$val]\n";
        }
      }
    }
  }
  else
  {
    # this is ok, not a bug, Alma simply does not know anything about this phaidra object
    print __LINE__, " ac_number=[$ac_number] mex missing\n";
  }

  if (!exists($book->{val_urn}) && exists ($book->{urn}))
  {
    $book->{update_urn}= $book->{urn};
    $self->{counters}->{set_urn}++;
    push(@alma_notes, 'set_urn');
  }

  if (!exists($book->{val_hdl}) && exists ($book->{hdl}))
  {
    $book->{update_hdl}= $book->{hdl};
    $self->{counters}->{set_hdl}++;
    push(@alma_notes, 'set_hdl');
  }

  unless (exists($book->{df_phaidra}))
  { # TODO: if there is no phaidra_url at all, set it...
    $self->{counters}->{set_phaidra_url}++;
    $book->{update_phaidra_url}= $book->{phaidra_url};
    push(@alma_notes, 'set_phaidra_url');
  }

  push (@alma_notes, 'ok') unless (@alma_notes);
  $book->{alma_notes}= join(',', @alma_notes);

  return 'ok';
}

=head1 INTERNAL? METHODS

=cut

1;

