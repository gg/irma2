
package Univie::Utheses::Container;

sub new
{
  my $class= shift;

  my $self=
  {
    public => {},
    private => {},
  };

  bless $self, $class;
  $self->set(@_);
  $self;
}

sub set
{
  my $self= shift;
  my %par= @_;
  foreach my $par (keys %par)
  {
    $self->{$par}= $par{$par};
  }
  $self;
}

sub public
{
  my $self= shift;
  my $elment= shift;
  my $data= shift;

  $self->{public}->{$elment}= $data;
}

1;

__END__
