#!/usr/bin/perl

use strict;
use utf8;

package Univie::Utheses::API;

use base 'Module';

use LWP;
use JSON -convert_blessed_universally;
use Encode;

=head2 my $utapi= new Univie::Utheses::API( api_url => 'https://utheses-api-utheses-prod.cprod.univie.ac.at/' );

=cut

sub getContainerPublicMetadata
{
  my $self= shift;
  my $utheses_id= shift;

  my ($code1, $res1, $info)= $self->utheses_request('GET', 'container/get/public', $utheses_id);
  print __LINE__, " code1=[$code1] res1=[$res1] info=[$info]\n";

  # TODO: Error handling!
  $info;
}

sub getPendingDoisCreateRequest
{
  my $self= shift;

  my ($code1, $res1, $info)= $self->utheses_request('GET', 'doi/get/createRequest', undef, );
  # print __LINE__, " code1=[$code1] res1=[$res1] info=[$info]\n";

  # TODO: Error handling!
  (wantarray) ? ($code1, $res1, $info) : $info;
}

sub utheses_request
{
  my $self=   shift;
  my $method= shift;
  my $what=   shift;
  my $par=    shift;

  my ($api_url, $headers)= map { $self->{config}->{$_} } qw(api_url headers);

  my $req_url= join ('/', $api_url, $what, $par);
  print __LINE__, " req_url=[$req_url]\n";
  my $req = HTTP::Request->new( $method => $req_url );
  if (defined ($headers))
  {
    foreach my $h (@$headers)
    {
      $req->header(@$h);
    }
  }

  my $ua= LWP::UserAgent->new;
  my $res= $ua->request($req);

  # print __LINE__, " res: ", main::Dumper($res);
  my $txt= decode("utf8", $res->content());
  my $code= $res->code();

  my $info;
  if ($code =~ m#^2#)
  {
    eval
    {
      $info= from_json($txt);
    };
    if ($@)
    {
      die $@; # TODO: this should be handled mor gracefully!
    }
  }

  return ($code, $txt, $info);
}

1;

