package IRMA::NA;

use strict;

use parent 'IRMA::db';

=begin comment

not used any more, should be removed ...

sub fetch_eprints_data
{
  my $self= shift;

  my $m= $self->connect();
  # print "m: ", main::Dumper($m);
  # $m->show_query(1);

  my $res= $m->get_all_x ('eprints');
  # print "res: irma-na eprints: ", main::Dumper($res);
  $res;
}

=end comment
=cut

sub fetch_ac_data
{
  my $self= shift;

  my $m= $self->connect();
  # print "m: ", main::Dumper($m);
  # $m->show_query(1);

  my $res= $m->get_all_x ('ac_numbers');
  # print "res: irma-na ac_numbers: ", main::Dumper($res);
  $res;
}

sub update_from_eprints
{
  my $self= shift;
  my $epr_res= shift;

  foreach my $eprint_id (sort { $a <=> $b } keys %$epr_res)
  {
    my $row= $epr_res->{$eprint_id};
    print "row: ", main::Dumper ($row);
  }
}

1;

