package IRMA::eprints;

use strict;

use parent 'IRMA::db';

my %classifications= ();
my $classifications_processed= 0;

sub table_filter
{
  my $table_name= shift;
  return 0 if ($table_name =~ m#^cache\d+$#);
  return 0 if ($table_name =~ m#_(en|de)$#);
  return 0 if ($table_name =~ m#__(index|index_grep|rindex)$#);
  # print "table_filter: table_name=[$table_name]\n";
  return 1;
}

=head2 $epr->fetch_data()

retrieve all data from eprints application (table eprint)

=cut

sub fetch_data
{
  my $self= shift;
  my $eprint_status= shift || 'archive';
  my $other_conditions= shift;

  my $m= $self->connect();

  # my $res2= $m->desc('eprint');
  # print "eprint table definition: ", Dumper ($res2);

  # print "m: ", main::Dumper ($m);

  my $conditions= 'eprint_status=?';
  foreach my $c (keys %$other_conditions)
  {
    $conditions .= ' AND doi IS NOT NULL' if ($c eq 'doi' &&  $other_conditions->{doi});
    $conditions .= ' AND doi IS NULL'     if ($c eq 'doi' && !$other_conditions->{doi});
  }

  # $m->show_query(1);
  my $res= $m->get_all_x ('eprint', [$conditions, $eprint_status],
                          'eprintid,eprint_status,ac_nummer,type,matr,urn,uri,sperre,einverstaendnis,rev_number'
                         );
  # print "inbox res: ", main::Dumper ($res);

  $res;
}

sub get_eprint_ids_for_block
{
  my $self= shift;
  my $block= shift;

  my $begin= $block*100;
  my $end= $begin+100;

  my $m= $self->connect();

  my $conditions= 'eprintid >= ? AND eprintid < ? AND ((eprint_status="archive" AND urn IS NOT NULL) OR eprint_status="buffer")';

  # $m->show_query(1);
  my $res= $m->get_all_x ('eprint', [$conditions, $begin, $end], 'eprintid');
  # print "inbox res: ", main::Dumper ($res);

  my @eprint_ids= map { $_.'' } sort { $a <=> $b } keys %$res;

  (wantarray) ? @eprint_ids : \@eprint_ids;
}

sub fetch_metadata
{
  my $self= shift;

  my $m= $self->connect();

  # my $res2= $m->desc('eprint');
  # print "eprint table definition: ", Dumper ($res2);

  # print "m: ", main::Dumper ($m);

  my @columns_metadata= qw(
    eprintid ac_nummer urn doi thesis_type dir fileinfo
    sprache
    title title_zusatz title_eng title_ger
    abstract abstract_eng
    keywords keywords_eng
    date_year place_of_pub country
    lastmod_year lastmod_month lastmod_day lastmod_hour lastmod_minute lastmod_second
  );

  my @conditions=
  (
    'eprint_status="archive"',
    'metadata_visibility="show"',
    'date_sperre_year IS NULL',
    'einverstaendnis="TRUE"',
    'full_text_status="public"',
    '(abstract_nicht_anzeigen IS NULL OR abstract_nicht_anzeigen<>"TRUE")',   # otherwise there are NULLs and empty strings
    'urn IS NOT NULL',
    'doi IS NULL',  # TODO: this should be optional, e.g. only when DOIs are generated; if metadata for phaidra is produced, the condition woould be 'doi IS NOT NULL'
  );

  # my $columns_metadata= join(',', @columns_metadata);

  $m->show_query(1);
  my $res= $m->get_all_x ('eprint', [ join(' and ', @conditions) ], join(',', @columns_metadata));

  # print "inbox res: ", main::Dumper ($res);

  $res;
}

=head2 get_eprints_urls($id)

returns list of possible eprints/othes URLs for a given id, ordered by preference, if that's relevant

=cut

sub get_eprint_urls
{
  # my $self= shift; # TODO ...
  my $id= shift;

  return undef unless ($id =~ /^\d+$/);

  return ("https://othes.univie.ac.at/$id/",
          "https://othes.univie.ac.at/$id",
          "http://othes.univie.ac.at/$id/",
          "http://othes.univie.ac.at/$id",
          # "http://othes.univie.ac.at//$id/", # Duh!
          # "http://othes.univie.ac.at//$id",  # Duh!
         );
}

=head2 fetch_eprint_app_data($epr)

  $epr .. IRMA::eprints object

NOTE: This is *very* specific to OTHES !

Retrieves only the good rows from the application database and returns
three hashes with the good rows, the bad rows and the student registration
numbers:

  my $res=
  {
    ok     => \%ok,     # hashed by ac_number
    errors => \%errors, # hashed by eprint_id, list of errors
    matr   => \%matr,   # hashed by matrnr, contains a list of eprint_ids
  };

=cut

sub fetch_eprint_app_data
{
  my $epr= shift;

  my $epr_res= $epr->fetch_data();
  # print "epr_res: ", Dumper($epr_res);

  my (%ac_number, %matr, %urn);
  my (%err_ac_number, %errors);
  EPR_ITEM: foreach my $epr_id (keys %$epr_res)
  {
    my $row= $epr_res->{$epr_id};
    my ($type, $matr, $ac_number, $urn, $status)= map { $row->{$_} } qw(type matr ac_nummer urn eprint_status);

    unless ($status eq 'archive') # NOTE: $epr->fetch_data() (currently) only selects rows where eprint_status="archive"
    {
      push (@{$errors{$epr_id}}, "unknown status=[$status]");
      next EPR_ITEM;
    }

    unless ($type eq 'thesis')
    {
      push (@{$errors{$epr_id}}, "unknown type=[$type]");
      next EPR_ITEM;
    }

    unless ($matr =~ m#^\d{8}$# || $matr eq '') # allow empty matr as well
    {
      push (@{$errors{$epr_id}}, "malformed matr=[$matr]");
      next EPR_ITEM;
    }

    unless ($ac_number =~ m#^AC\d{8}$#)
    {
      push (@{$errors{$epr_id}}, "malformed ac_number=[$ac_number]");
      next EPR_ITEM;
    }

    if (exists ($ac_number{$ac_number}))
    {
      push (@{$errors{$epr_id}}, "duplicate ac_number=[$ac_number]");
      push (@{$err_ac_number{$ac_number}}, "duplicate: $epr_id");
      next EPR_ITEM;
    }

    push (@{$matr{$matr}}, $epr_id);
    $ac_number{$ac_number}= $epr_id;

    # print "epr row: ", Dumper ($row);
  }

  foreach my $ac_number (keys %err_ac_number)
  {
    delete ($ac_number{$ac_number});
  }

  my %ok;
  foreach my $ac_number (keys %ac_number)
  {
    my $epr_id= $ac_number{$ac_number};
    $ok{$ac_number}= $epr_res->{$epr_id};
  }

  my $res=
  {
    ok     => \%ok,
    errors => \%errors,
    matr   => \%matr,
  };
  # print "fetch_eprint: errors: ", Dumper($res->{errors});
  # print "fetch_eprint: res: ", Dumper($res);

  $res;
}

sub get_creators
{
  my $self= shift;
  my $eprintid= shift;

  my $epr_db= $self->connect();

  my $names= $epr_db->get_all_x('eprint_creators_name', ['eprintid=?', $eprintid], 'eprintid,pos,creators_name_given,creators_name_family');
  my @names_keys= sort { $a cmp $b } keys %$names;
  # next ITEM unless (@names_keys > 1); # only items with multiple authors

    print __LINE__, " names: ", main::Dumper($names);
    print __LINE__, " names_keys: ", join('/', @names_keys), "\n";

  my @creators;
  foreach my $names_key (@names_keys)
  {
    my $n= $names->{$names_key};

    my ($id,$pos)= split(',',$names_key);
    $creators[$pos]= { family_name => $n->{creators_name_family}, given_name => $n->{creators_name_given} };
  }

  my $creators_xml;
  foreach my $creator (@creators)
  {
    next unless (defined($creator));
    $creators_xml .= <<"EOX";
    <creator>
      <creatorName>$creator->{family_name}, $creator->{given_name}</creatorName>
    </creator>
EOX
  }

  ($creators_xml, \@creators);
}

sub get_classification_table
{
  my $self= shift;
  my $force= shift;

  return \%classifications if (!$force && $classifications_processed);

print __LINE__, " sub get_classification_table\n";
  my $epr_db= $self->connect();

  my $s_names=   $epr_db->get_all_x('subject_name_name', undef, 'subjectid,name_name'); # NOTE: column pos is always 0
  my $s_lang=    $epr_db->get_all_x('subject_name_lang', undef, 'subjectid,name_lang'); # NOTE: column pos is always 0
  my $s_parents= $epr_db->get_all_x('subject_parents',   undef, 'subjectid,parents');   # NOTE: column pos is always 0

  foreach my $key (keys %$s_names)
  {
    my $v= $s_names->{$key};
    # print __LINE__, " key=[$key] ", main::Dumper($v);
    my $s_id1= $v->{subjectid};
    my ($s_id2, $label)= split (' ', $v->{name_name}, 2);

    if ($s_id1 ne $s_id2)
    {
      print __LINE__, " ATTN: s_id mismatch: ", main::Dumper($v);
      next;
    }

    $classifications{$s_id1}->{label}= $label;
  }

  foreach my $key (keys %$s_lang)
  {
    my $v= $s_lang->{$key};
    # print __LINE__, " key=[$key] ", main::Dumper($v);

    # $classifications{$v->{subjectid}}->{lang}= $v->{name_lang}; it's always de but it should be ger anyway
    $classifications{$v->{subjectid}}->{lang}= 'ger';
  }

  foreach my $key (keys %$s_parents)
  {
    my $v= $s_parents->{$key};
    # print __LINE__, " key=[$key] ", main::Dumper($v);

    $classifications{$v->{subjectid}}->{parents}= $v->{parents};
  }

  $classifications_processed= 1;
  \%classifications;
}

sub get_classifications
{
  my $self= shift;
  my $eprintid= shift;

  my $epr_db= $self->connect();

  my $subject_codes= $epr_db->get_all_x('eprint_subjects', ['eprintid=?', $eprintid], 'eprintid,pos,subjects');

  my $cl= $self->get_classification_table();

  my @errors;
  my @res;
  foreach my $key (keys %$subject_codes)
  {
    my $v= $subject_codes->{$key};
    my ($p, $s)= map { $v->{$_} } qw(pos subjects);
    my $c= $cl->{$s};

    $res[$p]=
    {
      id => $s,
      (map { $_ => $c->{$_} } qw(label lang parents))
    }

  }

  (\@errors, \@res);
}

1;

