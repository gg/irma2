package IRMA::db;

use strict;

my %KNOWN_DB_DRIVERS= map { $_ => 1 } qw(mysql mongodb);

sub new
{
  my $class= shift;
  my %par= @_;

print "IRMA::db::new(): ", join (' ', (map { $par{$_} } qw(driver)), (map { $par{dbcc}->{$_} } qw(host database username))), "\n";
# '|', sort keys %{$par{dbcc}}),"\n";

  my $self= bless {}, $class;
  foreach my $par (keys %par)
  {
    $self->{$par}= $par{$par};
  }
  $self->connect () if (exists ($par{driver}) && exists ($par{dbcc}));

  $self;
}

sub connect
{
  my $self= shift;

# print "IRMA::db::connect()\n";
  my $m= $self->{_db};
  return $m if (defined ($m));

# print __LINE__, " connect(): self=", main::Dumper($self);
  my ($driver, $dbcc)= map { $self->{$_} } qw(driver dbcc);

  if ($driver eq 'mysql')
  {
    $m= new Redmine::DB::MySQL (%$dbcc);

    $m->set (table_filter => $self->{table_filter}) if (exists ($self->{table_filter})); # TODO: transfer list?

    # print "m: ", Dumper ($m);
    $m->connect();

    # my $tables= $m->tables();
    # print "tables: ", Dumper ($tables);
    $m->desc_all() if ($self->{do_desc_all});
  }
  elsif ($driver eq 'mongodb')
  {
    require Util::MongoDB;
    # print "connecting to MongoDB ", main::Dumper ($dbcc);
    $m= Util::MongoDB::connect ($dbcc);
  }
  else
  {
    # TODO: better error handling ...
    die "unknown driver='$driver'";
  }

  $self->{_dbcc}= $dbcc;
  $self->{_db}= $m;

  $m;
}

=head1 MySQL specific

=cut

sub desc_all
{
  my $self= shift;
  my $m= $self->connect();
  $m->desc_all();
}

=head1 MongoDB specific

=cut

sub get_collection
{
  my $self= shift;
  my $col_name= shift;

  my $col= $self->{collections}->{$col_name};
  return $col if (defined ($col));

  my $m= $self->connect();

  my $col= $m->get_collection($col_name);
  $self->{collections}->{$col_name}= $col;

  $col;
}

=head1 Convenience

=cut

sub get_any_db
{
  my $cnf= shift;
  my $cnf_name= shift;

  my ($driver, $dbcc)= get_db_config ($cnf, $cnf_name);
  # print __LINE__, " marc: driver=[$driver] dbcc=", Dumper ($dbcc);

  my $irma_db= new IRMA::db ('driver' => $driver, 'dbcc' => $dbcc);
  # print __LINE__, " irma_db: ", Dumper ($irma_db);
  $irma_db;
}

=head2 get_db_config ($cnf, $db_name)

get database config data from $cnf hash

=cut

sub get_db_config
{
  my $cnf= shift || die "no configuration from ".join (' ', caller());
  my $db_name= shift;

# print "get_db_config: cnf=", main::Dumper($cnf);
  my $dbc= $cnf->{$db_name} || die "no database definition for db_name=[$db_name]";

  my $driver= $dbc->{driver};
  if (exists ($KNOWN_DB_DRIVERS{$driver}))
  {
    my $dbcc= $dbc->{$driver};
    return ($driver, $dbcc);
  }
  else { die "database driver not known; allowed: ". join(', ', keys %KNOWN_DB_DRIVERS); }

  return undef;
}

1;

