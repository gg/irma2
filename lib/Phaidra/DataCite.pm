
=head1 NAME

  Phaidra::DataCite

=head1 DESCRIPTION

Interact with a Phaidra instance to retrieve data relevant for DataCite.


=cut

package Phaidra::DataCite;

use strict;
use utf8;

use base 'Module';

use LWP;
# use Crypt::SSLeay;
use HTTP::Headers::Util;

use XML::LibXML;
use XML::Writer;

my $datacite_ns= 'http://datacite.org/schema/kernel-4';
my $datacite_schemaLocation= 'http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd';

sub get_metadata
{
  my $obj= shift;
  my $pid= shift;
  my $fmt= shift || 'json'; # TODO ...

  my $accept= 'application/json';
  my $url_md= join ('/', $obj->{config}->{api_url}, 'object', $pid, 'datacite');
  
  if ($fmt eq 'xml')
  {
    $url_md .= '?format=xml';
    $accept= 'application/json'; # TODO ???
  }

  print __LINE__, " fmt=[$fmt] url_md=[$url_md]\n";

# build request
  my $headers= HTTP::Headers->new(
    'Accept'  => $accept,
    # 'Content-Type' => $content_type
  );
  
  my $req= HTTP::Request->new('GET' => $url_md, $headers);
  # not needed here $req->authorization_basic($user_name, $user_pw);

  # pass request to the user agent and get a response back
  my $ua= LWP::UserAgent->new;
  my $res= $ua->request($req);
  # print __LINE__, " res=[$res] ", main::Dumper ($res);

  my $code= $res->code();
  my $content= $res->content();
  # print __LINE__, " code=[$code] content=[$content]\n";
  # main::hex_dump ($content);

  # TODO/HACK: fix double UTF8 encoding handed down from PhaidraAPI
  # ...

  ($code, $content);
}

# stolen from PhaidraAPI/Model/Datacite.pm
sub json_2_xml
{
  my $self= shift;
  my $json= shift;

    my $prefixmap = {
      $datacite_ns => 'datacite',
      'http://www.w3.org/2001/XMLSchema-instance' => 'xsi'
    };
    my $forced_declarations = [
      $datacite_ns,
      'http://www.w3.org/2001/XMLSchema-instance'
    ];

    my $xml= <<"EOX";
<?xml version="1.0" encoding="UTF-8"?>
EOX

    my $writer = XML::Writer->new(
      OUTPUT => \$xml,
      # NAMESPACES => 1,
      PREFIX_MAP => $prefixmap,
      # FORCED_NS_DECLS => $forced_declarations,
      DATA_MODE => 1,
      DATA_INDENT => 2,
      ENCODING => 'utf-8'
    );

    $writer->startTag('resource',
                      'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                      'xmlns' => 'http://datacite.org/schema/kernel-4',
                      'xsi:schemaLocation' => $datacite_schemaLocation);

    # print __LINE__, " json: ",main::Dumper($json);
    json_2_xml_rec(undef, $json, $writer);
    $writer->endTag('resource');

    $writer->end();

  return $xml;
}

=head1 INTERNAL FUNCTIONS

=head2 $xml= json_2_xml_rec($json)

=cut

sub json_2_xml_rec
{
  my $parent = shift;
  my $children = shift;
  my $writer = shift;

  foreach my $child (@{$children})
  {
    my $children_size = defined($child->{children}) ? scalar (@{$child->{children}}) : 0;
    my $attributes_size = defined($child->{attributes}) ? scalar (@{$child->{attributes}}) : 0;

    if((!defined($child->{value}) || ($child->{value} eq '')) && $children_size == 0 && $attributes_size == 0)
    {
      next;
    }

    if (defined($child->{attributes}) && (scalar @{$child->{attributes}} > 0))
    {
      my @attrs;
      foreach my $a (@{$child->{attributes}}){
        if (defined($a->{value}) && $a->{value} ne '')
        {
          if ($a->{xmlname} eq 'DOESNOTWORK_lang')
          {
            push @attrs, ['http://www.w3.org/XML/1998/namespace', 'lang'] => $a->{value};
          }
          else
          {
            push @attrs, $a->{xmlname} => $a->{value};
          }
        }
      }

      $writer->startTag($child->{xmlname}, @attrs);
    }
    else
    {
      $writer->startTag($child->{xmlname});
    }

    if ($children_size > 0)
    {
      json_2_xml_rec($child, $child->{children}, $writer);
    }
    else
    {
      $writer->characters($child->{value});
    }

    $writer->endTag($child->{xmlname});
  }
}

1;

