
package Alma::MARC_Extractor;

use strict;

sub new
{
  my $class= shift;
  my $fields= shift;

  my $self=
  {
    fields => $fields,
    mex_ot2ut => 1,
  };

  bless ($self, $class);
  $self;
}

sub extract_identifiers
{
  my $self= shift;

  my $marc= shift; # MARC record to select data from
  my $rec= shift;  # record to fill extracted data into

  foreach my $f (@{$self->{fields}}) { $rec->{$f}= $marc->{$f}; }

  my $mrd= $marc->{xmlref}->{records}->[0]->{record}->[0]->{recordData}->[0]->{record}->[0];
  unless (defined ($mrd))
  {
    $rec->{marc_record}= 'marc_data_not_found';
    next MARC;
  }
  $rec->{marc_record}= 'marc_data_found';

  # print __LINE__, " mrd: ", Dumper($mrd);
  my ($df_urn, $val_urn, $df_doi, $val_doi, $df_hdl, $val_hdl,
      $df_othes, $val_othes, $df_utheses, $val_utheses,
      $df_phaidra, $val_phaidra);

  foreach my $cf (@{$mrd->{controlfield}})
  {
    if ($cf->{tag} eq '005')
    {
      $rec->{ts_marc}= $cf->{content};
    }
  }

  foreach my $df (@{$mrd->{datafield}})
  {
    foreach my $sf (@{$df->{subfield}})
    {
      if ($sf->{content} =~ m#urn:nbn:at:ubw#)
      {
        $df_urn= join(':', (map { $df->{$_} } qw(tag ind1 ind2)), $sf->{code});
        $val_urn= $sf->{content};
      }
      elsif ($sf->{content} =~ m#10\.25365#)
      {
        $df_doi= join(':', (map { $df->{$_} } qw(tag ind1 ind2)), $sf->{code});
        $val_doi= $sf->{content};
      }
      elsif ($sf->{content} =~ m#11353/#)
      {
        $df_hdl= join(':', (map { $df->{$_} } qw(tag ind1 ind2)), $sf->{code});
        $val_hdl= $sf->{content};
      }
      elsif ($sf->{content} =~ m#othes\.univie#)
      {
        $df_othes= join(':', (map { $df->{$_} } qw(tag ind1 ind2)), $sf->{code});
        $val_othes= $sf->{content};
      }
      elsif ($sf->{content} =~ m#utheses\.univie#)
      {
        $df_utheses= join(':', (map { $df->{$_} } qw(tag ind1 ind2)), $sf->{code});
        $val_utheses= $sf->{content};
      }
      elsif ($sf->{content} =~ m#phaidra\.univie#)
      {
        $df_phaidra= join(':', (map { $df->{$_} } qw(tag ind1 ind2)), $sf->{code});
        $val_phaidra= $sf->{content};
      }
    }
  }
  $rec->{ df_urn}=      $df_urn;
  $rec->{val_urn}=     $val_urn;
  $rec->{ df_doi}=      $df_doi;
  $rec->{val_doi}=     $val_doi;
  $rec->{ df_hdl}=      $df_hdl;
  $rec->{val_hdl}=     $val_hdl;

  if ($self->{mex_ot2ut})
  {
    $rec->{ df_othes}=    $df_othes;
    $rec->{val_othes}=   $val_othes;
    $rec->{ df_utheses}=  $df_utheses;
    $rec->{val_utheses}= $val_utheses;
  }

  if ($self->{mex_phaidra})
  {
    $rec->{ df_phaidra_url}=  $df_phaidra;
    $rec->{val_phaidra_url}= $val_phaidra;
  }

  $rec;
}

sub extract_identifiers2
{
  my $self= shift;

  my $marc= shift; # MARC record to select data from
  my $rec= shift;  # record to fill extracted data into

  foreach my $f (@{$self->{fields}}) { $rec->{$f}= $marc->{$f}; }

  my $mrd= $marc->{xmlref}->{records}->[0]->{record}->[0]->{recordData}->[0]->{record}->[0];
  unless (defined ($mrd))
  {
    $rec->{marc_record}= 'marc_data_not_found';
    next MARC;
  }
  $rec->{marc_record}= 'marc_data_found';

  foreach my $cf (@{$mrd->{controlfield}})
  {
    if ($cf->{tag} eq '005')
    {
      $rec->{ts_marc}= $cf->{content};
    }
  }

  foreach my $df (@{$mrd->{datafield}})
  {
    foreach my $sf (@{$df->{subfield}})
    {
      my $match;

         if ($sf->{content} =~ m#urn:nbn:at:ubw#)  { $match= 'urn'; }
      elsif ($sf->{content} =~ m#10\.25365#)       { $match= 'doi'; }
      elsif ($sf->{content} =~ m#11353/#)          { $match= 'hdl'; }
      elsif ($sf->{content} =~ m#othes\.univie#)   { $match= 'othes' }
      elsif ($sf->{content} =~ m#utheses\.univie#) { $match= 'utheses' }
      elsif ($sf->{content} =~ m#phaidra\.univie#) { $match= 'phaidra' }

      if (defined ($match))
      {
        push (@{$rec->{mex}->{$match}},
        [
          join(':', (map { $df->{$_} } qw(tag ind1 ind2)), $sf->{code}),
          $sf->{content}
        ]);
      }
    }
  }

  $rec;
}

1;

