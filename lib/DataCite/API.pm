
=head1 NAME

  DataCite::API

=head1 DESCRIPTION

Interact with DataCite MDS API ([1], [2])

=head1 REFERENCES

* [1] https://support.datacite.org/docs/mds-api-guide
* [2] https://support.datacite.org/reference/overview
* [3] http://schema.test.datacite.org/
* [4] https://blog.datacite.org/cool-dois/
* [5] http://www.crockford.com/wrmg/base32.html

=cut

package DataCite::API;

use strict;
use utf8;

use base 'Module';

use Encode;
use LWP;
# use Crypt::SSLeay;
use HTTP::Headers::Util;

use Encode::Base32::Crockford;

sub mint_doi
{
  my $self= shift;

  # my $prefix= ($self->{mode} eq 'test') ? $cfg->{test_prefix} : $cfg->{prefix};

  return join ('/', $self->{config}->{prefix}, mk_suffix()),
}

sub register_doi
{
  my $self= shift;
  my $doi= shift;
  my $metadata_xml= shift;
  my $repo_url= shift;
  
  my ($code1, $res1)= $self->datacite_request ('POST', 'metadata', $metadata_xml, 'application/xml;charset=UTF-8');
  print __LINE__, " code1=[$code1] res1=[$res1]\n";
  unless ($code1 =~ m#^20[01]#)
  {
    print STDERR "ATTN: register_doi POST metadata returned code1=[$code1] res1=[$res1]\n";
    return (0, $res1, undef);
  }

  my $doi_reg= <<"EOX";
doi=$doi
url=$repo_url
EOX

  my ($code2, $res2)= $self->datacite_request ('POST', 'doi', $doi_reg, 'application/xml;charset=UTF-8');
  print __LINE__, " doi_reg=[$doi_reg] code2=[$code2] res2=[$res2]\n";

  unless ($code2 =~ m#^20[01]#)
  {
    print STDERR "ATTN: register_doi POST doi returned code2=[$code2] res2=[$res2]\n";
    return (0, $res1, $res2);
  }

  (1, $res1, $res2);
}

sub datacite_request
{
  my $self=    shift;
  my $method=  shift;
  my $verb=    shift;
  my $content= shift;
  my $content_type= shift;

  my ($api_url, $username, $password)= map { $self->{config}->{$_} } qw(api_url username password);

  my $req_url= join ('/', $api_url, $verb);
  print __LINE__, " method=[$method] req_url=[$req_url]\n";

  # build request
  my $headers = HTTP::Headers->new(
    'Accept'  => 'application/xml',
    'Content-Type' => $content_type
  );
  
  my $req = HTTP::Request->new(
    $method => $req_url,
    $headers, Encode::encode_utf8( $content )
  );
  $req->authorization_basic($username, $password);

  # pass request to the user agent and get a response back
  my $ua= LWP::UserAgent->new;
  my $res= $ua->request($req);

  return ($res->code(), $res->content());
}

=head1 INTERNAL FUNCTIONS

=head2 $doi= mk_suffix()

actually mint a DOI according to [4]

=cut

sub mk_suffix
{
  my $tries= 10;
  my $str;

  do
  {
    my $num= rand(33285996544) + 1073741824;
    # my $num= rand(33286544) + 107824;
    $str= Encode::Base32::Crockford::base32_encode_with_checksum($num);
    # printf ("%12d %s\n", $num, $str);

    return join ('-', $1, $2) if ($str =~ m#^([A-Z0-9]{4})([A-Z0-9]{4})$#);
  } while ($tries-- > 0);

  # As a last resort, print the string which might
  # contain *, =, ~, and $ .
  # These strings are valid, but maybe less readable ...

  return join ('-', $1, $2) if ($str =~ m#^(.{4})(.{4})$#);

# TODO: this should not happen, print some kind of warning?
  return ($str);
}

1;

