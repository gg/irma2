#!/usr/bin/perl
# $Id: script.pl,v 1.15 2016/09/26 10:00:50 gonter Exp $

=head1 NAME

DataCite DOI

=head1 USAGE

  dcd.pl order 5   ... reserve 5 new DOIs

=head1 EXAMPLES

  dcd.pl --ticket 14012 order 1

=cut

use strict;

# use FindBin;
use lib 'lib';

use Data::Dumper;
$Data::Dumper::Indent= 1;

# use Module;
use JSON;
use Util::JSON;
use Util::Simple_CSV;

binmode( STDOUT, ':utf8' ); autoflush STDOUT 1;

use Phaidra::DataCite;
use DataCite::API;

binmode( STDERR, ':utf8' ); autoflush STDERR 1;
binmode( STDIN,  ':utf8' );

my $config_file= '/etc/irma/DataCite.json';

# identifiers table
my $na_path= 'na/na-dcd-10-25365';
my $identifiers_file= join ('/', $na_path, 'identifiers.tsv');

my $ifx= '10.25365/phaidra';
my $na_id= 1;
my $context_id= 1;
my $ticket;

my $tsv;     # tsv object if identifiers_file is already loaded
my $urls;    # maps urls to identifiers

my $register_live= 0;

my $x_flag= 0;

my @PARS;
my $arg;
while (defined ($arg= shift (@ARGV)))
{
  if ($arg eq '--') { push (@PARS, @ARGV); @ARGV= (); }
  elsif ($arg =~ /^--(.+)/)
  {
    my ($opt, $val)= split ('=', $1, 2);

       if ($opt eq 'help')   { usage(); }
    elsif ($opt eq 'ticket') { $ticket= $val || shift (@ARGV);  }
    elsif ($opt eq 'doit')   { $register_live= 1; }
    elsif ($opt eq 'prefix')
    {
      my $prefix= $val || shift (@ARGV);
      $prefix=~ s/\./-/;
      $na_path= 'na/na-dcd-'. $prefix;
      $identifiers_file= join ('/', $na_path, 'identifiers.tsv');
    }
    elsif ($opt eq 'test')
    {
      $na_path= 'na/na-dcd-10-5072';
      $identifiers_file= join ('/', $na_path, 'identifiers.tsv');
    }
    elsif ($opt eq 'identifiers-file' || $opt eq 'idf') { $identifiers_file= $val || shift(@ARGV); }
    elsif ($opt eq 'all')
    {
      push(@PARS, get_all_urls());
    }
    else { usage(); }
  }
  elsif ($arg =~ /^-(.+)/)
  {
    foreach my $opt (split ('', $1))
    {
         if ($opt eq 'h') { usage(); exit (0); }
      elsif ($opt eq 'x') { $x_flag= 1; }
      else { usage(); }
    }
  }
  else
  {
    push (@PARS, $arg);
  }
}

# print join (' ', __FILE__, __LINE__, 'caller=['. caller() . ']'), "\n";

usage('no arguments') unless (@PARS);
# print __LINE__, " PARS: ", join("\n", @PARS), "\n";

my $cnf= Util::JSON::read_json_file ($config_file);
# print __LINE__, " cnf: ", main::Dumper ($cnf); exit(0);

my $op_code= shift (@PARS);
print __LINE__, " op_code=[$op_code]\n";

if ($op_code eq 'register')
{
  while (my $repo_url= shift (@PARS))
  {
    next if ($repo_url eq 'canonical_url'); # TSV header ...
    register_url ($cnf, $repo_url);
    sleep(3) if (@PARS);
  }
}
elsif ($op_code eq 'update')
{
  while (my $identifier= shift (@PARS))
  {
    next if ($identifier eq 'identifier'); # TSV header ...
    # register_url ($cnf, $repo_url); does not work that way!!
    sleep(3) if (@PARS);
  }
}
elsif ($op_code eq 'order')
{
  my $order_count= shift (@PARS) || 1;
  order_identifiers ($order_count);
}

exit (0);

sub usage
{
  my $msg= join (' ', @_);
  if ($msg) { print $msg, "\n"; sleep(3); }
  print <<EOX;
usage: $0 [-opts] pars

template ...

options:
-h  ... help
-x  ... set x flag
--  ... remaining args are parameters
EOX


  exit (0);
}

sub register_url
{
  my $cnf= shift;
  my $repo_url= shift;

  print __LINE__, " repo_url=[$repo_url]\n";
  if (# Phaidra
      $repo_url =~ m#^https?://(phaidra(-(sandbox|temp))?\.univie\.ac\.at)/(detail_object|view/)?(o:\d+)$#
      # OJS
      || $repo_url =~ m#https://(ojs3test\.univie\.ac\.at)/index\.php/wks/article/view/(\d+)$#
      || $repo_url =~ m#https://(wbagon\.univie\.ac\.at)/index\.php/(wbagon)/article/view/(\d+)$#
      || $repo_url =~ m#https://(rezenstfm\.univie\.ac\.at)/index\.php/(tfm)/article/view/(\d+)$#
      || $repo_url =~ m#https://(journals\.univie\.ac\.at)/index\.php/(aaj|integ|jeacs|keshif|kf|lili|mp|mpzd|oezg|rhy|wdr|yis)/(issue|article)/view/(\d+)$#
      || $repo_url =~ m#https://(yis\.univie\.ac\.at)/index\.php/(yis)/article/view/(\d+)$#
      || $repo_url =~ m#https://(chronotopos\.eu)/index\.php/(cts)/article/view/(\d+)$#
      || $repo_url =~ m#https://(viennalawreview\.com)/index\.php/(vlr)/article/view/(\d+)$#
      || $repo_url =~ m#https://(aseas\.univie\.ac\.at)/index\.php/(aseas)/article/view/(\d+)$#
      || $repo_url =~ m#https://(tyche\.univie\.ac\.at)/index\.php/(tyche)/article/view/(\d+)$#
      || $repo_url =~ m#https://(www\.protokollezurbibel\.at)/index\.php/(pzb)/article/view/(\d+)$#
      || $repo_url =~ m#https://(www\.tde-journal\.org)/index\.php/(tde)/article/view/(\d+)$#
      || $repo_url =~ m#https://(exfonte\.org)/index\.php/(exf)/article/view/(\d+)$#
      || $repo_url =~ m#https://(journalofsocialontology\.org)/index\.php/(jso)/article/view/(\d+)$#
      # Othes
      || $repo_url =~ m#https://(othes\.univie\.ac\.at)/(\d+)/$#
     )
  {
    my ($repo, @parts)= ($1, $2, $3, $4, $5, $6, $7, $8);
    my $pid;
    while (@parts && !defined($pid)) { $pid= pop (@parts); }

    my $journal= (@parts == 1 || @parts == 2) ? $parts[0] : undef; # ugly hack!

    print __LINE__, " repo=[$repo] journal=[$journal] pid=[$pid] parts=[",join('|',@parts),"]\n";

    usage ("unknown repo=[$repo]") unless (exists ($cnf->{repositories}->{$repo}));
    print __LINE__, " repo=[$repo]\n";

    # TODO: factor the code below out!
    # call do_register($cnf->{repositories}->{$repo}, $journal, ...);

    my $repo_cnf= $cnf->{repositories}->{$repo};

    my $t_reg_cnf=  $cnf->{doi_registries}->{$repo_cnf->{t_registry}};
    my $reg_cnf=  $cnf->{doi_registries}->{$repo_cnf->{registry}};

    # print __LINE__, " repo_cnf: ", main::Dumper ($repo_cnf);
    # print __LINE__, " t_reg_cnf: ", main::Dumper ($t_reg_cnf);
    # print __LINE__, " reg_cnf: ", main::Dumper ($reg_cnf);

    my $prod_doi_string= find_doi_string ($repo_url);
    unless (defined ($prod_doi_string))
    {
      print "NOTE: no identifier registered sofar\n";
      return undef;
    }

    my $sep= $repo_cnf->{id_separator};
    $sep= '.' if ($journal eq 'jeacs' || $journal eq 'integ');
    my ($pfx, $ns, $nr)= split_doi_string ($prod_doi_string, $sep);
print __LINE__, " prod_doi_string=[$prod_doi_string] pfx=[$pfx] ns=[$ns] nr=[$nr]\n";

    if ($prod_doi_string =~ m#(10\.25365)\/(BZJ)(-)(\d+-\d+)#) # see #25831
    {
      # 219 prod_doi_string=[10.25365/BZJ-048-000] pfx=[10.25365] ns=[BZJ-048-000] nr=[] dc_xml=[na/na-dcd-10-25365/metadata/BZJ-048-000/BZJ-048-000..xml]
      ($pfx, $ns, $sep, $nr)= ($1, $2, $3, $4);
    }

    my $dc_xml= join ('/', $na_path, 'metadata', $ns, join ($sep, $ns, $nr) .'.xml');

print __LINE__, " prod_doi_string=[$prod_doi_string] pfx=[$pfx] ns=[$ns] nr=[$nr] dc_xml=[$dc_xml]\n";

    my $prod_xml_new; # DataCite Metadata in XML format
    if (-f $dc_xml)
    {
      my @st= stat(_);
      my $size= $st[7];
      print "ATTN: XML file [$dc_xml] already exists, reading it!\n";
      unless (open (DC_XML, '<:utf8', $dc_xml))
      {
        print "ATTN: can not read XML from [$dc_xml]\n";
        return;
      }
      sysread (DC_XML, $prod_xml_new, $size);
      close (DC_XML);
    }
    else
    { # no XML found, so fetch metadata, convert it to XML and test it on the mds.test.datacite.org

      if ($repo_cnf->{repo_type} eq 'phaidra')
      {
        my $repo_obj= new Phaidra::DataCite (config => $repo_cnf);
        # print __LINE__, " repo_obj: ", main::Dumper ($repo_obj);

        # my ($c1, $xml)=  $repo_obj->get_metadata ($pid, 'xml');
        my ($c2, $json)= $repo_obj->get_metadata ($pid, 'json');
        my $api_res= decode_json ($json);
        if ($api_res->{status} ne '200')
        {
          print __LINE__, " api_res: ", Dumper ($api_res);
          return undef;
        }

        my $datacite_res= $api_res->{datacite};
        print __LINE__, " datacite_res: ", Dumper ($datacite_res);

        # 2018-11-19 Phaidra's DataCite output is quite ... broken ...
        fixup_datacite_data ($datacite_res);

        # TODO:
        # * mint new DOI
        # * insert DOI in metadata

        # NOTE: if ($datacite_res->{status} ne 'OK') request a dummy doi_string

        my $t_reg_obj= new DataCite::API (config => $t_reg_cnf, 'xmode' => 'test');
        my $doi_string= $t_reg_obj->mint_doi();
   
        my $doi_element=
        {
          xmlname => 'identifier',
          value => $doi_string,
          attributes =>
          [
            {
              xmlname => 'identifierType',
              value => 'DOI'
            }
          ]
        };
        print __LINE__, " doi_element: ", main::Dumper($doi_element);

        my $md= $datacite_res->{datacite_elements};
        unshift (@$md, $doi_element);
        print __LINE__, " md: ", main::Dumper ($md);

        fixup_phaidra_metadata($md);

        my $xml_new= $repo_obj->json_2_xml ($md);
        print __LINE__, " xml_new=[$xml_new]\n";

        # TODO: interact with the IRMA database to find out if the DOI string is really unique and register in there

        # TODO: interact with DataCite API to register the DOI with the metadata
        my ($t_reg_result, $t_reg_msg1, $t_reg_msg2)= $t_reg_obj->register_doi ($doi_string, $xml_new, $repo_url);

        $doi_element->{value}= $prod_doi_string;  # overwrite test DOI string with real DOI string

        $prod_xml_new= $repo_obj->json_2_xml ($md);

        unless (open (DC_XML, '>:utf8', $dc_xml))
        {
          print "ATTN: can not write XML to [$dc_xml]\n";
          return;
        }
        syswrite (DC_XML, $prod_xml_new);
        close (DC_XML);

        print __LINE__, " metatada fetched: ", get_ts(), "\n";
        if ($datacite_res->{status} ne 'OK'
            && $datacite_res->{status} ne 'INCOMPLETE' # maybe we should check, what was missing
           )
        {
          print "Metadata not ok; status=[$datacite_res->{status}] errors: ", Dumper ($datacite_res->{errors});
          return undef;
        }

        unless ($t_reg_result)
        {
          print "ATTN: register_doi with Test-DOI was not ok\n";
          return undef;
        }
      }
      else
      {
          print "ERROR: can't fetch metadata for this type of repository! [", $repo_cnf->{repo_type}, "]\n";
          return undef;
      }
    }

    print __LINE__, " prod_xml_new=[$prod_xml_new]\n";

    if ($register_live)
    {
      my $reg_obj= new DataCite::API (config => $reg_cnf, xmode => 'test');
      my ($reg_res, $reg_msg1, $reg_msg2)= $reg_obj->register_doi ($prod_doi_string, $prod_xml_new, $repo_url);
      unless ($reg_res)
      {
        print "ATTN: register_doi doi=[$prod_doi_string] was not ok; reg_msg1=[$reg_msg1] reg_msg2=[$reg_msg2]\n";
        return undef;
      }
      print "DOI registered: ", get_ts(), "\n";
    }
  }
  else
  {
    usage("unknown repo_url=[$repo_url]");
  }

}

# TODO: shema checking could work differently
sub fixup_phaidra_metadata
{
  my $md= shift; # should be an array

  # print __LINE__, " fixup_phaidra_metadata: md= ", Dumper($md);
  die 'not an array reference' unless (ref($md) eq 'ARRAY');

  my $cnt= @$md;
  print __LINE__, " md element count=[$cnt]\n";
  my %pos;
  for (my $i= 0; $i < $cnt; $i++)
  {
    my $e= $md->[$i];
    print __LINE__, " fpm: i=[$i] e= ", Dumper($e);
    $pos{$e->{xmlname}}= $i;
  }

  print __LINE__, " pos: ", Dumper(\%pos);

  splice(@$md, 3, 0, { xmlname => 'publisher',       'value' => ':none' }) unless (exists ($pos{publisher}));
  splice(@$md, 4, 0, { xmlname => 'publicationYear', 'value' => '2023'  }) unless (exists ($pos{publicationYear}));
}

sub get_ts
{
  my $time= shift || time ();

  my @ts= gmtime ($time);
  sprintf ("%4d-%02d-%02dT%02d%02d%02dZ", $ts[5]+1900, $ts[4]+1, $ts[3], $ts[2], $ts[1], $ts[0]);
}

sub setup_identifiers
{
  $tsv= new Util::Simple_CSV (load => $identifiers_file, separator => "\t", no_array => 1);
  $urls= undef;
}

sub find_doi_string
{
  my $url= shift;

  setup_identifiers() unless (defined ($tsv));

  unless (defined ($urls))
  {
    $urls= {};
    my $data= $tsv->{data};
    ROW: foreach my $row (@$data)
    {
      my ($identifier, $canonical_url)= map { $row->{$_} } qw(identifier canonical_url);
      next unless ($identifier && $canonical_url);

      if (exists ($urls->{$canonical_url}))
      {
        print __LINE__, " ERROR: url=[$canonical_url] already defined!\n";
        next ROW;
      }
      $urls->{$canonical_url}= $identifier;
    }
  }

  return (exists ($urls->{$url}))
         ? $urls->{$url}
         : undef;
}

sub get_all_urls
{
  find_doi_string('10.4567/dummy'); # don't care for the actual url ...

  sort keys %$urls;
}

sub split_doi_string
{
  my $doi= shift;  # e.g. 10.25365/phaidra.1
  my $id_separator= shift;
  
  $id_separator= qr(\.) if (!$id_separator || $id_separator eq '.');

  my ($pfx, $sfx)= split ('/', $doi, 2);
  my ($ns, $nr)= split ($id_separator, $sfx, 2);

  ($pfx, $ns, $nr);
}

sub order_identifiers
{
  my $order_count= shift;

  setup_identifiers() unless (defined ($tsv));

  my $counter= get_last_id($tsv);

print __LINE__, " identifier: ", main::Dumper ($counter);
  for (my $i= 0; $i < $order_count; $i++)
  {
    my $next_identifier= join ('.', $ifx, ++$counter->{$ifx});

    my $data= { na_id => $na_id, context_id => $context_id, ticket => $ticket, identifier => $next_identifier };

    push (@{$tsv->{data}} => $data);
  }

  $tsv->save_csv_file ();
}

sub get_last_id
{
  my $tsv= shift;

  my $data= $tsv->{data};
  my %counter;
  foreach my $row (@$data)
  {
    my $identifier= $row->{identifier};
    
    next unless ($identifier); # TODO: maybe write warning

    # my ($pfx, $sfx)= split ('/', $identifier, 2);
    my @sfx= split (/\./, $identifier);
    my $cnt= pop (@sfx);
    my $ifx= join ('.', @sfx);

    # print __LINE__, " pfx=[$pfx] sfx=[$sfx] ifx=[$ifx] cnt=[$cnt]\n";
    print __LINE__, " ifx=[$ifx] cnt=[$cnt]\n";
    if (!exists ($counter{$ifx}) || $counter{$ifx} < $cnt)
    {
      $counter{$ifx}= $cnt;
    }
    elsif ($counter{$ifx} == $cnt)
    {
      print "ATTN: duplicate identifier=[$identifier]\n";
    }
    elsif ($counter{$ifx} > $cnt)
    { # higher counter known, nothing to do
    }
  }

  (wantarray) ? %counter : \%counter;
}

sub fixup_datacite_data
{
  my $dc= shift;

  my $data= $dc->{data};

  foreach my $dce (@{$dc->{datacite_elements}})
  {
    print __LINE__, ' dce: ', Dumper($dce);
    if ($dce->{xmlname} eq 'creators')
    {
      my @filtered_creators= ();

      foreach my $creator (@{$dce->{children}})
      {
        my $creator_fixed= fix_creators ($creator);
        push (@filtered_creators, $creator_fixed) if (defined ($creator_fixed));
      }

      if (@filtered_creators)
      {
        $dce->{children}= \@filtered_creators;
      }
    }
    elsif ($dce->{xmlname} eq 'publicationYear')
    {
      print __LINE__, " publicationYear: dce=", Dumper($dce);
      if (!defined($dce->{value}))
      {
        my $year= $data->{publicationYear}->[0]->{value};
        $dce->{value}= $year if (defined($year));
      }
      elsif ($dce->{value} =~ m#^(\d{4})(-\d{2})?(-\d{2})?$#)
      {
        my $new_year= $1;
        print __LINE__, " publicationYear fixed from [", $dce->{value}, "] to [$new_year]\n";
        $dce->{value}= $new_year;
      }
    }
  }
}

sub fix_creators
{
  my $creator= shift;

  my $creator_fixed;

  if ($creator->{xmlname} eq 'creator')
  {
    my @creator_elements;
    foreach my $element (@{$creator->{children}})
    {
      push (@creator_elements, $element) if (defined ($element->{value}) && $element->{value});
    }

    if (@creator_elements)
    {
      $creator_fixed= { xmlname => 'creator', children => \@creator_elements };
    }
  }

  unless (defined ($creator_fixed))
  {
    print __LINE__, " ATTN: filtered creator: ", Dumper ($creator);
  }

  $creator_fixed;
}

__END__

=head1 AUTHOR

Firstname Lastname <address@example.org>

=head1 BUGS

wrong registry label is not dectected, e.g. when using

      "registry" : "DataCite_Test_prod",

instead of

      "registry" : "DataCite_Test_Prod",

=head1 TODO

=head2 using ticket number for register

  dcd.pl --ticket ##### register

This command should retrieve all objects asociated with the given ticket
number from the idenetifiers table and register them.

Until then, this also should work

  tsv identifiers.tsv --select ticket=##### --col canonical_url -O- | xargs ./dcd.pl register --doit

=head1 NOTES

=head2 uscholar

 * Telefonat mit Guido Blechl 2017-12-04T1320
   * Fuer uscholar Objekte soll der Permalink von Phaidra eingetragen werden



