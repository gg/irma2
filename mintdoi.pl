#!/usr/bin/perl

use strict;

use Data::Dumper;
$Data::Dumper::Indent= 1;

use Encode::Base32::Crockford;

# check(); exit;

my $prefix= '10.0174';
my $cnt= 10_000_000;

while ($cnt-- > 0)
{
  my $str= mk_doi();

  # next unless (defined ($str));
  # print $str, "\n";
  print join ('/', $prefix, $str), "\n";
}

exit(0);

sub mk_doi
{
  my $tries= 5;
  my $str;

  do
  {
    my $num= rand(33285996544) + 1073741824;
    # my $num= rand(33286544) + 107824;
    $str= Encode::Base32::Crockford::base32_encode_with_checksum($num);
    # printf ("%12d %s\n", $num, $str);

    # return join ('-', $tries, $1, $2) if ($str =~ m#^([A-Z0-9]{4})([A-Z0-9]{4})$#);
  } while ($tries-- > 0);

  return join ('-', $1, $2) if ($str =~ m#^([A-Z0-9]{4})([A-Z0-9]{4})$#);
  # return join ('-', $tries, $1, $2) if ($str =~ m#^(.{4})(.{4})$#);

  return ($str);
}

sub check
{
  for (my $num= 1073741824;; $num += 1048576)
  {
    my $str= Encode::Base32::Crockford::base32_encode_with_checksum($num);
    printf ("%12d %s\n", $num, $str);
  }
}

