#!/usr/bin/perl

=head1 NAME

  eod1.pl

=head1 DESCRIPTION

=cut

use strict;

use Data::Dumper;
$Data::Dumper::Indent= 1;
$Data::Dumper::Sortkeys= 1;

use FileHandle;

use utf8;
binmode( STDIN,  ':utf8' );
binmode( STDOUT, ':utf8' ); autoflush STDOUT 1;
binmode( STDERR, ':utf8' ); autoflush STDERR 1;

use Util::ts;
use Util::JSON;
use MongoDB;
use Util::MongoDB;

use lib 'lib';

use Alma::MARC_Extractor;
# use IRMA::NA;
use Univie::EoD::CrossReference;

my $agent_config_file= '/etc/irma/eodagent.json';
my $op_mode= 'complete';

my @tsv_columns=
qw( pid verdict ownerId state model
    ac_number alma_notes ac_number_note aleph_url
    marc_record ts_fetched fetched ts_marc mms_id lib_code
    ticket ticket_status ticket_pid ticket_url vt
    df_doi val_doi doi update_doi
    df_urn val_urn urn update_urn
    df_hdl val_hdl hdl update_hdl
    df_phaidra val_phaidra phaidra_url update_phaidra_url
);

my (@pars, @ac_numbers, @pids);
while (defined (my $arg= shift (@ARGV)))
{
     if ($arg eq '-')  { push (@pars, '-'); }
  elsif ($arg eq '--') { push (@pars, @ARGV); @ARGV= (); }
  elsif ($arg =~ /^--(.+)/)
  {
    my ($opt, $val)= split ('=', $1, 2); 
    if ($opt eq 'help') { usage(); }
    else { usage(); }
  }
  elsif ($arg =~ /^-(.+)/)
  {
    foreach my $opt (split ('', $1))
    {   
         if ($opt eq 'h') { usage(); exit (0); }
      # elsif ($opt eq 'x') { $x_flag= 1; }
      else { usage(); }
    }   
  }
  else
  {
       if ($arg =~ m#^[\w\d\-]+:\d+$#) { push (@pids, $arg) }
    elsif ($arg =~ m#^AC\d{8}$#)       { push (@ac_numbers, $arg) }
    else  { push (@pars, $arg); }
  }
}

print join (' ', __FILE__, __LINE__, 'caller=['. caller() . ']'), "\n";

my $agent_cnf= Util::JSON::read_json_file ($agent_config_file);
# print __LINE__, " agent_cnf: ", main::Dumper ($agent_cnf); exit(0);

my %counters;
my @books_duplicate_ac_number;
my @books_problems;
my @books_ok;

my $crf= Univie::EoD::CrossReference->new( agent_cnf => $agent_cnf, counters => \%counters );

my $count_objects= 0;
if (@ac_numbers)
{
  $crf->get_book_by_ac_number(\@ac_numbers);
  $count_objects += @ac_numbers;
}

if (@pids)
{
  $crf->get_book_by_pid(\@pids);
  $count_objects += @pids;
}

if (@pars)
{
  while (my $par= shift (@pars))
  {
    # TODO: ???
  }
}

if ($count_objects == 0)
{
  $crf->get_book_inventory();
}

$crf->flag_duplicate_ac_numbers();
process_book_list($crf->get_book_list());

print __LINE__, " counters: ", Dumper(\%counters);

Util::JSON::write_json_file('eod_problems.json', \@books_problems);

write_tsv_file('eod_data.tsv', \@books_ok);
write_tsv_file('duplicate_eod_data.tsv', \@books_duplicate_ac_number);
write_tsv_file('problems_eod_data.tsv', \@books_problems);

exit(0);

sub process_book_list
{
  my $book_list= shift;

my $book_count= @$book_list;
print __LINE__, " found $book_count books\n";

# BEGIN step 2: check IRMA records

# BEGIN step 3: check Alma records
# my $req_col= $marc_db->get_collection('requests');

  # my @marc_mex_fields= qw(df_doi val_doi df_phaidra val_phaidra df_urn val_urn ts_marc);
  # my @marc_extra_fields= qw(marc_record ts_fetched);

  BOOK3: foreach my $book (@$book_list)
  {
    my ($verdict)= $crf->check_book($book);
    $book->{verdict}= $verdict;
    $counters{$verdict}++;

    if ($verdict eq 'ok')
    {
      push (@books_ok, $book);
    }
    elsif ($verdict eq 'ac_number_note' && $book->{ac_number_note} eq 'dup')
    {
      push (@books_duplicate_ac_number, $book);
    }
    else
    {
      push (@books_problems, $book);
    }
  }
}

sub write_tsv_file
{
  my $tsv_filename= shift;
  my $book_list= shift;

  open (TSV, '>:utf8', $tsv_filename) or die "can't write to $tsv_filename";
  my $count_books_ok= @$book_list;
  print TSV join("\t", @tsv_columns), "\n";
  print "saving $count_books_ok to $tsv_filename\n";
  BOOKx: foreach my $book (@$book_list)
  {
    print TSV join("\t", map { $book->{$_} } @tsv_columns), "\n";
  }
  close (TSV);
}

__END__

=head1 PROBLEMS

=head2 AC06947549

-[ RECORD 1 ]----------------------
pid         | o:1024961
verdict     | ok
ownerId     | ondemae7
state       | Active
model       | Book
ac_number   | AC06947549
alma_notes  | update_hdl
aleph_url   | https://ubdata.univie.ac.at/AC06947549
marc_record | marc_data_found
ts_fetched  | 2023-03-18T172423
fetched     | 1679160263
ts_marc     | 20230210194745.0
mms_id      | 990074078270203332
lib_code    | 43ACC_UBW
df_hdl      | 776:0:8:o
val_hdl     | 11353/10.1027250
df_phaidra  | 856:4:1:u
val_phaidra | https://phaidra.univie.ac.at/o:1024961
phaidra_url | https://phaidra.univie.ac.at/o:1024961

the handle value in 776:0:8:o is 11353/10.1027250 but it should be 11353/10.1024961

=head2 AC02901724

-[ RECORD 1 ]----------------------
pid                | o:90496
alma_notes         | update_phaidra_url
ownerId            | ondemae7
state              | Active
model              | Book
ac_number          | AC02901724
aleph_url          | http://aleph.univie.ac.at/F?func=find-c&ccl_term=AC02901724
marc_record        | marc_data_found
ts_fetched         | 2023-03-18T173800
fetched            | 1679161080
ts_marc            | 20230303140010.0
mms_id             | 990013785100203332
lib_code           | 43ACC_UBW
df_phaidra         | 856:4: :u
val_phaidra        | http://phaidra.univie.ac.at/o:90495
phaidra_url        | https://phaidra.univie.ac.at/o:90496
update_phaidra_url | https://phaidra.univie.ac.at/o:90496

o:90495 is a collection, o:90496 is the only member; this is a ZS record,
so this is ok and the suggested change is not correct.  Filtered for now

=head2 request queue entry example (edited)

/home/gg/perl/Phaidra/Stage/JobQueue.pm 128 insert_new_job()
* job: $VAR1 = {
  'requested_by' => $my_agent_name,
  'ac_number' => 'AC08824094',
  'ts_iso' => '20230327T133843',
  'action' => 'update_alma_2xml',
  'status' => 'new',
  'agent' => 'alma_cat'
};


=head2 update queue (stand 2023-03-27T1340)

ac_number          | AC08838287
ts_marc            | 20211219214345.0

ac_number   | AC08824094
ts_marc     | 20171115013000.0


=head1 config file

=head2 prefiltered_pids

List of pids which are removed from the search results and thus are
never entered into any result set.  E.g. objects that should be deleted
but are not yet.

=head2 filtered_pids

List of pids which should not be processed further due to some reason.
See ticket for details.

=head2 filtered_pids_resolved

List of pids that were dealt with already.  The script does not do
anything with this list actually, it is only used to store items from
one of the other lists for reference only.

=head1 DOIs

stage:gg> find alma_cat/ -type f -print |xargs fgrep 10.25365/digital-copy
alma_cat/082/AC07818082/alma.xml:            <subfield code="a">10.25365/digital-copy.1</subfield>
alma_cat/405/AC15515405/alma.xml:            <subfield code="a">10.25365/digital-copy.1</subfield>
stage:gg> date
Mon Mar 27 16:40:26 CEST 2023


